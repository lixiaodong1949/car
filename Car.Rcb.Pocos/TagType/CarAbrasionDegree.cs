﻿using Car.Framework;
using Car.Framework.Entity;
using FreeSql.DataAnnotations;
using System;
using System.Collections.Generic;
using System.Text;

namespace Car.Rcb.Pocos
{
    /// <summary>
    /// 磨损程度表
    /// </summary>
    [Table(Name = "car_abrasion_degree")]
    public class CarAbrasionDegree : BizBaseEntity<long>
    {
        /// <summary>
        /// 磨损名称
        /// </summary>
        public string AbrasionName { get; set; }

        /// <summary>
        /// 新旧程度Ids
        /// </summary>
        public string NewOldIds { get; set; }

        /// <summary>
        /// 数据使用状态 0:启用 1:禁用
        /// </summary>
        [Column(DbType = "int")]
        public DataUseStatus DataUseStatus { get; set; }

        /// <summary>
        /// 排序序号
        /// </summary>
        public int Sort { get; set; }
    }
}
