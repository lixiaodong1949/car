﻿using Car.Framework;
using Car.Framework.Entity;
using FreeSql.DataAnnotations;
using System;
using System.Collections.Generic;
using System.Text;

namespace Car.Rcb.Pocos
{
    /// <summary>
    /// 配件分类表
    /// </summary>
    [Table(Name = "car_accessory_type")]
    public class CarAccessoryType : BizBaseEntity<long>
    {
        /// <summary>
        /// 配件名称
        /// </summary>
        public string AccessoryName { get; set; }

        /// <summary>
        /// 数据使用状态 0:启用 1:禁用
        /// </summary>
        [Column(DbType = "int")]
        public DataUseStatus DataUseStatus { get; set; }

        /// <summary>
        /// 排序序号
        /// </summary>
        public int Sort { get; set; }
    }
}
