﻿using Car.Framework;
using Car.Framework.Entity;
using FreeSql.DataAnnotations;
using System;
using System.Collections.Generic;
using System.Text;

namespace Car.Rcb.Pocos
{
    /// <summary>
    /// 产品类型表
    /// </summary>
    [Table(Name = "car_product_type")]
    public class CarProductType : BizBaseEntity<long>
    {
        /// <summary>
        /// 产品名称
        /// </summary>
        public string ProductName { get; set; }

        /// <summary>
        /// 配件分类Ids
        /// </summary>
        public string AccessoryIds { get; set; }

        /// <summary>
        /// 数据使用状态 0:启用 1:禁用
        /// </summary>
        [Column(DbType = "int")]
        public DataUseStatus DataUseStatus { get; set; }

        /// <summary>
        /// 排序序号
        /// </summary>
        public int Sort { get; set; }
    }
}
