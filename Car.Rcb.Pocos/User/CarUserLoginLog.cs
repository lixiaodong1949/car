﻿using Car.Framework.Entity;
using FreeSql.DataAnnotations;
using System;
using System.Collections.Generic;
using System.Text;

namespace Car.Rcb.Pocos
{
    /// <summary>
    /// 用户登录信息表
    /// </summary>
    [Table(Name = "car_userloginlog")]
    public class CarUserLoginLog : BaseEntity<long>
    {
        /// <summary>
        /// 登录用户Id
        /// </summary>
        public long UserId { get; set; }

        /// <summary>
        /// 登录Ip
        /// </summary>
        public string LoginIp { get; set; }

        /// <summary>
        /// 登录时间
        /// </summary>
        public DateTime LoginTime { get; set; }

        /// <summary>
        /// 登录token
        /// </summary>
        [Column(Name = "AccessToken", DbType = "nvarchar(1000)")]
        public string AccessToken { get; set; }
    }
}
