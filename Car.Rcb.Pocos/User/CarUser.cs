﻿using Car.Framework;
using Car.Framework.Entity;
using FreeSql.DataAnnotations;
using System;
using System.Collections.Generic;
using System.Text;

namespace Car.Rcb.Pocos
{
    /// <summary>
    /// 用户表
    /// </summary>
    [Table(Name = "car_user")]
    public class CarUser : BizBaseEntity<long>
    {
        /// <summary>
        /// 手机号
        /// </summary>
        [Column(Name = "MobilePhone", DbType = "varchar(255)", IsNullable = true)]
        public string MobilePhone { get; set; }

        /// <summary>
        /// 密码
        /// </summary>
        [Column(Name = "Password", DbType = "varchar(255)", IsNullable = true)]
        public string Password { get; set; }

        /// <summary>
        /// 性别
        /// </summary>
        public string Sex { get; set; }

        /// <summary>
        /// 昵称
        /// </summary>
        [Column(Name = "NickName", DbType = "varchar(255)", IsNullable = true)]
        public string NickName { get; set; }

        /// <summary>
        /// 真实姓名
        /// </summary>
        public string RealName { get; set; }

        /// <summary>
        /// 头像链接
        /// </summary>
        public string PictureLink { get; set; }

        /// <summary>
        /// 邮箱
        /// </summary>
        [Column(Name = "Email", DbType = "varchar(255)", IsNullable = true)]
        public string Email { get; set; }

        /// <summary>
        /// 身份证号码
        /// </summary>
        [Column(Name = "IdNumber", DbType = "varchar(20)", IsNullable = true)]
        public string IdNumber { get; set; }

        /// <summary>
        /// 用户状态 0:启用 1:禁用
        /// </summary>
        [Column(DbType = "int")]
        public UserStatus UserStatus { get; set; }
    }
}
