﻿using Car.Framework.Entity;
using FreeSql.DataAnnotations;
using System;
using System.Collections.Generic;
using System.Text;

namespace Car.Rcb.Pocos
{
    /// <summary>
    /// 二手或求购图片表
    /// </summary>
    [Table(Name = "car_usedoraskbuyarticle_picture")]
    public class CarUsedOrAskBuyArticlePicture : BizBaseEntity<long>
    {
        /// <summary>
        /// 二手或求购主键Id
        /// </summary>
        public long UsedOrAskBuyId { get; set; }

        /// <summary>
        /// 图片地址
        /// </summary>
        public string PictureUrl { get; set; }

        /// <summary>
        /// 是否是封面图 false:不是 true:是
        /// </summary>
        public bool IsCoverPicture { get; set; }
    }
}
