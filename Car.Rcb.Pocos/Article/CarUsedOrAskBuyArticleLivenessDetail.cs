﻿using Car.Framework.Entity;
using FreeSql.DataAnnotations;
using System;
using System.Collections.Generic;
using System.Text;

namespace Car.Rcb.Pocos
{
    /// <summary>
    /// 二手或求购活跃度详情表
    /// </summary>
    [Table(Name = "car_usedoraskbuyarticle_livenessdetail")]
    public class CarUsedOrAskBuyArticleLivenessDetail : BizBaseEntity<long>
    {
        /// <summary>
        /// 二手或求购主键Id
        /// </summary>
        public long UsedOrAskBuyId { get; set; }

        /// <summary>
        /// 查看数
        /// </summary>
        public int PreviewNumber { get; set; }

        /// <summary>
        /// 收藏数
        /// </summary>
        public int CollectNumber { get; set; }
    }
}
