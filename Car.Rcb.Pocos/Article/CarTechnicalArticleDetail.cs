﻿using Car.Framework;
using Car.Framework.Entity;
using FreeSql.DataAnnotations;
using System;
using System.Collections.Generic;
using System.Text;

namespace Car.Rcb.Pocos
{
    /// <summary>
    /// 技术文章详情表
    /// </summary>
    [Table(Name = "car_technicalarticle_detail")]
    public class CarTechnicalArticleDetail : BizBaseEntity<long>
    {
        /// <summary>
        /// 标题名称
        /// </summary>
        [Column(Name = "TitleName", DbType = "varchar(255)", IsNullable = true)]
        public string TitleName { get; set; }

        /// <summary>
        /// 文章类型
        /// </summary>
        public long TechnologyTagId { get; set; }

        /// <summary>
        /// 是否是原创 false:否 true:是
        /// </summary>
        public bool IsOriginal { get; set; }

        /// <summary>
        /// 审核状态 (0:发布待审 1:审核成功 2:审核拒绝)
        /// </summary>
        [Column(DbType = "int")]
        public AuditStatus AuditStatus { get; set; }

        /// <summary>
        /// 上架状态 (0:未上架 1:已上架 2:已下架)
        /// </summary>
        [Column(DbType = "int")]
        public HitShelfStatus HitShelfStatus { get; set; }
    }
}
