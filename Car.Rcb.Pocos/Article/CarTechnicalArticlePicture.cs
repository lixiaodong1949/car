﻿using Car.Framework.Entity;
using FreeSql.DataAnnotations;
using System;
using System.Collections.Generic;
using System.Text;

namespace Car.Rcb.Pocos
{
    /// <summary>
    /// /// <summary>
    /// 技术文章段落图片详情表
    /// </summary>
    [Table(Name = "car_technicalarticle_picture")]
    public class CarTechnicalArticlePicture : BizBaseEntity<long>
    {
        /// <summary>
        /// 文章主键Id
        /// </summary>
        public long ArticleId { get; set; }

        /// <summary>
        /// 段落主键Id
        /// </summary>
        public long ParagraphId { get; set; }
        
        /// <summary>
        /// 图片地址
        /// </summary>
        public string PictureUrl { get; set; }

        /// <summary>
        /// 是否是封面图 false:不是 true:是
        /// </summary>
        public bool IsCoverPicture { get; set; }
    }
}
