﻿using Car.Framework.Entity;
using FreeSql.DataAnnotations;
using System;
using System.Collections.Generic;
using System.Text;

namespace Car.Rcb.Pocos
{
    /// <summary>
    /// 二手或求购文章用户活跃详情表
    /// </summary>
    [Table(Name = "car_usedoraskbuyarticle_useractivedetail")]
    public class CarUsedOrAskBuyArticleUserActiveDetail : BizBaseEntity<long>
    {
        /// <summary>
        /// 二手或求购主键Id
        /// </summary>
        public long UsedOrAskBuyId { get; set; }

        /// <summary>
        /// 用户主键Id
        /// </summary>
        public long UserId { get; set; }

        /// <summary>
        /// 是否已查看
        /// </summary>
        public bool IsPreview { get; set; }

        /// <summary>
        /// 是否已收藏
        /// </summary>
        public bool IsCollect { get; set; }
    }
}
