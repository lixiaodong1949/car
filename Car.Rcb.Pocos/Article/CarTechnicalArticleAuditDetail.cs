﻿using Car.Framework;
using Car.Framework.Entity;
using FreeSql.DataAnnotations;
using System;
using System.Collections.Generic;
using System.Text;

namespace Car.Rcb.Pocos
{
    /// <summary>
    /// /// <summary>
    /// 技术文章审核详情表
    /// </summary>
    [Table(Name = "car_technicalarticle_auditdetail")]
    public class CarTechnicalArticleAuditDetail : BizBaseEntity<long>
    {
        /// <summary>
        /// 文章主键Id
        /// </summary>
        public long ArticleId { get; set; }

        /// <summary>
        /// 审核操作状态 (1:审核成功 2:审核拒绝)
        /// </summary>
        [Column(DbType = "int")]
        public AuditStatus AuditStatus { get; set; }

        /// <summary>
        /// 操作理由
        /// </summary>
        [Column(Name = "OperateReason", DbType = "LongText", IsNullable = true)]
        public string OperateReason { get; set; }
    }
}
