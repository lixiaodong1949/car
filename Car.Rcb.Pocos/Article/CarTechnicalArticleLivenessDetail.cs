﻿using Car.Framework.Entity;
using FreeSql.DataAnnotations;
using System;
using System.Collections.Generic;
using System.Text;

namespace Car.Rcb.Pocos
{
    /// <summary>
    /// 技术文章活跃度详情表
    /// </summary>
    [Table(Name = "car_technicalarticle_livenessdetail")]
    public class CarTechnicalArticleLivenessDetail : BizBaseEntity<long>
    {
        /// <summary>
        /// 文章主键Id
        /// </summary>
        public long ArticleId { get; set; }

        /// <summary>
        /// 查看数
        /// </summary>
        public int PreviewNumber { get; set; }

        /// <summary>
        /// 收藏数
        /// </summary>
        public int CollectNumber { get; set; }

        /// <summary>
        /// 点赞数
        /// </summary>
        public int LikeNumber { get; set; }
    }
}
