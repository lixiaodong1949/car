﻿using Car.Framework;
using Car.Framework.Entity;
using FreeSql.DataAnnotations;
using System;
using System.Collections.Generic;
using System.Text;

namespace Car.Rcb.Pocos
{
    /// <summary>
    /// /// <summary>
    /// 二手或求购文章审核详情表
    /// </summary>
    [Table(Name = "car_usedoraskbuyarticle_auditdetail")]
    public class CarUsedOrAskBuyArticleAuditDetail : BizBaseEntity<long>
    {
        /// <summary>
        /// 二手或求购主键Id
        /// </summary>
        public long UsedOrAskBuyId { get; set; }

        /// <summary>
        /// 审核操作状态 (1:审核成功 2:审核拒绝)
        /// </summary>
        [Column(DbType = "int")]
        public AuditStatus AuditStatus { get; set; }

        /// <summary>
        /// 操作理由
        /// </summary>
        [Column(Name = "OperateReason", DbType = "LongText", IsNullable = true)]
        public string OperateReason { get; set; }
    }
}
