﻿using Car.Framework.Entity;
using FreeSql.DataAnnotations;
using System;
using System.Collections.Generic;
using System.Text;

namespace Car.Rcb.Pocos
{
    /// <summary>
    /// 技术文章用户活跃详情表
    /// </summary>
    [Table(Name = "car_technicalarticle_useractivedetail")]
    public class CarTechnicalArticleUserActiveDetail : BizBaseEntity<long>
    {
        /// <summary>
        /// 文章主键Id
        /// </summary>
        public long ArticleId { get; set; }

        /// <summary>
        /// 用户主键Id
        /// </summary>
        public long UserId { get; set; }

        /// <summary>
        /// 是否已查看
        /// </summary>
        public bool IsPreview { get; set; }

        /// <summary>
        /// 是否已收藏
        /// </summary>
        public bool IsCollect { get; set; }

        /// <summary>
        /// 是否已点赞
        /// </summary>
        public bool IsLike { get; set; }
    }
}
