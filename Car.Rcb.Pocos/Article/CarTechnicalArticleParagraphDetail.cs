﻿using Car.Framework.Entity;
using FreeSql.DataAnnotations;
using System;
using System.Collections.Generic;
using System.Text;

namespace Car.Rcb.Pocos
{
    /// <summary>
    /// /// <summary>
    /// 技术文章段落详情表
    /// </summary>
    [Table(Name = "car_technicalarticle_paragraphdetail")]
    public class CarTechnicalArticleParagraphDetail : BizBaseEntity<long>
    {
        /// <summary>
        /// 文章主键Id
        /// </summary>
        public long ArticleId { get; set; }

        /// <summary>
        /// 段落内容
        /// </summary>
        [Column(Name = "Content", DbType = "LongText", IsNullable = true)]
        public string Content { get; set; }
    }
}
