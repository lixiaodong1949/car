﻿using Car.Framework;
using Car.Framework.Entity;
using FreeSql.DataAnnotations;
using System;
using System.Collections.Generic;
using System.Text;

namespace Car.Rcb.Pocos
{
    /// <summary>
    /// 二手或求购详情表
    /// </summary>
    [Table(Name = "car_usedoraskbuyarticle_detail")]
    public class CarUsedOrAskBuyArticleDetail : BizBaseEntity<long>
    {
        /// <summary>
        /// 二手或求购类型 (0:二手 1:求购)
        /// </summary>
        [Column(DbType = "int")]
        public UsedOrAskBuyType UsedOrAskBuyType { get; set; }

        /// <summary>
        /// 标题名称
        /// </summary>
        [Column(Name = "TitleName", DbType = "varchar(255)", IsNullable = true)]
        public string TitleName { get; set; }

        /// <summary>
        /// 适用品牌Id
        /// </summary>
        public long BrandId { get; set; }

        /// <summary>
        /// 配件分类Id
        /// </summary>
        public long AccessoryTypeId { get; set; }

        /// <summary>
        /// 产品类型Id
        /// </summary>
        public long ProductTypeId { get; set; }

        /// <summary>
        /// 新旧程度Id
        /// </summary>
        public long NewOldDegreeId { get; set; }

        /// <summary>
        /// 磨损程度Id
        /// </summary>
        public long AbrasionDegreeId { get; set; }

        /// <summary>
        /// 出售原因
        /// </summary>
        [Column(Name = "SaleReason", DbType = "LongText", IsNullable = true)]
        public string SaleReason { get; set; }

        /// <summary>
        /// 省级代码
        /// </summary>
        public string ProvinceCode { get; set; }

        /// <summary>
        /// 省级名称
        /// </summary>
        public string ProvinceName { get; set; }

        /// <summary>
        /// 市级代码
        /// </summary>
        public string CityCode { get; set; }

        /// <summary>
        /// 市级名称
        /// </summary>
        public string CityName { get; set; }

        /// <summary>
        /// 区级代码
        /// </summary>
        public string DistrictCode { get; set; }

        /// <summary>
        /// 区级名称
        /// </summary>
        public string DistrictName { get; set; }

        /// <summary>
        /// 详细地址
        /// </summary>
        public string DetailedAddress { get; set; }

        /// <summary>
        /// 联系电话
        /// </summary>
        public string MobilePhone { get; set; }

        /// <summary>
        /// 议价类型 (0:面议 1:否决)
        /// </summary>
        [Column(DbType = "int")]
        public BargainType BargainType { get; set; }

        /// <summary>
        /// 编号
        /// </summary>
        public string SerialNumber { get; set; }

        /// <summary>
        /// 审核状态 (0:发布待审 1:审核成功 2:审核拒绝)
        /// </summary>
        [Column(DbType = "int")]
        public AuditStatus AuditStatus { get; set; }

        /// <summary>
        /// 上架状态 (0:未上架 1:已上架 2:已下架)
        /// </summary>
        [Column(DbType = "int")]
        public HitShelfStatus HitShelfStatus { get; set; }

        /// <summary>
        /// 交易是否完成 false:未完成 true:已完成
        /// </summary>
        public bool IsDealClose { get; set; }

        /// <summary>
        /// 追加品牌
        /// </summary>
        public string BrandAdditional { get; set; }
    }
}
