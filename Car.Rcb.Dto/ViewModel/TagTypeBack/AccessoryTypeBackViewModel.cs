﻿using Car.Framework;
using System;
using System.Collections.Generic;
using System.Text;

namespace Car.Rcb.Dto
{
    /// <summary>
    /// 配件分类返回实体
    /// </summary>
    public class AccessoryTypeBackViewModel
    {
        /// <summary>
        /// 主键Id
        /// </summary>
        public long Id { get; set; }

        /// <summary>
        /// 配件名称
        /// </summary>
        public string AccessoryName { get; set; }

        /// <summary>
        /// 数据使用状态 0:启用 1:禁用
        /// </summary>
        public DataUseStatus DataUseStatus { get; set; }

        /// <summary>
        /// 排序序号
        /// </summary>
        public int Sort { get; set; }
    }
}
