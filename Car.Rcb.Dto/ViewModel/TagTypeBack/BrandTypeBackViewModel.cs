﻿using Car.Framework;
using System;
using System.Collections.Generic;
using System.Text;

namespace Car.Rcb.Dto
{
    /// <summary>
    /// 品牌类型返回实体
    /// </summary>
    public class BrandTypeBackViewModel
    {
        /// <summary>
        /// 主键Id
        /// </summary>
        public long Id { get; set; }

        /// <summary>
        /// 品牌名称
        /// </summary>
        public string BrandName { get; set; }

        /// <summary>
        /// 数据使用状态 0:启用 1:禁用
        /// </summary>
        public DataUseStatus DataUseStatus { get; set; }

        /// <summary>
        /// 排序序号
        /// </summary>
        public int Sort { get; set; }
    }
}
