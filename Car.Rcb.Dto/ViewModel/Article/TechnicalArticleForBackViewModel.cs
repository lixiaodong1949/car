﻿using Car.Framework;
using System;
using System.Collections.Generic;
using System.Text;

namespace Car.Rcb.Dto
{
    /// <summary>
    /// 技术文章后台返回实体
    /// </summary>
    public class TechnicalArticleForBackViewModel
    {
        /// <summary>
        /// 主键Id
        /// </summary>
        public long Id { get; set; }

        /// <summary>
        /// 标题名称
        /// </summary>        
        public string TitleName { get; set; }

        /// <summary>
        /// 封面图地址
        /// </summary>
        public string CoverPictureUrl { get; set; }

        /// <summary>
        /// 文章类型主键
        /// </summary>
        public long TagId { get; set; }

        /// <summary>
        /// 文章类型名称
        /// </summary>
        public string TagName { get; set; }

        /// <summary>
        /// 是否是原创 false:否 true:是
        /// </summary>
        public bool IsOriginal { get; set; }

        /// <summary>
        /// 审核状态 (0:发布待审 1:审核成功 2:审核拒绝)
        /// </summary>
        public AuditStatus AuditStatus { get; set; }

        /// <summary>
        /// 上架状态 (0:未上架 1:已上架 2:已下架)
        /// </summary>
        public HitShelfStatus HitShelfStatus { get; set; }

        /// <summary>
        /// 查看数
        /// </summary>
        public int PreviewNumber { get; set; }

        /// <summary>
        /// 收藏数
        /// </summary>
        public int CollectNumber { get; set; }

        /// <summary>
        /// 点赞数
        /// </summary>
        public int LikeNumber { get; set; }

        /// <summary>
        /// 创建时间
        /// </summary>
        public DateTime CreateTime { get; set; }

        /// <summary>
        /// 创建人主键Id
        /// </summary>
        public long CreateById { get; set; }

        /// <summary>
        /// 创建人名称(用户昵称)
        /// </summary>
        public string CreateByName { get; set; }

        /// <summary>
        /// 当前时间
        /// </summary>
        public DateTime CurrentTime { get; set; }

        /// <summary>
        /// 段落列表
        /// </summary>
        public List<TechnicalArticleParagraphViewModel> ParagraphList = new List<TechnicalArticleParagraphViewModel>();
    }
}
