﻿using Car.Framework;
using System;
using System.Collections.Generic;
using System.Text;

namespace Car.Rcb.Dto
{
    /// <summary>
    /// 二手或求购文章后台返回实体
    /// </summary>
    public class UsedOrAskBuyArticleForBackViewModel
    {
        /// <summary>
        /// 主键Id
        /// </summary>
        public long Id { get; set; }

        /// <summary>
        /// 标题名称
        /// </summary>
        public string TitleName { get; set; }

        /// <summary>
        /// 封面图地址
        /// </summary>
        public string CoverPictureUrl { get; set; }

        /// <summary>
        /// 适用品牌Id
        /// </summary>
        public long BrandId { get; set; }

        /// <summary>
        /// 品牌名称
        /// </summary>
        public string BrandName { get; set; }

        /// <summary>
        /// 配件分类Id
        /// </summary>
        public long AccessoryTypeId { get; set; }

        /// <summary>
        /// 配件名称
        /// </summary>
        public string AccessoryName { get; set; }

        /// <summary>
        /// 产品类型Id
        /// </summary>
        public long ProductTypeId { get; set; }

        /// <summary>
        /// 产品名称
        /// </summary>
        public string ProductName { get; set; }

        /// <summary>
        /// 新旧程度Id
        /// </summary>
        public long NewOldDegreeId { get; set; }

        /// <summary>
        /// 新旧名称
        /// </summary>
        public string NewOldName { get; set; }

        /// <summary>
        /// 磨损程度Id
        /// </summary>
        public long AbrasionDegreeId { get; set; }

        /// <summary>
        /// 磨损名称
        /// </summary>
        public string AbrasionName { get; set; }

        /// <summary>
        /// 出售原因
        /// </summary>
        public string SaleReason { get; set; }

        /// <summary>
        /// 省级代码
        /// </summary>
        public string ProvinceCode { get; set; }

        /// <summary>
        /// 省级名称
        /// </summary>
        public string ProvinceName { get; set; }

        /// <summary>
        /// 市级代码
        /// </summary>
        public string CityCode { get; set; }

        /// <summary>
        /// 市级名称
        /// </summary>
        public string CityName { get; set; }

        /// <summary>
        /// 区级代码
        /// </summary>
        public string DistrictCode { get; set; }

        /// <summary>
        /// 区级名称
        /// </summary>
        public string DistrictName { get; set; }

        /// <summary>
        /// 详细地址
        /// </summary>
        public string DetailedAddress { get; set; }

        /// <summary>
        /// 联系电话
        /// </summary>
        public string MobilePhone { get; set; }

        /// <summary>
        /// 议价类型 (0:面议 1:否决)
        /// </summary>
        public BargainType BargainType { get; set; }

        /// <summary>
        /// 编号
        /// </summary>
        public string SerialNumber { get; set; }

        /// <summary>
        /// 追加品牌
        /// </summary>
        public string BrandAdditional { get; set; }

        /// <summary>
        /// 审核状态 (0:发布待审 1:审核成功 2:审核拒绝)
        /// </summary>
        public AuditStatus AuditStatus { get; set; }

        /// <summary>
        /// 上架状态 (0:未上架 1:已上架 2:已下架)
        /// </summary>
        public HitShelfStatus HitShelfStatus { get; set; }

        /// <summary>
        /// 查看数
        /// </summary>
        public int PreviewNumber { get; set; }

        /// <summary>
        /// 收藏数
        /// </summary>
        public int CollectNumber { get; set; }

        /// <summary>
        /// 创建时间
        /// </summary>
        public DateTime CreateTime { get; set; }

        /// <summary>
        /// 创建人主键Id
        /// </summary>
        public long CreateById { get; set; }

        /// <summary>
        /// 创建人名称(用户昵称)
        /// </summary>
        public string CreateByName { get; set; }

        /// <summary>
        /// 当前时间
        /// </summary>
        public DateTime CurrentTime { get; set; }

        /// <summary>
        /// 图片列表
        /// </summary>
        public List<UsedOrAskBuyArticlePictureViewModel> PictureList = new List<UsedOrAskBuyArticlePictureViewModel>();
    }
}
