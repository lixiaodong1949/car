﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Car.Rcb.Dto
{
    /// <summary>
    /// 技术文章评论详情返回实体
    /// </summary>
    public class TechnicalArticleCommentViewModel
    {
        /// <summary>
        /// 主键Id
        /// </summary>
        public long Id { get; set; }

        /// <summary>
        /// 评论用户Id
        /// </summary>
        public long UserId { get; set; }

        /// <summary>
        /// 评论者(用户昵称)
        /// </summary>
        public string Reviewer { get; set; }

        /// <summary>
        /// 评论者头像链接
        /// </summary>
        public string PictureLink { get; set; }

        /// <summary>
        /// 评论者是否是自己 false:否 true:是
        /// </summary>
        public bool IsOneSelf { get; set; }

        /// <summary>
        /// 评论内容
        /// </summary>
        public string Content { get; set; }

        /// <summary>
        /// 评论时间
        /// </summary>
        public DateTime CreateTime { get; set; }

        /// <summary>
        /// 当前时间
        /// </summary>
        public DateTime CurrentTime { get; set; }
    }
}
