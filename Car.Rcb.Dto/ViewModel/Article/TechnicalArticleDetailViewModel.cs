﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Car.Rcb.Dto
{
    /// <summary>
    /// 技术文章详情返回实体
    /// </summary>
    public class TechnicalArticleDetailViewModel
    {
        /// <summary>
        /// 主键Id
        /// </summary>
        public long Id { get; set; }

        /// <summary>
        /// 标题名称
        /// </summary>        
        public string TitleName { get; set; }

        /// <summary>
        /// 文章类型主键
        /// </summary>
        public long TagId { get; set; }

        /// <summary>
        /// 文章类型名称
        /// </summary>
        public string TagName { get; set; }

        /// <summary>
        /// 创建时间
        /// </summary>
        public DateTime CreateTime { get; set; }

        /// <summary>
        /// 创建人主键Id
        /// </summary>
        public long CreateById { get; set; }

        /// <summary>
        /// 创建人名称(用户昵称)
        /// </summary>
        public string CreateByName { get; set; }

        /// <summary>
        /// 文章创建者头像链接
        /// </summary>
        public string PictureLink { get; set; }

        /// <summary>
        /// 当前时间
        /// </summary>
        public DateTime CurrentTime { get; set; }

        /// <summary>
        /// 查看数
        /// </summary>
        public int PreviewNumber { get; set; }

        /// <summary>
        /// 收藏数
        /// </summary>
        public int CollectNumber { get; set; }

        /// <summary>
        /// 点赞数
        /// </summary>
        public int LikeNumber { get; set; }

        /// <summary>
        /// 是否已查看
        /// </summary>
        public bool IsPreview { get; set; }

        /// <summary>
        /// 是否已收藏
        /// </summary>
        public bool IsCollect { get; set; }

        /// <summary>
        /// 是否已点赞
        /// </summary>
        public bool IsLike { get; set; }

        /// <summary>
        /// 段落列表
        /// </summary>
        public List<TechnicalArticleParagraphViewModel> ParagraphList = new List<TechnicalArticleParagraphViewModel>();
    }
}
