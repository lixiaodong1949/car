﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Car.Rcb.Dto
{
    /// <summary>
    /// 技术文章段落图片返回实体
    /// </summary>
    public class TechnicalArticlePictureViewModel
    {
        /// <summary>
        /// 图片地址
        /// </summary>
        public string PictureUrl { get; set; }
    }
}
