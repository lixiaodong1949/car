﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Car.Rcb.Dto
{
    /// <summary>
    /// 技术文章段落返回实体
    /// </summary>
    public class TechnicalArticleParagraphViewModel
    {
        /// <summary>
        /// 主键Id
        /// </summary>
        public long Id { get; set; }

        /// <summary>
        /// 段落内容
        /// </summary>
        public string Content { get; set; }

        /// <summary>
        /// 图片列表
        /// </summary>
        public List<TechnicalArticlePictureViewModel> PictureList = new List<TechnicalArticlePictureViewModel>();
    }
}
