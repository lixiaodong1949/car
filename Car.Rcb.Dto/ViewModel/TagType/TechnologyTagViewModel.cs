﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Car.Rcb.Dto
{
    /// <summary>
    /// 技术标签返回实体
    /// </summary>
    public class TechnologyTagViewModel
    {
        /// <summary>
        /// 主键Id
        /// </summary>
        public long Id { get; set; }

        /// <summary>
        /// 标签名称
        /// </summary>
        public string TagName { get; set; }

        /// <summary>
        /// 图标链接
        /// </summary>
        public string IconLink { get; set; }

        /// <summary>
        /// 排序序号
        /// </summary>
        public int Sort { get; set; }
    }
}
