﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Car.Rcb.Dto
{
    /// <summary>
    /// 磨损程度返回实体
    /// </summary>
    public class AbrasionDegreeRequestModel
    {
        /// <summary>
        /// 主键Id
        /// </summary>
        public long Id { get; set; }

        /// <summary>
        /// 磨损名称
        /// </summary>
        public string AbrasionName { get; set; }

        /// <summary>
        /// 新旧程度Ids
        /// </summary>
        public string NewOldIds { get; set; }

        /// <summary>
        /// 排序序号
        /// </summary>
        public int Sort { get; set; }
    }
}
