﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Car.Rcb.Dto
{
    /// <summary>
    /// 删除请求实体
    /// </summary>
    public class DeleteDataRequestModel
    {
        /// <summary>
        /// 主键Ids
        /// </summary>
        public List<long> Ids { get; set; }
    }
}
