﻿using Car.Framework;
using System;
using System.Collections.Generic;
using System.Text;

namespace Car.Rcb.Dto
{
    /// <summary>
    /// 数据启用/禁用请求实体
    /// </summary>
    public class EnabledOrDisabledDataRequestModel
    {
        /// <summary>
        /// 数据使用状态 0:启用 1:禁用
        /// </summary>
        public DataUseStatus DataUseStatus { get; set; }

        /// <summary>
        /// 主键Id
        /// </summary>
        public long Id { get; set; }
    }
}
