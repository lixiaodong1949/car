﻿using Car.Framework;
using System;
using System.Collections.Generic;
using System.Text;

namespace Car.Rcb.Dto
{
    /// <summary>
    /// 二手或求购文章审核请求实体
    /// </summary>
    public class UsedOrAskBuyArticleAuditRequestModel
    {
        /// <summary>
        /// 二手或求购主键Id
        /// </summary>
        public long UsedOrAskBuyId { get; set; }

        /// <summary>
        /// 审核操作状态 (1:审核成功 2:审核拒绝)
        /// </summary>
        public AuditStatus AuditStatus { get; set; }

        /// <summary>
        /// 操作理由
        /// </summary>
        public string OperateReason { get; set; }
    }
}
