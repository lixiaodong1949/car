﻿using Car.Framework;
using System;
using System.Collections.Generic;
using System.Text;

namespace Car.Rcb.Dto
{
    /// <summary>
    /// 二手或求购请求实体
    /// </summary>
    public class UsedOrAskBuyRequestModel
    {
        /// <summary>
        /// 二手或求购类型 (0:二手 1:求购)
        /// </summary>
        public UsedOrAskBuyType UsedOrAskBuyType { get; set; }

        /// <summary>
        /// 标题名称
        /// </summary>        
        public string TitleName { get; set; }

        /// <summary>
        /// 适用品牌Id
        /// </summary>
        public long BrandId { get; set; }

        /// <summary>
        /// 配件分类Id
        /// </summary>
        public long AccessoryTypeId { get; set; }

        /// <summary>
        /// 产品类型Id
        /// </summary>
        public long ProductTypeId { get; set; }

        /// <summary>
        /// 新旧程度Id
        /// </summary>
        public long NewOldDegreeId { get; set; }

        /// <summary>
        /// 磨损程度Id
        /// </summary>
        public long AbrasionDegreeId { get; set; }

        /// <summary>
        /// 出售原因
        /// </summary>        
        public string SaleReason { get; set; }

        /// <summary>
        /// 省级代码
        /// </summary>
        public string ProvinceCode { get; set; }

        /// <summary>
        /// 省级名称
        /// </summary>
        public string ProvinceName { get; set; }

        /// <summary>
        /// 市级代码
        /// </summary>
        public string CityCode { get; set; }

        /// <summary>
        /// 市级名称
        /// </summary>
        public string CityName { get; set; }

        /// <summary>
        /// 区级代码
        /// </summary>
        public string DistrictCode { get; set; }

        /// <summary>
        /// 区级名称
        /// </summary>
        public string DistrictName { get; set; }

        /// <summary>
        /// 详细地址
        /// </summary>
        public string DetailedAddress { get; set; }

        /// <summary>
        /// 联系电话
        /// </summary>
        public string MobilePhone { get; set; }

        /// <summary>
        /// 议价类型 (0:面议 1:否决)
        /// </summary>
        public BargainType BargainType { get; set; }

        /// <summary>
        /// 追加品牌
        /// </summary>
        public string BrandAdditional { get; set; }

        /// <summary>
        /// 二手或求购图片列表
        /// </summary>
        public List<UsedOrAskBuyPictureDetail> PictureList { get; set; } = new List<UsedOrAskBuyPictureDetail>();
    }

    /// <summary>
    /// 二手或求购图片实体
    /// </summary>
    public class UsedOrAskBuyPictureDetail
    {
        /// <summary>
        /// 图片地址
        /// </summary>
        public string PictureUrl { get; set; }
    }
}
