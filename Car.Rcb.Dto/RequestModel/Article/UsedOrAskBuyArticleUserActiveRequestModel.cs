﻿using Car.Framework;
using System;
using System.Collections.Generic;
using System.Text;

namespace Car.Rcb.Dto
{
    /// <summary>
    /// 二手或求购文章用户活跃请求实体
    /// </summary>
    public class UsedOrAskBuyArticleUserActiveRequestModel
    {
        /// <summary>
        /// 二手或求购主键Id
        /// </summary>
        public long UsedOrAskBuyId { get; set; }

        /// <summary>
        /// 操作类型
        /// </summary>
        public UserActiveOperationType OperationType { get; set; }
    }
}
