﻿using Car.Framework;
using System;
using System.Collections.Generic;
using System.Text;

namespace Car.Rcb.Dto
{
    /// <summary>
    /// 技术文章用户活跃请求实体
    /// </summary>
    public class TechnicalArticleUserActiveRequestModel
    {
        /// <summary>
        /// 文章主键Id
        /// </summary>
        public long ArticleId { get; set; }

        /// <summary>
        /// 操作类型
        /// </summary>
        public UserActiveOperationType OperationType { get; set; }
    }
}
