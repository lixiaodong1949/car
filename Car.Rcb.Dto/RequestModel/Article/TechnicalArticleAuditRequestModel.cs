﻿using Car.Framework;
using System;
using System.Collections.Generic;
using System.Text;

namespace Car.Rcb.Dto
{
    /// <summary>
    /// 技术文章审核请求实体
    /// </summary>
    public class TechnicalArticleAuditRequestModel
    {
        /// <summary>
        /// 文章主键Id
        /// </summary>
        public long ArticleId { get; set; }

        /// <summary>
        /// 审核操作状态 (1:审核成功 2:审核拒绝)
        /// </summary>
        public AuditStatus AuditStatus { get; set; }

        /// <summary>
        /// 操作理由
        /// </summary>
        public string OperateReason { get; set; }
    }
}
