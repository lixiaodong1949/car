﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Car.Rcb.Dto
{
    /// <summary>
    /// 技术文章上传请求实体
    /// </summary>
    public class TechnicalArticleRequestModel
    {
        /// <summary>
        /// 标题名称
        /// </summary>
        public string TitleName { get; set; }

        /// <summary>
        /// 文章类型
        /// </summary>
        public long TechnologyTagId { get; set; }

        /// <summary>
        /// 是否是原创 false:否 true:是
        /// </summary>
        public bool IsOriginal { get; set; }

        /// <summary>
        /// 段落列表
        /// </summary>
        public List<ParagraphDetail> ParagraphList { get; set; } = new List<ParagraphDetail>();
    }

    /// <summary>
    /// 段落详情实体
    /// </summary>
    public class ParagraphDetail
    {
        /// <summary>
        /// 段落内容
        /// </summary>     
        public string Content { get; set; }

        /// <summary>
        /// 段落图片列表
        /// </summary>
        public List<ParagraphPictureDetail> PictureList { get; set; } = new List<ParagraphPictureDetail>();
    }

    /// <summary>
    /// 段落图片实体
    /// </summary>
    public class ParagraphPictureDetail
    {
        /// <summary>
        /// 图片地址
        /// </summary>
        public string PictureUrl { get; set; }
    }

}
