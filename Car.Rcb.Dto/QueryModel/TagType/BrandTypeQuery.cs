﻿using Car.Framework;
using Car.Framework.Entity;
using System;
using System.Collections.Generic;
using System.Text;

namespace Car.Rcb.Dto
{
    /// <summary>
    /// 品牌类型查询实体
    /// </summary>
    public class BrandTypeQuery : PageRequest
    {
        /// <summary>
        /// 品牌名称
        /// </summary>
        public string BrandName { get; set; }

        /// <summary>
        /// 数据使用状态 0:启用 1:禁用
        /// </summary>
        public Nullable<DataUseStatus> DataUseStatus { get; set; }
    }
}
