﻿using Car.Framework;
using Car.Framework.Entity;
using System;
using System.Collections.Generic;
using System.Text;

namespace Car.Rcb.Dto
{
    /// <summary>
    /// 新旧程度查询实体
    /// </summary>
    public class NewOldDegreeQuery : PageRequest
    {
        /// <summary>
        /// 新旧名称
        /// </summary>
        public string NewOldName { get; set; }

        /// <summary>
        /// 数据使用状态 0:启用 1:禁用
        /// </summary>
        public Nullable<DataUseStatus> DataUseStatus { get; set; }
    }
}
