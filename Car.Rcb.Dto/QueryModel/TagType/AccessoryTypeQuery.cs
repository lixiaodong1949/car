﻿using Car.Framework;
using Car.Framework.Entity;
using System;
using System.Collections.Generic;
using System.Text;

namespace Car.Rcb.Dto
{
    /// <summary>
    /// 配件分类查询实体
    /// </summary>
    public class AccessoryTypeQuery : PageRequest
    {
        /// <summary>
        /// 配件名称
        /// </summary>
        public string AccessoryName { get; set; }

        /// <summary>
        /// 数据使用状态 0:启用 1:禁用
        /// </summary>
        public Nullable<DataUseStatus> DataUseStatus { get; set; }
    }
}
