﻿using Car.Framework;
using Car.Framework.Entity;
using System;
using System.Collections.Generic;
using System.Text;

namespace Car.Rcb.Dto
{
    /// <summary>
    /// 产品类型查询实体
    /// </summary>
    public class ProductTypeQuery : PageRequest
    {
        /// <summary>
        /// 配件分类Id
        /// </summary>
        public long AccessoryId { get; set; }

        /// <summary>
        /// 产品名称
        /// </summary>
        public string ProductName { get; set; }

        /// <summary>
        /// 数据使用状态 0:启用 1:禁用
        /// </summary>
        public Nullable<DataUseStatus> DataUseStatus { get; set; }
    }
}
