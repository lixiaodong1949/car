﻿using Car.Framework;
using Car.Framework.Entity;
using System;
using System.Collections.Generic;
using System.Text;

namespace Car.Rcb.Dto
{
    /// <summary>
    /// 个人的二手或求购文章查询实体(发布、收藏)
    /// </summary>
    public class UsedOrAskBuyArticleOneselfQuery : PageRequest
    {
        /// <summary>
        /// 二手或求购类型 (0:二手 1:求购)
        /// </summary>
        public UsedOrAskBuyType UsedOrAskBuyType { get; set; }

        /// <summary>
        /// 标题名称
        /// </summary>
        public string TitleName { get; set; }

        /// <summary>
        /// 适用品牌Id
        /// </summary>
        public Nullable<long> BrandId { get; set; }

        /// <summary>
        /// 配件分类Id
        /// </summary>
        public Nullable<long> AccessoryTypeId { get; set; }

        /// <summary>
        /// 产品类型Id
        /// </summary>
        public Nullable<long> ProductTypeId { get; set; }

        /// <summary>
        /// 新旧程度Id
        /// </summary>
        public Nullable<long> NewOldDegreeId { get; set; }

        /// <summary>
        /// 磨损程度Id
        /// </summary>
        public Nullable<long> AbrasionDegreeId { get; set; }

        /// <summary>
        /// 省级代码
        /// </summary>
        public string ProvinceCode { get; set; }

        /// <summary>
        /// 市级代码
        /// </summary>
        public string CityCode { get; set; }

        /// <summary>
        /// 区级代码
        /// </summary>
        public string DistrictCode { get; set; }

        /// <summary>
        /// 议价类型 (0:面议 1:否决)
        /// </summary>
        public Nullable<BargainType> BargainType { get; set; }

        /// <summary>
        /// 资源来源 0:自己发布 1:收藏
        /// </summary>
        public ResourcePersonSource ResourceSource { get; set; }
    }
}
