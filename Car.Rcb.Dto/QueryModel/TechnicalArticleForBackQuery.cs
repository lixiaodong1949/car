﻿using Car.Framework;
using Car.Framework.Entity;
using System;
using System.Collections.Generic;
using System.Text;

namespace Car.Rcb.Dto
{
    /// <summary>
    /// 技术文章后台查询实体
    /// </summary>
    public class TechnicalArticleForBackQuery : PageRequest
    {
        /// <summary>
        /// 文章类型主键
        /// </summary>
        public Nullable<long> TagId { get; set; }

        /// <summary>
        /// 标题名称
        /// </summary>        
        public string TitleName { get; set; }

        /// <summary>
        /// 审核状态 (0:发布待审 1:审核成功 2:审核拒绝)
        /// </summary>
        public Nullable<AuditStatus> AuditStatus { get; set; }

        /// <summary>
        /// 上架状态 (0:未上架 1:已上架 2:已下架)
        /// </summary>
        public Nullable<HitShelfStatus> HitShelfStatus { get; set; }
    }
}
