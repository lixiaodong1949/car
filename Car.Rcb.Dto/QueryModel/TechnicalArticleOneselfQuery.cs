﻿using Car.Framework;
using Car.Framework.Entity;
using System;
using System.Collections.Generic;
using System.Text;

namespace Car.Rcb.Dto
{
    /// <summary>
    /// 个人的技术文章查询实体(发布、收藏)
    /// </summary>
    public class TechnicalArticleOneselfQuery : PageRequest
    {
        /// <summary>
        /// 文章类型主键
        /// </summary>
        public Nullable<long> TagId { get; set; }

        /// <summary>
        /// 标题名称
        /// </summary>        
        public string TitleName { get; set; }

        /// <summary>
        /// 资源来源 0:自己发布 1:收藏
        /// </summary>
        public ResourcePersonSource ResourceSource { get; set; }
    }
}
