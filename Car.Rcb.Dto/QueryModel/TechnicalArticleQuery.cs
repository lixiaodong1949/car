﻿using Car.Framework.Entity;
using System;
using System.Collections.Generic;
using System.Text;

namespace Car.Rcb.Dto
{
    /// <summary>
    /// 技术文章查询实体
    /// </summary>
    public class TechnicalArticleQuery : PageRequest
    {
        /// <summary>
        /// 文章类型主键
        /// </summary>
        public Nullable<long> TagId { get; set; }
        
        /// <summary>
        /// 标题名称
        /// </summary>        
        public string TitleName { get; set; }

        ///// <summary>
        ///// 是否是自己发布的文章
        ///// </summary>
        //public bool IsOneself { get; set; }
    }
}
