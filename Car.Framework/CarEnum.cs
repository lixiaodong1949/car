﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Text;

namespace Car.Framework
{
    #region 用户类型
    /// <summary>
    /// 用户类型
    /// </summary>
    public enum UserStatus
    {
        /// <summary>
        /// 启用
        /// </summary>
        [Description("启用")]
        Enabled = 0,

        /// <summary>
        /// 禁用
        /// </summary>
        [Description("禁用")]
        Disabled = 1,
    }
    #endregion

    #region 数据使用状态
    /// <summary>
    /// 数据使用状态
    /// </summary>
    public enum DataUseStatus
    {
        /// <summary>
        /// 启用
        /// </summary>
        [Description("启用")]
        Enabled = 0,

        /// <summary>
        /// 禁用
        /// </summary>
        [Description("禁用")]
        Disabled = 1,
    }
    #endregion

    #region 审核状态
    /// <summary>
    ///  审核状态
    /// </summary>
    public enum AuditStatus
    {
        /// <summary>
        /// 发布待审
        /// </summary>
        [Description("发布待审")]
        Pending = 0,

        /// <summary>
        /// 审核成功
        /// </summary>
        [Description("审核成功")]
        AuditSuccess = 1,

        /// <summary>
        /// 审核拒绝
        /// </summary>
        [Description("审核拒绝")]
        AuditRefuse = 2,
    }
    #endregion

    #region 上架状态
    /// <summary>
    /// 上架状态
    /// </summary>
    public enum HitShelfStatus
    {
        /// <summary>
        /// 未上架
        /// </summary>
        [Description("未上架")]
        NoHitShelf = 0,

        /// <summary>
        /// 已上架
        /// </summary>
        [Description("已上架")]
        HitShelf = 1,

        /// <summary>
        /// 已下架
        /// </summary>
        [Description("已下架")]
        OutStock = 2,
    }
    #endregion

    #region 用户活跃操作类型
    /// <summary>
    /// 用户活跃操作类型
    /// </summary>
    public enum UserActiveOperationType
    {
        /// <summary>
        /// 查看
        /// </summary>
        [Description("查看")]
        Preview = 0,

        /// <summary>
        /// 收藏
        /// </summary>
        [Description("收藏")]
        Collect = 1,

        /// <summary>
        /// 点赞
        /// </summary>
        [Description("点赞")]
        Like = 2,
    }
    #endregion

    #region 二手或求购类型
    /// <summary>
    /// 二手或求购类型
    /// </summary>
    public enum UsedOrAskBuyType
    {
        /// <summary>
        /// 二手
        /// </summary>
        [Description("二手")]
        Used = 0,

        /// <summary>
        /// 求购
        /// </summary>
        [Description("求购")]
        AskBuy = 1,
    }
    #endregion

    #region 议价类型
    /// <summary>
    /// 议价类型
    /// </summary>
    public enum BargainType
    {
        /// <summary>
        /// 面议
        /// </summary>
        [Description("面议")]
        Negotiable = 0,

        /// <summary>
        /// 否决
        /// </summary>
        [Description("否决")]
        Veto = 1,
    }
    #endregion

    #region 资源个人操作类型
    /// <summary>
    /// 资源个人操作类型
    /// </summary>
    public enum ResourcePersonSource
    {
        /// <summary>
        /// 自己发布
        /// </summary>
        [Description("自己发布")]
        DoOneOwn = 0,

        /// <summary>
        /// 收藏
        /// </summary>
        [Description("收藏")]
        Collect = 1,
    }
    #endregion

}
