﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Car.Framework.Authorizations
{
    /// <summary>
    /// 返回用户信息
    /// </summary>
    public class UserTicket
    {
        /// <summary>
        /// 用户主键Id
        /// </summary>
        public long Id { get; set; }

        /// <summary>
        /// 昵称
        /// </summary>
        public string NickName { get; set; }

        /// <summary>
        /// 真实姓名
        /// </summary>

        public string RealName { get; set; }

        /// <summary>
        /// 头像链接
        /// </summary>
        public string PictureLink { get; set; }

        /// <summary>
        /// token
        /// </summary>
        public string AccessToken { get; set; }

        /// <summary>
        /// 电话号码
        /// </summary>
        public string MobilePhone { get; set; }
    }
}
