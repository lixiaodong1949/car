﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Car.Framework
{
    public static class CommonConstants
    {
        /// <summary>
        /// 成功
        /// </summary>
        public const int SuccessCode = 200;
        
        /// <summary>
        ///  未登录
        /// </summary>       
        public const int Unauthorized = 401;

        /// <summary>
        /// 未找到
        /// </summary>
        public const int NotFund = 404;

        /// <summary>
        /// 服务器错误
        /// </summary>
        public const int ErrorCode = 500;

        /// <summary>
        /// 参数错误
        /// </summary>
        public const int BadRequest = 400;

        /// <summary>
        /// 真实姓名
        /// </summary>
        public const string ClaimsNickName = "NickName";

        /// <summary>
        /// 真实姓名
        /// </summary>
        public const string ClaimsRealName = "RealName";

        /// <summary>
        /// 头像链接
        /// </summary>
        public const string ClaimsPictureLink = "PictureLink";

        /// <summary>
        /// 未删除
        /// </summary>
        public const int IsNotDelete = 0;

        /// <summary>
        /// 已删除
        /// </summary>
        public const int IsDelete = 1;

        /// <summary>
        /// 
        /// </summary>
        public const string ContextUserPropertyKey = "REQUEST_USER";

        /// <summary>
        /// 
        /// </summary>
        public const string IdentServerSecret = "RenCheBang_123~#$%#%^321";
        
        // <summary>
        /// cors
        /// </summary>
        public const string AnyOrigin = "AnyOrigin";
    }
}
