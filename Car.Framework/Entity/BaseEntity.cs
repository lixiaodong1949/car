﻿using FreeSql.DataAnnotations;
using System;
using System.Collections.Generic;
using System.Text;

namespace Car.Framework.Entity
{
    public abstract class BaseEntity<TPKey> : Entity
    {
        [Column(Name = "Id", IsPrimary = true)]
        public virtual TPKey Id { get; set; }
    }
}
