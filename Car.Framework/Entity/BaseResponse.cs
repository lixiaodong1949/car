﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Car.Framework.Entity
{
    public class BaseResponse
    {
        /// <summary>
        /// 返回状态码
        /// </summary>
        public int Code { get; set; } = CommonConstants.SuccessCode;
        /// <summary>
        /// 返回消息
        /// </summary>
        public string Msg { get; set; }

        /// <summary>
        /// 设置成功
        /// </summary>
        /// <param name="msg"></param>
        public virtual void SetSuccess(string msg = null)
        {
            Code = CommonConstants.SuccessCode;
            Msg = msg;
        }

        /// <summary>
        /// 设置失败
        /// </summary>
        /// <param name="msg"></param>
        /// <param name="code"></param>
        public virtual void SetError(string msg, int code = CommonConstants.BadRequest)
        {
            Code = code;
            Msg = msg;
        }

    }
}
