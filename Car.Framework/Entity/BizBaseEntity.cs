﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Car.Framework.Entity
{
    public abstract class BizBaseEntity<TPKey> : BaseEntity<TPKey>
    {
        /// <summary>
        /// 是否删除
        /// </summary>
        public int IsDelete { get; set; } = CommonConstants.IsNotDelete;

        /// <summary>
        /// 创建人
        /// </summary>
        public long CreateBy { get; set; }

        /// <summary>
        /// 创建时间
        /// </summary>
        public DateTime CreateTime { get; set; } = DateTime.Now;

        /// <summary>
        /// 更新者
        /// </summary>
        public long UpdateBy { get; set; }

        /// <summary>
        /// 更新时间
        /// </summary>
        public DateTime? UpdateTime { get; set; }
    }
}
