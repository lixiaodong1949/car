﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Car.Framework.Entity
{
    public class PageResponse<T> : BaseResponse where T : class
    {
        public PageResponse()
        {

        }

        public PageResponse(int pageIndex, int pageSize, long recordCount, List<T> result)
        {
            RecordCount = recordCount;
            PageIndex = pageIndex;
            PageSize = pageSize;
            Data = result;
        }

        /// <summary>
        /// 
        /// </summary>
        public long RecordCount { get; set; }

        /// <summary>
        /// 总行数
        /// </summary>
        public long Count { get { return RecordCount; } }

        /// <summary>
        /// 
        /// </summary>
        public int PageCount => PageSize == 0 ? 0 : (int)Math.Ceiling((decimal)RecordCount / PageSize);

        /// <summary>
        /// 
        /// </summary>
        public int PageIndex { get; set; } = 1;

        /// <summary>
        /// 
        /// </summary>
        public int PageSize { get; set; } = 20;

        /// <summary>
        /// 
        /// </summary>
        public List<T> Data { get; set; }
    }
}
