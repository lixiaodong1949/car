﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Car.Framework.Entity
{
    public class PageRequest
    {
        public PageRequest()
        {

        }
        public PageRequest(int pageIndex, int pageSize)
        {
            PageIndex = pageIndex;
            PageSize = pageSize;
        }



        private int _pageIndex;
        /// <summary>
        /// 页码
        /// </summary>
        public int PageIndex
        {
            get
            {
                if (_pageIndex <= 0)
                    _pageIndex = 1;
                return _pageIndex;
            }
            set { _pageIndex = value; }
        }

        private int _pageSize;
        /// <summary>
        /// 每页数量
        /// </summary>
        public int PageSize
        {
            get
            {
                if (_pageSize <= 0 || _pageSize > 10000)
                    _pageSize = 20;
                return _pageSize;
            }
            set { _pageSize = value; }
        }
    }

}
