﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Car.Framework.Dal
{
    /// <summary>
    /// 数据库上下文
    /// </summary>
    public static class DbContext
    {
        /// <summary>
        /// 数据库上下文单例
        /// </summary>
        public static IFreeSql FreeSql { get; private set; }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="freeSql"></param>
        internal static void RegisterFreeSql(IFreeSql freeSql)
        {
            FreeSql = freeSql;
        }
    }
}
