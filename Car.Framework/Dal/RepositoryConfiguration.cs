﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Text;
using Tas.Common.Configurations;

namespace Car.Framework.Dal
{
    public static class RepositoryConfiguration
    {
        #region 数据库连接
        /// <summary>
        /// 数据库连接
        /// </summary>
        /// <param name="projectConfiguration"></param>
        /// <param name="sqlConnection"></param>
        /// <param name="slaveConnectionString"></param>
        /// <returns></returns>
        public static ProjectConfiguration UseFreeSqlRepository(this ProjectConfiguration projectConfiguration, string sqlConnection, params string[] slaveConnectionString)
        {
            var fsql = new FreeSql.FreeSqlBuilder()
                .UseConnectionString(FreeSql.DataType.MySql, sqlConnection)
                .UseSlave(slaveConnectionString)
                                            .UseAutoSyncStructure(true) //自动同步实体结构【开发环境必备】                
                                                                        //.UseLogger(CommonLogger.CreateLogger("UseFreeSqlRepository"))
                                            .UseMonitorCommand(a =>
                                            {
                                                Trace.WriteLine(a.CommandText);
                                            })
                .Build();
            fsql.Aop.CurdAfter = (s, e) =>
            {
                Trace.WriteLine($"aop after {e.Sql}");
            };
            fsql.Aop.ConfigEntityProperty = (s, e) =>
            {
                if (e.Property.PropertyType.IsEnum)
                    e.ModifyResult.MapType = typeof(int);
            };

            DbContext.RegisterFreeSql(fsql);

            return projectConfiguration;
        }
        #endregion

    }
}
