﻿using Car.Framework.Authorizations;
using IdentityModel;
using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using System.Text;

namespace Car.Framework.Auth
{
    /// <summary>
    /// JWTToken生成类
    /// </summary>
    public class JwtTokenHelper
    {
        /// <summary>
        /// 获取基于JWT的Token
        /// </summary> 
        /// <returns></returns>
        public static UserTicket BuildJwtToken(UserTicket ticket, PermissionRequirement permissionRequirement)
        {
            var claims = new Claim[]
                {
                    new Claim(JwtClaimTypes.Id, ticket.Id.ToString()),
                    new Claim(CommonConstants.ClaimsNickName, ticket.NickName??string.Empty),
                    new Claim(CommonConstants.ClaimsRealName, ticket.RealName??string.Empty),
                    new Claim(CommonConstants.ClaimsPictureLink, ticket.PictureLink??string.Empty),
                };
            var jwt = new JwtSecurityToken(claims: claims, signingCredentials: permissionRequirement.SigningCredentials);
            var token = new JwtSecurityTokenHandler().WriteToken(jwt);
            ticket.AccessToken = token;
            return ticket;
        }


    }
}
