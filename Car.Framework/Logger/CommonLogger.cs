﻿using System;
using System.Text;
using Microsoft.Extensions.Logging;

namespace Car.Framework.Logger
{
    /// <summary>
    /// 日志帮助类
    /// </summary>
    public static class CommonLogger
    {
        /// <summary>
        /// 
        /// </summary>
        private static ILogger _log;
        /// <summary>
        /// 
        /// </summary>
        private static ILoggerFactory _loggerFactory;
        /// <summary>
        /// 
        /// </summary>
        /// <param name="loggerFactory"></param>
        internal static void RegisterLoggerFactory(ILoggerFactory loggerFactory)
        {
            _loggerFactory = loggerFactory;
            _log = _loggerFactory.CreateLogger("CommonLogger");
        }
        /// <summary>
        /// 
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <returns></returns>
        public static ILogger CreateLogger<T>()
        {
            return _loggerFactory?.CreateLogger<T>();
        }
        /// <summary>
        /// 
        /// </summary> 
        /// <returns></returns>
        public static ILogger CreateLogger(string categoryName)
        {
            return _loggerFactory?.CreateLogger(categoryName);
        }
        /// <summary>
        /// 写日志
        /// </summary>
        /// <param name="message">内容</param>
        /// <param name="level">等级</param>
        /// <param name="ex">异常</param>
        public static void WriteLog(string message, LogLevel level, Exception ex)
        {
            if (_log == null)
                throw new Exception("未注册日志组件");
            var sb = new StringBuilder();
            sb.Append("\r\n\t" + message);
            if (ex != null)
            {
                sb.Append("\r\n\t错误信息：" + ex.Message);
                sb.Append("\r\n\t错误源：" + ex.Source);
                sb.Append("\r\n\t异常方法：" + ex.TargetSite);
                sb.Append("\r\n\t堆栈信息：" + ex.StackTrace);
            }
            var logStr = sb.ToString();
            switch (level)
            {
                case LogLevel.Information:
                    _log.LogInformation(logStr);
                    break;
                case LogLevel.Error:
                    _log.LogError(logStr);
                    break;
                case LogLevel.Warning:
                    _log.LogWarning(logStr);
                    break;
                case LogLevel.Debug:
                    _log.LogDebug(logStr);
                    break;
            }
        }

        /// <summary>
        /// 记录错误日志
        /// </summary>
        /// <param name="p">错误信息</param>
        public static void Error(string p)
        {
            WriteLog(p, LogLevel.Error, null);
        }

        /// <summary>
        /// 记录错误日志
        /// </summary>
        /// <param name="p">错误信息</param>
        /// <param name="e">异常信息</param>
        public static void Error(string p, Exception e)
        {
            WriteLog(p, LogLevel.Error, e);
        }

        /// <summary>
        /// 记录错误日志
        /// </summary>
        /// <param name="p">错误信息</param>
        /// <param name="args">格式化参数</param>
        public static void Error(string p, params object[] args)
        {
            WriteLog(string.Format(p, args), LogLevel.Error, null);
        }

        /// <summary>
        /// 记录信息日志
        /// </summary>
        /// <param name="p">信息</param>
        public static void Info(string p)
        {
            WriteLog(p, LogLevel.Information, null);
        }

        /// <summary>
        /// 记录信息日志
        /// </summary>
        /// <param name="p">信息</param>
        /// <param name="args">格式化数据</param>
        public static void Info(string p, params object[] args)
        {
            WriteLog(string.Format(p, args), LogLevel.Information, null);
        }

        /// <summary>
        /// 记录警告日志
        /// </summary>
        /// <param name="p">警告信息</param>
        public static void Warn(string p)
        {
            WriteLog(p, LogLevel.Warning, null);
        }

        /// <summary>
        /// 记录警告日志
        /// </summary>
        /// <param name="p">警告信息</param>
        /// <param name="e">异常信息</param>
        public static void Warn(string p, Exception e)
        {
            WriteLog(p, LogLevel.Warning, e);
        }

        /// <summary>
        /// 记录警告日志
        /// </summary>
        /// <param name="p">警告信息</param>
        /// <param name="args">格式化数据</param>
        public static void Warn(string p, params object[] args)
        {
            WriteLog(string.Format(p, args), LogLevel.Warning, null);
        }

        /// <summary>
        /// 记录Debug信息
        /// </summary>
        /// <param name="p">Debug信息</param>
        public static void Debug(string p)
        {
            WriteLog(p, LogLevel.Debug, null);
        }

        /// <summary>
        /// 记录Debug信息
        /// </summary>
        /// <param name="p">Debug信息</param>
        /// <param name="args">格式化数据</param>
        public static void Debug(string p, params object[] args)
        {
            WriteLog(string.Format(p, args), LogLevel.Debug, null);
        }
    }
}
