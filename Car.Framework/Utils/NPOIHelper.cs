﻿using Car.Framework.Logger;
using NPOI.HSSF.UserModel;
using NPOI.SS.UserModel;
using NPOI.XSSF.UserModel;
using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;

namespace Car.Framework.Utils
{
    public class NPOIHelper<T> where T : class
    {
        public static List<T> ImportExcelData(Stream ms, string ee)
        {
            //DataTable dt=ExcelToDataTable(ms, true);
            List<T> list = new List<T>(); //ToDataList<T>(dt);
            IWorkbook workbook = null;
            if (ee == "xls")
            {
                workbook = new HSSFWorkbook(ms);
            }
            else
            {
                workbook = new XSSFWorkbook(ms);
            }

            ISheet sheet = workbook.GetSheetAt(0);
            IRow cellNum = sheet.GetRow(0);

            var propertys = new List<PropertyInfo>(typeof(T).GetProperties());
            string value = null;
            int num = cellNum.LastCellNum;
            IRow row2 = sheet.GetRow(0);
            //sheet.GroupRow(0, 10);
            for (int i = 1; i <= sheet.LastRowNum; i++)
            {
                IRow row = sheet.GetRow(i);
                T obj = System.Activator.CreateInstance<T>();

                for (int j = 0; j < num; j++)
                {
                    string c_name = row2.GetCell(j).ToString();
                    value = row.GetCell(j).ToString();
                    PropertyInfo info = propertys.Find(p => ((DescriptionAttribute[])p.GetCustomAttributes(typeof(DescriptionAttribute), false))[0].Description == c_name);
                    if (info != null)
                    {
                        try
                        {
                            if (!Convert.IsDBNull(value))
                            {
                                object v = null;
                                if (info.PropertyType.ToString().Contains("System.Nullable"))
                                {
                                    v = Convert.ChangeType(value, Nullable.GetUnderlyingType(info.PropertyType));
                                }
                                else
                                {
                                    v = Convert.ChangeType(value, info.PropertyType);
                                }
                                info.SetValue(obj, v, null);
                            }
                        }
                        catch (Exception ex)
                        {
                            CommonLogger.Error("nlog", new Exception("字段[" + info.Name + "]转换出错," + ex.Message));
                        }
                    }
                }
                list.Add(obj);
            }

            return list;
        }

        public static List<T> ImportExcelData2(Stream ms, string suffix)
        {
            try
            {
                //DataTable dt=ExcelToDataTable(ms, true);
                List<T> list = new List<T>(); //ToDataList<T>(dt);
                IWorkbook workbook = new XSSFWorkbook(ms);
                if (suffix == "xls")
                {
                    workbook = new HSSFWorkbook(ms);
                }
                else
                {
                    workbook = new XSSFWorkbook(ms);
                }
                ISheet sheet = workbook.GetSheetAt(0);
                IRow cellNum = sheet.GetRow(0);
                var propertys = new List<PropertyInfo>(typeof(T).GetProperties());
                string value = null;
                int num = cellNum.LastCellNum;
                IRow row2 = sheet.GetRow(0);
                for (int i = 1; i <= sheet.LastRowNum; i++)
                {
                    IRow row = sheet.GetRow(i);

                    T obj = System.Activator.CreateInstance<T>();
                    bool isNull = true;
                    for (int j = 0; j < num; j++)
                    {
                        string c_name = row2.GetCell(j).ToString();
                        ICell cell = null;
                        if (row != null)
                        {
                            cell = row.GetCell(j);
                        }

                        //value = row.GetCell(j).ToString();
                        if (cell != null)
                        {
                            value = cell.ToString();
                            if (!string.IsNullOrEmpty(value))
                            {
                                isNull = false;
                            }
                            if (cell.CellType == CellType.Numeric)
                            {
                                if (DateUtil.IsCellDateFormatted(cell))//日期类型
                                {
                                    value = cell.DateCellValue.ToString("yyyy-MM-dd HH:mm:ss");

                                }
                                else//其他数字类型
                                {
                                    value = cell.NumericCellValue.ToString();

                                }
                            }
                        }
                        else
                        {
                            value = null;
                        }

                        PropertyInfo info = propertys.Find(p => ((DescriptionAttribute[])p.GetCustomAttributes(typeof(DescriptionAttribute), false))[0].Description == c_name);
                        if (info != null)
                        {
                            try
                            {
                                if (!Convert.IsDBNull(value))
                                {
                                    object v = null;
                                    if (info.PropertyType.ToString().Contains("System.Nullable"))
                                    {
                                        v = Convert.ChangeType(value, Nullable.GetUnderlyingType(info.PropertyType));
                                    }
                                    else
                                    {
                                        v = Convert.ChangeType(value, info.PropertyType);
                                    }
                                    info.SetValue(obj, v, null);
                                }
                            }
                            catch (Exception ex)
                            {
                                CommonLogger.Error("nlog", new Exception("字段[" + info.Name + "]转换出错," + ex.Message));
                            }
                        }
                    }
                    if (!isNull)
                        list.Add(obj);
                }
                return list;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.StackTrace + "[]" + ex.Message);
            }
        }
        public static DataTable ExcelToDataTable(Stream ms, bool isFirstRowColumn)
        {
            ISheet sheet = null;
            DataTable data = new DataTable();
            int startRow = 0;
            try
            {


                IWorkbook workbook = new XSSFWorkbook(ms);

                sheet = workbook.GetSheetAt(0);

                if (sheet != null)
                {
                    IRow firstRow = sheet.GetRow(0);
                    int cellCount = firstRow.LastCellNum; //一行最后一个cell的编号 即总的列数

                    if (isFirstRowColumn)
                    {
                        for (int i = firstRow.FirstCellNum; i < cellCount; ++i)
                        {
                            ICell cell = firstRow.GetCell(i);
                            if (cell != null)
                            {
                                string cellValue = cell.StringCellValue;
                                if (cellValue != null)
                                {
                                    DataColumn column = new DataColumn(cellValue);
                                    data.Columns.Add(column);
                                }
                            }
                        }
                        startRow = sheet.FirstRowNum + 1;
                    }
                    else
                    {
                        startRow = sheet.FirstRowNum;
                    }

                    //最后一列的标号
                    int rowCount = sheet.LastRowNum;
                    for (int i = startRow; i <= rowCount; ++i)
                    {
                        IRow row = sheet.GetRow(i);
                        if (row == null) continue; //没有数据的行默认是null　　　　　　　

                        DataRow dataRow = data.NewRow();
                        for (int j = row.FirstCellNum; j < cellCount; ++j)
                        {
                            if (row.GetCell(j) != null) //同理，没有数据的单元格都默认是null
                                dataRow[j] = row.GetCell(j).ToString();
                        }
                        data.Rows.Add(dataRow);
                    }
                }

                return data;
            }
            catch (Exception ex)
            {
                Console.WriteLine("Exception: " + ex.Message);
                return null;
            }
        }

        public static List<T> ToDataList(DataTable dt)
        {
            var list = new List<T>();
            var plist = new List<PropertyInfo>(typeof(T).GetProperties());

            foreach (DataRow item in dt.Rows)
            {
                T s = Activator.CreateInstance<T>();
                for (int i = 0; i < dt.Columns.Count; i++)
                {
                    //var va = (DescriptionAttribute[])plist[i].GetCustomAttributes(typeof(DescriptionAttribute), false);
                    //var desc = va[0].Description;
                    PropertyInfo info = plist.Find(p => ((DescriptionAttribute[])p.GetCustomAttributes(typeof(DescriptionAttribute), false))[0].Description == dt.Columns[i].ColumnName);
                    if (info != null)
                    {
                        try
                        {
                            if (!Convert.IsDBNull(item[i]))
                            {
                                object v = null;
                                if (info.PropertyType.ToString().Contains("System.Nullable"))
                                {
                                    v = Convert.ChangeType(item[i], Nullable.GetUnderlyingType(info.PropertyType));
                                }
                                else
                                {
                                    v = Convert.ChangeType(item[i], info.PropertyType);
                                }
                                info.SetValue(s, v, null);
                            }
                        }
                        catch (Exception ex)
                        {
                            throw new Exception("字段[" + info.Name + "]转换出错," + ex.Message);
                        }
                    }
                }
                list.Add(s);
            }
            return list;
        }


        public static DataTable ToDataTable(IEnumerable<T> collection)
        {
            var props = typeof(T).GetProperties();

            var dt = new DataTable();

            dt.Columns.AddRange(props.Select(p => new DataColumn(((DescriptionAttribute[])p.GetCustomAttributes(typeof(DescriptionAttribute), false))[0].Description, p.PropertyType)).ToArray());

            if (collection.Count() > 0)

            {

                for (int i = 0; i < collection.Count(); i++)

                {

                    ArrayList tempList = new ArrayList();

                    foreach (PropertyInfo pi in props)

                    {

                        object obj = pi.GetValue(collection.ElementAt(i), null);

                        tempList.Add(obj);

                    }

                    object[] array = tempList.ToArray();

                    dt.LoadDataRow(array, true);

                }

            }

            return dt;

        }

        public static byte[] OutputExcel(List<T> entitys, string[] title)
        {
            NPOI.SS.UserModel.IWorkbook workbook = new NPOI.XSSF.UserModel.XSSFWorkbook();

            Type entityType = entitys[0].GetType();
            PropertyInfo[] entityProperties = entityType.GetProperties();

            DataTable dt = ToDataTable(entitys);
            byte[] buffer = new byte[1024 * 2];
            if (dt.Rows.Count > 0)
            {
                NPOI.SS.UserModel.ISheet sheet = workbook.CreateSheet("data_001");

                // 添加表头
                NPOI.SS.UserModel.IRow row = sheet.CreateRow(0);
                int index = 0;
                foreach (DataColumn item in dt.Columns)
                {

                    NPOI.SS.UserModel.ICell cell = row.CreateCell(index);
                    cell.SetCellType(NPOI.SS.UserModel.CellType.String);
                    cell.SetCellValue(item.Caption);
                    index++;

                }

                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    index = 0;
                    row = sheet.CreateRow(i + 1);
                    for (int k = 0; k < dt.Columns.Count; k++)
                    {

                        NPOI.SS.UserModel.ICell cell = row.CreateCell(index);
                        cell.SetCellType(NPOI.SS.UserModel.CellType.String);
                        cell.SetCellValue(dt.Rows[i][k].ToString());
                        index++;

                    }
                }
            }


            using (MemoryStream ms = new MemoryStream())
            {
                workbook.Write(ms);
                buffer = ms.ToArray();
                ms.Close();
            }

            return buffer;
        }

        public static List<T> ImportNaturalPersons(Stream ms, string ee)
        {
            List<T> list = new List<T>();
            IWorkbook workbook = null;
            if (ee == "xls")
            {
                workbook = new HSSFWorkbook(ms);
            }
            else
            {
                workbook = new XSSFWorkbook(ms);
            }

            ISheet sheet = workbook.GetSheetAt(0);
            IRow cellNum = sheet.GetRow(0);

            var propertys = new List<PropertyInfo>(typeof(T).GetProperties());
            string value = null;
            int num = cellNum.LastCellNum;
            IRow row2 = sheet.GetRow(0);

            for (int i = 1; i <= sheet.LastRowNum; i++)
            {
                IRow row = sheet.GetRow(i);
                T obj = System.Activator.CreateInstance<T>();

                for (int j = 0; j < num; j++)
                {
                    string c_name = row2.GetCell(j).ToString();
                    if (row.GetCell(j) != null)
                        value = row.GetCell(j).ToString();
                    else
                        value = string.Empty;

                    PropertyInfo info = propertys.Find(p => ((DescriptionAttribute[])p.GetCustomAttributes(typeof(DescriptionAttribute), false))[0].Description == c_name);
                    if (info != null)
                    {
                        try
                        {
                            if (!Convert.IsDBNull(value))
                            {
                                object v = null;
                                if (info.PropertyType.ToString().Contains("System.Nullable"))
                                {
                                    v = Convert.ChangeType(value, Nullable.GetUnderlyingType(info.PropertyType));
                                }
                                else
                                {
                                    v = Convert.ChangeType(value, info.PropertyType);
                                }
                                info.SetValue(obj, v, null);
                            }
                        }
                        catch (Exception ex)
                        {
                            CommonLogger.Error("nlog", new Exception("字段[" + info.Name + "]转换出错," + ex.Message));
                        }
                    }
                }
                list.Add(obj);
            }

            return list;
        }

        #region 检测模板 读取数据
        /// <summary>
        /// 检测模板 读取数据
        /// </summary>
        /// <param name="ms"></param>
        /// <param name="fileName"></param>
        /// <returns></returns>
        public static ResponseContext<List<T>> CheckImportExcelData(Stream ms, string fileName)
        {
            ResponseContext<List<T>> response = new ResponseContext<List<T>>();

            List<T> list = new List<T>();
            IWorkbook workbook = null;
            if (fileName.Contains(".xlsx"))
                workbook = new XSSFWorkbook(ms);
            else
                workbook = new HSSFWorkbook(ms);

            ISheet sheet = workbook.GetSheetAt(0);
            IRow firstRowTemp = sheet.GetRow(0);

            if (firstRowTemp == null)
            {
                response.Code = CommonConstants.ErrorCode;
                response.Msg = "模板不正确,请上传正确模板!";
                return response;
            }

            var propertys = new List<PropertyInfo>(typeof(T).GetProperties());

            if (propertys.Count != firstRowTemp.LastCellNum)
            {
                response.Code = CommonConstants.ErrorCode;
                response.Msg = "模板不正确,请上传正确模板!";
                return response;
            }

            DataTable data = new DataTable();
            int startRow = 0;
            IRow firstRow = sheet.GetRow(0);
            int cellCount = firstRow.LastCellNum; //一行最后一个cell的编号 即总的列数 +1是因为加上序号

            for (int j = 0; j < firstRowTemp.LastCellNum; j++)
            {
                string columnName = firstRow.GetCell(j).ToString();
                PropertyInfo info = propertys.Find(p => ((DescriptionAttribute[])p.GetCustomAttributes(typeof(DescriptionAttribute), false))[0].Description == columnName);

                if (info == null)
                {
                    response.Code = CommonConstants.ErrorCode;
                    response.Msg = string.Format("不存在{0}列,请上传正确模板!", columnName);
                    return response;
                }
            }

            data.Columns.Add("Excel序号");
            for (int i = firstRow.FirstCellNum; i < cellCount; ++i)
            {
                ICell cell = firstRow.GetCell(i);
                if (cell != null && !string.IsNullOrEmpty(cell.StringCellValue))
                {
                    string cellValue = cell.StringCellValue.Trim();
                    if (!string.IsNullOrEmpty(cellValue))
                    {
                        DataColumn column = new DataColumn(cellValue);
                        data.Columns.Add(column);
                    }
                }
            }

            startRow = sheet.FirstRowNum + 1;

            //每行数据的个数就是列数 这个列数为实际上有值的列数
            cellCount = data.Columns.Count;

            //最后一列的标号
            int rowMaxIndex = sheet.LastRowNum;
            for (int i = startRow; i <= rowMaxIndex; i++)
            {
                IRow row = sheet.GetRow(i);
                if (row == null || row.Cells.Count == 0
                    || row.Cells.ToList().All(q => GetCellValue(q) == null || string.IsNullOrEmpty(GetCellValue(q).ToString()))) continue; //没有数据的行默认是null　

                if (row.FirstCellNum < 0) continue;
                DataRow dataRow = data.NewRow();
                dataRow[0] = i + 1;
                for (int j = row.FirstCellNum; j < cellCount - 1; j++)
                {
                    if (row.GetCell(j) != null) //同理，没有数据的单元格都默认是null
                    {
                        var value = GetCellValue(row.GetCell(j));
                        dataRow[j + 1] = value;
                    }
                }
                data.Rows.Add(dataRow);
            }

            for (int i = 0; i < data.Rows.Count; i++)
            {
                DataRow dataRow = data.Rows[i];

                T obj = Activator.CreateInstance<T>();

                for (int j = 0; j < propertys.Count; j++)
                {
                    string checkValue = null;

                    PropertyInfo info = propertys[j];

                    if (!GetDataRowData(dataRow, ((DescriptionAttribute)propertys[j].GetCustomAttributes(typeof(DescriptionAttribute), true)[0]).Description, out checkValue))
                        continue;
                    else
                    {
                        object v = null;
                        if (info.PropertyType.ToString().Contains("System.Nullable"))
                        {
                            v = Convert.ChangeType(checkValue, Nullable.GetUnderlyingType(info.PropertyType));
                        }
                        else
                        {
                            v = Convert.ChangeType(checkValue, info.PropertyType);
                        }
                        info.SetValue(obj, v, null);
                    }
                }

                list.Add(obj);
            }

            response.Data = list;

            return response;
        }
        #endregion

        #region 将每个数据都按照类型转化从而获取对应数据类型的数据
        /// <summary>
        /// 将每个数据都按照类型转化从而获取对应数据类型的数据
        /// </summary>
        /// <param name="cell"></param>
        /// <returns></returns>
        private static object GetCellValue(ICell cell)
        {
            object value = null;
            try
            {
                if (cell.CellType != CellType.Blank)
                {
                    switch (cell.CellType)
                    {
                        //Date Type的数据CellType是Numeric
                        case CellType.Numeric:
                            if (DateUtil.IsCellDateFormatted(cell))
                            {
                                //两种时间格式  不带时间光日期的为14
                                if (cell.CellStyle.DataFormat == 14)
                                {
                                    value = cell.DateCellValue.ToShortDateString().Trim();
                                    break;
                                }

                                //带时间的为22
                                if (cell.CellStyle.DataFormat == 22)
                                {
                                    value = cell.DateCellValue;
                                    break;
                                }
                            }
                            else
                            {
                                // Numeric type
                                value = cell.NumericCellValue;
                            }
                            break;
                        case CellType.Boolean:
                            // Boolean type
                            value = cell.BooleanCellValue;
                            break;
                        case CellType.Formula:
                            //对于公式，要判断是否有值在做处理
                            try
                            {
                                HSSFFormulaEvaluator e = new HSSFFormulaEvaluator(cell.Sheet.Workbook);
                                e.EvaluateInCell(cell);
                                value = cell.ToString().Replace("\n", "").Replace("\t", "").Replace("\r", "").Trim();
                            }
                            catch
                            {
                                value = GetFormulaCellValue(cell);
                            }
                            break;
                        case CellType.Error:
                            value = "";
                            break;
                        default:
                            // String type
                            value = cell.StringCellValue.Replace("\n", "").Replace("\t", "").Replace("\r", "").Trim();
                            break;
                    }
                }
            }
            catch (Exception)
            {
                value = "";
            }
            return value ?? "".ToString().Trim();
        }
        #endregion

        #region 获取公式表格中的数据
        /// <summary>
        /// 获取公式表格中的数据
        /// </summary>
        /// <param name="cell"></param>
        /// <returns></returns>
        private static object GetFormulaCellValue(ICell cell)
        {
            object value = null;

            //首先判断是否有值 ErrorCellValue有值说明无值
            try
            {
                value = cell.ErrorCellValue;
                value = "";
                return value;
            }
            catch
            {
                value = "";
            }

            try
            {
                if (DateUtil.IsCellDateFormatted(cell))//日期
                {
                    //两种时间格式  不带时间光日期的为14
                    if (cell.CellStyle.DataFormat == 14)
                    {
                        value = cell.DateCellValue.ToShortDateString();
                        return value;
                    }

                    //带时间的为22
                    if (cell.CellStyle.DataFormat == 22)
                    {
                        value = cell.DateCellValue;
                        return value;
                    }
                }
                else
                {
                    try
                    {
                        value = cell.NumericCellValue;
                        return value;
                    }
                    catch
                    {
                        value = "";
                    }
                }
            }
            catch
            {
                value = "";
            }

            try
            {
                value = cell.StringCellValue.Replace("\n", "").Replace("\t", "").Replace("\r", "");
                return value;
            }
            catch
            {
                value = "";
            }

            try
            {
                value = cell.RichStringCellValue;
                return value;
            }
            catch
            {
                value = "";
            }

            return value;
        }
        #endregion

        #region DataRow或者Object中属性的值 返回值表示不存在对应属性
        /// <summary>
        /// DataRow或者Object中属性的值 返回值表示不存在对应属性
        /// </summary>
        /// <param name="row"></param>
        /// <param name="ColumnName"></param>
        /// <param name="returnStr"></param>
        /// <returns></returns>
        public static bool GetDataRowData(DataRow row, string ColumnName, out string returnStr)
        {
            returnStr = null;
            if (!row.Table.Columns.Contains(ColumnName))
                return false;
            returnStr = row[ColumnName] == null ? "" : row[ColumnName].ToString();
            return true;
        }
        #endregion

    }
}
