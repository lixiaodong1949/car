﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;
using System;
using System.Collections.Generic;
using System.Text;

namespace Car.Framework.Web.Filter
{
    /// <summary>
    /// Action模型验证过滤器
    /// </summary>
    public class ActionModelStateFilter : IActionFilter
    {
        public void OnActionExecuting(ActionExecutingContext context)
        {
            if (!context.ModelState.IsValid)
            {
                ResponseContext<object> result = new ResponseContext<object>();
                foreach (var item in context.ModelState.Values)
                {
                    foreach (var error in item.Errors)
                    {
                        result.Msg += error.ErrorMessage + "|";
                    }
                }
                result.Msg = result.Msg.TrimEnd('|');
                result.Code = CommonConstants.ErrorCode;
                context.Result = new JsonResult(result);
            }
        }

        public void OnActionExecuted(ActionExecutedContext context)
        {

        }
    }
}
