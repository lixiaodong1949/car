﻿using Car.Framework;
using Car.Framework.Auth;
using Car.Framework.Authorizations;
using Car.Framework.Dal;
using Car.Framework.Utils;
using Car.Rcb.Dto;
using Car.Rcb.IServices;
using Car.Rcb.Pocos;
using System;
using System.Collections.Generic;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using Tas.Common.IdGenerate;

namespace Car.Rcb.ServiceImpl
{
    /// <summary>
    /// 用户相关服务实现
    /// </summary>
    public class UserService : IUserService
    {
        #region 用户注册
        /// <summary>
        /// 用户注册
        /// </summary>
        /// <param name="model"></param>
        /// <param name="loginIp"></param>
        /// <param name="_requirement"></param>
        /// <returns></returns>
        public async Task<ResponseContext<UserTicket>> UseRegister(UserRegisterRequestModel model, string loginIp, PermissionRequirement _requirement)
        {
            if (model == null || string.IsNullOrEmpty(model.MobilePhone) || string.IsNullOrEmpty(model.Password))
                return new ResponseContext<UserTicket> { Code = CommonConstants.ErrorCode, Msg = "参数不能为空!" };

            var dbUser = DbContext.FreeSql.Select<CarUser>().Where(u => u.MobilePhone == model.MobilePhone).First();

            if (dbUser != null)
                return new ResponseContext<UserTicket> { Code = CommonConstants.ErrorCode, Msg = "该手机号已经注册,请直接登录!" };

            //验证手机号合理性
            if (!Regex.IsMatch(model.MobilePhone, @"^1[3456789]\d{9}$"))
                return new ResponseContext<UserTicket> { Code = CommonConstants.ErrorCode, Msg = "手机号码无效!" };

            CarUser user = new CarUser
            {
                Id = IdWorker.NextId(),
                MobilePhone = model.MobilePhone,
                Password = model.Password.Md5(),
                NickName = model.MobilePhone.Substring(0, 3) + "****" + model.MobilePhone.Substring(7, 4),
                UpdateTime = DateTime.Now,
                CreateTime = DateTime.Now,
            };
            user.CreateBy = user.Id;
            user.UpdateBy = user.Id;


            var ticket = user == null ? null : new UserTicket()
            {
                Id = user.Id,
                NickName = user.NickName,
                RealName = user.RealName,
                PictureLink = user.PictureLink,
                MobilePhone = user.MobilePhone,
            };

            ticket = JwtTokenHelper.BuildJwtToken(ticket, _requirement);

            var log = new CarUserLoginLog
            {
                AccessToken = ticket.AccessToken,
                Id = IdWorker.NextId(),
                LoginIp = loginIp,
                LoginTime = DateTime.Now,
                UserId = ticket.Id
            };
             
            using (var uow = DbContext.FreeSql.CreateUnitOfWork())
            {
                try
                {
                    //用户表
                    var userBack = await uow.GetRepository<CarUser>().InsertAsync(user);
                    //登录信息表
                    var logBack = await uow.GetRepository<CarUserLoginLog>().InsertAsync(log);

                    if (userBack != null)
                        uow.Commit();
                    else
                        uow.Rollback();
                }
                catch (Exception)
                {
                    uow.Rollback();
                }
            }

            return new ResponseContext<UserTicket> { Code = CommonConstants.SuccessCode, Data = ticket };
        }
        #endregion

        #region 用户手机号、密码登录
        /// <summary>
        /// 用户手机号、密码登录
        /// </summary>
        /// <param name="model"></param>
        /// <param name="loginIp"></param>
        /// <param name="_requirement"></param>
        /// <returns></returns>
        public async Task<ResponseContext<UserTicket>> UserPasswordLogin(UserLoginRequestModel model, string loginIp, PermissionRequirement _requirement)
        {
            return await Task.Run(() =>
            {
                var md5 = model.Password.Md5();
                var user = DbContext.FreeSql.Select<CarUser>().Where(u => u.MobilePhone == model.MobilePhone).First();

                if (user == null)
                    return new ResponseContext<UserTicket> { Code = CommonConstants.ErrorCode, Msg = "该手机号没有注册!" };
                if (user.UserStatus == UserStatus.Disabled)
                    return new ResponseContext<UserTicket> { Code = CommonConstants.ErrorCode, Msg = "用户已被禁用,请联系管理员!" };
                if (user.IsDelete == CommonConstants.IsDelete)
                    return new ResponseContext<UserTicket> { Code = CommonConstants.ErrorCode, Msg = "用户已被删除,请联系管理员!" };
                if (user.Password != md5)
                    return new ResponseContext<UserTicket> { Code = CommonConstants.ErrorCode, Msg = "密码不正确!" };

                var ticket = user == null ? null : new UserTicket()
                {
                    Id = user.Id,
                    NickName = user.NickName,
                    RealName = user.RealName,
                    PictureLink = user.PictureLink,
                    MobilePhone = user.MobilePhone,
                };

                ticket = JwtTokenHelper.BuildJwtToken(ticket, _requirement);

                var log = new CarUserLoginLog
                {
                    AccessToken = ticket.AccessToken,
                    Id = IdWorker.NextId(),
                    LoginIp = loginIp,
                    LoginTime = DateTime.Now,
                    UserId = ticket.Id
                };
                DbContext.FreeSql.Insert<CarUserLoginLog>(log).ExecuteAffrows();

                return new ResponseContext<UserTicket> { Code = CommonConstants.SuccessCode, Data = ticket };
            });
        }
        #endregion

        #region 获取用户信息
        /// <summary>
        /// 获取用户信息
        /// </summary>
        /// <param name="user"></param>
        /// <returns></returns>
        public async Task<ResponseContext<UserInfoViewModel>> GetUserInfo(UserTicket user)
        {
            ResponseContext<UserInfoViewModel> response = new ResponseContext<UserInfoViewModel>();

            CarUser entity = await DbContext.FreeSql.GetRepository<CarUser>().Where(x => x.Id == user.Id).FirstAsync();

            if (entity == null)
                return new ResponseContext<UserInfoViewModel> { Code = CommonConstants.ErrorCode, Msg = "用户不存在!" };

            UserInfoViewModel data = new UserInfoViewModel
            {
                Id = entity.Id,
                MobilePhone = entity.MobilePhone,
                Sex = entity.Sex,
                NickName = entity.NickName,
                RealName = entity.RealName,
                PictureLink = entity.PictureLink,
                Email = entity.Email,
                IdNumber = entity.IdNumber,
            };

            response.Data = data;

            return response;
        }
        #endregion

        #region 更新用户信息
        /// <summary>
        /// 更新用户信息
        /// </summary>
        /// <param name="model"></param>
        /// <param name="user"></param>
        /// <returns></returns>
        public async Task<ResponseContext<bool>> UpdateUserInfo(UserUpdateRequestModel model, UserTicket user)
        {
            CarUser entity = await DbContext.FreeSql.GetRepository<CarUser>().Where(x => x.Id == user.Id).FirstAsync();

            if (entity == null)
                return new ResponseContext<bool> { Code = CommonConstants.ErrorCode, Data = false, Msg = "用户不存在!" };

            entity.Sex = model.Sex;
            entity.NickName = model.NickName;
            entity.RealName = model.RealName;
            entity.PictureLink = model.PictureLink;
            entity.Email = model.Email;
            entity.IdNumber = model.IdNumber;
            entity.UpdateBy = user.Id;
            entity.UpdateTime = DateTime.Now;

            bool state = DbContext.FreeSql.GetRepository<CarUser>().UpdateDiy.SetSource(entity)
                .UpdateColumns(x => new
                {
                    x.Sex,
                    x.NickName,
                    x.RealName,
                    x.PictureLink,
                    x.Email,
                    x.IdNumber,
                    x.UpdateBy,
                    x.UpdateTime,
                }).ExecuteAffrows() > 0;

            return new ResponseContext<bool> { Code = state ? CommonConstants.SuccessCode : CommonConstants.ErrorCode, Data = state, Msg = state ? "成功!" : "操作失败!" };
        }
        #endregion

    }
}
