﻿using Car.Framework;
using Car.Framework.Auth;
using Car.Framework.Authorizations;
using Car.Framework.Dal;
using Car.Framework.Entity;
using Car.Framework.Utils;
using Car.Rcb.Dto;
using Car.Rcb.IServices;
using Car.Rcb.Pocos;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Tas.Common.IdGenerate;

namespace Car.Rcb.ServiceImpl
{
    /// <summary>
    /// 文章后台相关服务实现
    /// </summary>
    public class ArticleBackService : IArticleBackService
    {
        #region 获取技术文章后台列表
        /// <summary>
        /// 获取技术文章后台列表
        /// </summary>
        /// <param name="query"></param>
        /// <returns></returns>
        public async Task<PageResponse<TechnicalArticleForBackViewModel>> GetTechnicalArticleListForBack(TechnicalArticleForBackQuery query)
        {
            var result = new PageResponse<TechnicalArticleForBackViewModel>();
            if (query == null)
                return result;

            var select = DbContext.FreeSql.Select<CarTechnicalArticleDetail>().From<CarTechnologyTag, CarTechnicalArticleLivenessDetail, CarUser>(
                (a, b, c, d) => a.LeftJoin(x => x.TechnologyTagId == b.Id)
                .LeftJoin(x => x.Id == c.ArticleId)
                .LeftJoin(x => x.CreateBy == d.Id))
                .Where((a, b, c, d) => a.IsDelete == CommonConstants.IsNotDelete);

            if (query.TagId.HasValue)
                select.Where((a, b, c, d) => a.TechnologyTagId == query.TagId.Value);
            if (!string.IsNullOrEmpty(query.TitleName))
                select.Where((a, b, c, d) => a.TitleName.Contains(query.TitleName));
            if (query.AuditStatus.HasValue)
                select.Where((a, b, c, d) => a.AuditStatus == query.AuditStatus);
            if (query.HitShelfStatus.HasValue)
                select.Where((a, b, c, d) => a.HitShelfStatus == query.HitShelfStatus);

            var totalCount = select.ToList().Count();

            var items = select.OrderByDescending((a, b, c, d) => a.UpdateTime).Page(query.PageIndex, query.PageSize)
                .ToList((a, b, c, d) => new TechnicalArticleForBackViewModel
                {
                    Id = a.Id,
                    TitleName = a.TitleName,
                    TagId = a.TechnologyTagId,
                    TagName = b.TagName,
                    IsOriginal = a.IsOriginal,
                    AuditStatus = a.AuditStatus,
                    HitShelfStatus = a.HitShelfStatus,
                    PreviewNumber = c.PreviewNumber,
                    CollectNumber = c.CollectNumber,
                    LikeNumber = c.LikeNumber,
                    CreateTime = a.CreateTime,
                    CurrentTime = DateTime.Now,
                    CreateById = a.CreateBy,
                    CreateByName = d.NickName
                });

            //文章主键Ids
            List<long> articleIds = items.Select(x => x.Id).ToList();
            //查询文件段落列表
            List<CarTechnicalArticleParagraphDetail> parDataList = await DbContext.FreeSql.GetRepository<CarTechnicalArticleParagraphDetail>()
                .Where(x => articleIds.Contains(x.ArticleId) && x.IsDelete == CommonConstants.IsNotDelete).ToListAsync();
            //查询文章段落图片列表
            List<CarTechnicalArticlePicture> picDataList = await DbContext.FreeSql.GetRepository<CarTechnicalArticlePicture>()
                .Where(x => articleIds.Contains(x.ArticleId) && x.IsDelete == CommonConstants.IsNotDelete).ToListAsync();

            //获取封面图
            items.ForEach(x =>
            {
                x.CoverPictureUrl = picDataList.Where(a => a.ArticleId == x.Id && a.IsCoverPicture == true).FirstOrDefault()?.PictureUrl;
            });

            items.ForEach(x =>
            {
                //文章段落
                List<TechnicalArticleParagraphViewModel> parList = parDataList.Where(a => a.ArticleId == x.Id).Select(a => new TechnicalArticleParagraphViewModel
                {
                    Id = a.Id,
                    Content = a.Content
                }).ToList();

                //段落图片
                parList.ForEach(a =>
                {
                    List<TechnicalArticlePictureViewModel> picList = picDataList.Where(q => q.ParagraphId == a.Id).Select(q => new TechnicalArticlePictureViewModel
                    {
                        PictureUrl = q.PictureUrl,
                    }).ToList();

                    a.PictureList.AddRange(picList);
                });

                x.ParagraphList.AddRange(parList);
            });

            result.Data = items;
            result.Code = CommonConstants.SuccessCode;
            result.PageSize = query.PageSize;
            result.PageIndex = query.PageIndex;
            result.RecordCount = totalCount;
            return result;
        }
        #endregion

        #region 审核技术文章
        /// <summary>
        /// 审核技术文章
        /// </summary>
        /// <param name="model"></param>
        /// <param name="user"></param>
        /// <returns></returns>
        public async Task<ResponseContext<bool>> AuditTechnicalArticle(TechnicalArticleAuditRequestModel model, UserTicket user)
        {
            if (model == null)
                return new ResponseContext<bool> { Code = CommonConstants.ErrorCode, Data = false, Msg = "参数不允许为空!" };

            bool state = false;

            CarTechnicalArticleDetail articleData = await DbContext.FreeSql.GetRepository<CarTechnicalArticleDetail>().Where(x => x.Id == model.ArticleId).FirstAsync();
            if (articleData == null)
                return new ResponseContext<bool> { Code = CommonConstants.ErrorCode, Data = false, Msg = "文章不存在!" };
            if (articleData.AuditStatus != AuditStatus.Pending)
                return new ResponseContext<bool> { Code = CommonConstants.ErrorCode, Data = false, Msg = "文章已被审核!" };

            //文章审核详情
            CarTechnicalArticleAuditDetail data = new CarTechnicalArticleAuditDetail
            {
                Id = IdWorker.NextId(),
                ArticleId = model.ArticleId,
                AuditStatus = model.AuditStatus,
                OperateReason = model.OperateReason,
                CreateBy = user.Id,
                CreateTime = DateTime.Now,
                UpdateBy = user.Id,
                UpdateTime = DateTime.Now,
            };

            if (model.AuditStatus == AuditStatus.AuditSuccess)
            {
                articleData.AuditStatus = AuditStatus.AuditSuccess;
                articleData.HitShelfStatus = HitShelfStatus.HitShelf;
            }
            if (model.AuditStatus == AuditStatus.AuditRefuse)
                articleData.AuditStatus = AuditStatus.AuditRefuse;

            articleData.UpdateTime = DateTime.Now;

            #region 新增数据
            using (var uow = DbContext.FreeSql.CreateUnitOfWork())
            {
                try
                {
                    //文章审核详情
                    var auditData = await uow.GetRepository<CarTechnicalArticleAuditDetail>().InsertAsync(data);

                    //更新文章详情
                    await uow.GetRepository<CarTechnicalArticleDetail>().UpdateDiy.SetSource(articleData).UpdateColumns(x => new
                    {
                        x.AuditStatus,
                        x.HitShelfStatus,
                        x.UpdateTime
                    }).ExecuteAffrowsAsync();

                    state = auditData != null;

                    if (state)
                        uow.Commit();
                    else
                        uow.Rollback();
                }
                catch (Exception ex)
                {
                    uow.Rollback();
                }
            }
            #endregion

            return new ResponseContext<bool>
            {
                Code = state ? CommonConstants.SuccessCode : CommonConstants.ErrorCode,
                Data = state,
                Msg = state ? "成功!" : "操作失败!"
            };
        }
        #endregion

        #region 获取二手或求购文章后台列表
        /// <summary>
        /// 获取二手或求购文章后台列表
        /// </summary>
        /// <param name="query"></param>
        /// <returns></returns>
        public async Task<PageResponse<UsedOrAskBuyArticleForBackViewModel>> GetUsedOrAskBuyArticleListForBack(UsedOrAskBuyArticleForBackQuery query)
        {
            var result = new PageResponse<UsedOrAskBuyArticleForBackViewModel>();
            if (query == null)
                return result;

            var select = DbContext.FreeSql.Select<CarUsedOrAskBuyArticleDetail>().From<CarBrandType, CarAccessoryType, CarProductType, CarNewOldDegree,
                CarAbrasionDegree, CarUsedOrAskBuyArticleLivenessDetail, CarUser>(
                (a, b, c, d, e, f, g, h) => a.LeftJoin(x => x.BrandId == b.Id)
                .LeftJoin(x => x.AccessoryTypeId == c.Id)
                .LeftJoin(x => x.ProductTypeId == d.Id)
                .LeftJoin(x => x.NewOldDegreeId == e.Id)
                .LeftJoin(x => x.AbrasionDegreeId == f.Id)
                .LeftJoin(x => x.Id == g.UsedOrAskBuyId)
                .LeftJoin(x => x.CreateBy == h.Id))
                .Where((a, b, c, d, e, f, g, h) => a.UsedOrAskBuyType == query.UsedOrAskBuyType && a.IsDelete == CommonConstants.IsNotDelete);

            #region 条件查询
            if (!string.IsNullOrEmpty(query.TitleName))
                select.Where((a, b, c, d, e, f, g, h) => a.TitleName.Contains(query.TitleName));
            if (query.BrandId.HasValue)
                select.Where((a, b, c, d, e, f, g, h) => a.BrandId == query.BrandId);
            if (query.AccessoryTypeId.HasValue)
                select.Where((a, b, c, d, e, f, g, h) => a.AccessoryTypeId == query.AccessoryTypeId);
            if (query.ProductTypeId.HasValue)
                select.Where((a, b, c, d, e, f, g, h) => a.ProductTypeId == query.ProductTypeId);
            if (query.NewOldDegreeId.HasValue)
                select.Where((a, b, c, d, e, f, g, h) => a.NewOldDegreeId == query.NewOldDegreeId);
            if (query.AbrasionDegreeId.HasValue)
                select.Where((a, b, c, d, e, f, g, h) => a.AbrasionDegreeId == query.AbrasionDegreeId);
            if (!string.IsNullOrEmpty(query.ProvinceCode))
                select.Where((a, b, c, d, e, f, g, h) => a.ProvinceCode.Contains(query.ProvinceCode));
            if (!string.IsNullOrEmpty(query.CityCode))
                select.Where((a, b, c, d, e, f, g, h) => a.CityCode.Contains(query.CityCode));
            if (!string.IsNullOrEmpty(query.DistrictCode))
                select.Where((a, b, c, d, e, f, g, h) => a.DistrictCode.Contains(query.DistrictCode));
            if (query.BargainType.HasValue)
                select.Where((a, b, c, d, e, f, g, h) => a.BargainType == query.BargainType);
            if (query.AuditStatus.HasValue)
                select.Where((a, b, c, d, e, f, g, h) => a.AuditStatus == query.AuditStatus);
            if (query.HitShelfStatus.HasValue)
                select.Where((a, b, c, d, e, f, g, h) => a.HitShelfStatus == query.HitShelfStatus);
            #endregion

            var totalCount = select.ToList().Count();

            var items = select.OrderByDescending((a, b, c, d, e, f, g, h) => a.UpdateTime).Page(query.PageIndex, query.PageSize)
                .ToList((a, b, c, d, e, f, g, h) => new UsedOrAskBuyArticleForBackViewModel
                {
                    Id = a.Id,
                    TitleName = a.TitleName,
                    BrandId = a.BrandId,
                    BrandName = b.BrandName,
                    AccessoryTypeId = a.AccessoryTypeId,
                    AccessoryName = c.AccessoryName,
                    ProductTypeId = a.ProductTypeId,
                    ProductName = d.ProductName,
                    NewOldDegreeId = a.NewOldDegreeId,
                    NewOldName = e.NewOldName,
                    AbrasionDegreeId = a.AbrasionDegreeId,
                    AbrasionName = f.AbrasionName,
                    SaleReason = a.SaleReason,
                    ProvinceCode = a.ProvinceCode,
                    ProvinceName = a.ProvinceName,
                    CityCode = a.CityCode,
                    CityName = a.CityName,
                    DistrictCode = a.DistrictCode,
                    DistrictName = a.DistrictName,
                    DetailedAddress = a.DetailedAddress,
                    MobilePhone = a.MobilePhone,
                    BargainType = a.BargainType,
                    SerialNumber = a.SerialNumber,
                    BrandAdditional = a.BrandAdditional,
                    AuditStatus = a.AuditStatus,
                    HitShelfStatus = a.HitShelfStatus,
                    PreviewNumber = g.PreviewNumber,
                    CollectNumber = g.CollectNumber,
                    CreateTime = a.CreateTime,
                    CurrentTime = DateTime.Now,
                    CreateById = a.CreateBy,
                    CreateByName = h.NickName
                });

            //文章主键Ids
            List<long> articleIds = items.Select(x => x.Id).ToList();

            //查询文章图片列表
            List<CarUsedOrAskBuyArticlePicture> picDataList = await DbContext.FreeSql.GetRepository<CarUsedOrAskBuyArticlePicture>()
                .Where(x => articleIds.Contains(x.UsedOrAskBuyId) && x.IsDelete == CommonConstants.IsNotDelete).ToListAsync();

            //获取封面图
            items.ForEach(x =>
            {
                x.CoverPictureUrl = picDataList.Where(a => a.UsedOrAskBuyId == x.Id && a.IsCoverPicture == true).FirstOrDefault()?.PictureUrl;
            });

            items.ForEach(x =>
            {

                List<UsedOrAskBuyArticlePictureViewModel> picList = picDataList.Where(a => a.UsedOrAskBuyId == x.Id).Select(a => new UsedOrAskBuyArticlePictureViewModel
                {
                    PictureUrl = a.PictureUrl,
                }).ToList();

                x.PictureList.AddRange(picList);
            });

            result.Data = items;
            result.Code = CommonConstants.SuccessCode;
            result.PageSize = query.PageSize;
            result.PageIndex = query.PageIndex;
            result.RecordCount = totalCount;
            return result;
        }
        #endregion

        #region 审核二手或求购文章
        /// <summary>
        /// 审核二手或求购文章
        /// </summary>
        /// <param name="model"></param>
        /// <param name="user"></param>
        /// <returns></returns>
        public async Task<ResponseContext<bool>> AuditUsedOrAskBuyArticle(UsedOrAskBuyArticleAuditRequestModel model, UserTicket user)
        {
            if (model == null)
                return new ResponseContext<bool> { Code = CommonConstants.ErrorCode, Data = false, Msg = "参数不允许为空!" };

            bool state = false;

            CarUsedOrAskBuyArticleDetail articleData = await DbContext.FreeSql.GetRepository<CarUsedOrAskBuyArticleDetail>().Where(x => x.Id ==
                model.UsedOrAskBuyId).FirstAsync();
            if (articleData == null)
                return new ResponseContext<bool> { Code = CommonConstants.ErrorCode, Data = false, Msg = "文章不存在!" };
            if (articleData.AuditStatus != AuditStatus.Pending)
                return new ResponseContext<bool> { Code = CommonConstants.ErrorCode, Data = false, Msg = "文章已被审核!" };

            //文章审核详情
            CarUsedOrAskBuyArticleAuditDetail data = new CarUsedOrAskBuyArticleAuditDetail
            {
                Id = IdWorker.NextId(),
                UsedOrAskBuyId = model.UsedOrAskBuyId,
                AuditStatus = model.AuditStatus,
                OperateReason = model.OperateReason,
                CreateBy = user.Id,
                CreateTime = DateTime.Now,
                UpdateBy = user.Id,
                UpdateTime = DateTime.Now,
            };

            if (model.AuditStatus == AuditStatus.AuditSuccess)
            {
                articleData.AuditStatus = AuditStatus.AuditSuccess;
                articleData.HitShelfStatus = HitShelfStatus.HitShelf;
            }
            if (model.AuditStatus == AuditStatus.AuditRefuse)
                articleData.AuditStatus = AuditStatus.AuditRefuse;

            articleData.UpdateTime = DateTime.Now;

            #region 新增数据
            using (var uow = DbContext.FreeSql.CreateUnitOfWork())
            {
                try
                {
                    //文章审核详情
                    var auditData = await uow.GetRepository<CarUsedOrAskBuyArticleAuditDetail>().InsertAsync(data);

                    //更新文章详情
                    await uow.GetRepository<CarUsedOrAskBuyArticleDetail>().UpdateDiy.SetSource(articleData).UpdateColumns(x => new
                    {
                        x.AuditStatus,
                        x.HitShelfStatus,
                        x.UpdateTime
                    }).ExecuteAffrowsAsync();

                    state = auditData != null;

                    if (state)
                        uow.Commit();
                    else
                        uow.Rollback();
                }
                catch (Exception ex)
                {
                    uow.Rollback();
                }
            }
            #endregion

            return new ResponseContext<bool>
            {
                Code = state ? CommonConstants.SuccessCode : CommonConstants.ErrorCode,
                Data = state,
                Msg = state ? "成功!" : "操作失败!"
            };
        }
        #endregion


    }
}
