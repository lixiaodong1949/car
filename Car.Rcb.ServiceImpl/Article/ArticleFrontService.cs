﻿using Car.Framework;
using Car.Framework.Auth;
using Car.Framework.Authorizations;
using Car.Framework.Dal;
using Car.Framework.Entity;
using Car.Framework.Utils;
using Car.Rcb.Dto;
using Car.Rcb.IServices;
using Car.Rcb.Pocos;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Tas.Common.IdGenerate;

namespace Car.Rcb.ServiceImpl
{
    /// <summary>
    /// 文章前台相关服务实现
    /// </summary>
    public class ArticleFrontService : IArticleFrontService
    {
        #region 发布技术文章
        /// <summary>
        /// 发布技术文章
        /// </summary>
        /// <param name="model"></param>
        /// <param name="user"></param>
        /// <returns></returns>
        public async Task<ResponseContext<bool>> PublishTechnicalArticle(TechnicalArticleRequestModel model, UserTicket user)
        {
            if (model == null || model.ParagraphList == null || model.ParagraphList.Count == 0)
                return new ResponseContext<bool> { Code = CommonConstants.ErrorCode, Data = false, Msg = "内容不允许为空!" };

            bool state = false;

            #region 解析数据

            //文章详情
            CarTechnicalArticleDetail data = new CarTechnicalArticleDetail
            {
                Id = IdWorker.NextId(),
                TitleName = model.TitleName,
                TechnologyTagId = model.TechnologyTagId,
                IsOriginal = model.IsOriginal,
                AuditStatus = AuditStatus.AuditSuccess,//默认审核成功
                HitShelfStatus = HitShelfStatus.HitShelf,//默认已上架
                CreateBy = user.Id,
                CreateTime = DateTime.Now,
                UpdateBy = user.Id,
                UpdateTime = DateTime.Now,
            };

            //段落内容数据集
            List<CarTechnicalArticleParagraphDetail> parDataList = new List<CarTechnicalArticleParagraphDetail>();
            //段落图片数据集
            List<CarTechnicalArticlePicture> picDataList = new List<CarTechnicalArticlePicture>();

            model.ParagraphList.ForEach(x =>
            {
                CarTechnicalArticleParagraphDetail parModel = new CarTechnicalArticleParagraphDetail
                {
                    Id = IdWorker.NextId(),
                    ArticleId = data.Id,
                    Content = x.Content,
                    CreateBy = user.Id,
                    CreateTime = DateTime.Now,
                    UpdateBy = user.Id,
                    UpdateTime = DateTime.Now,
                };

                parDataList.Add(parModel);

                x.PictureList.ForEach(a =>
                {
                    CarTechnicalArticlePicture picModel = new CarTechnicalArticlePicture
                    {
                        Id = IdWorker.NextId(),
                        ArticleId = data.Id,
                        ParagraphId = parModel.Id,
                        PictureUrl = a.PictureUrl,
                        IsCoverPicture = false,
                        CreateBy = user.Id,
                        CreateTime = DateTime.Now,
                        UpdateBy = user.Id,
                        UpdateTime = DateTime.Now,
                    };

                    picDataList.Add(picModel);
                });
            });

            //默认第一张为封面图
            if (picDataList.Count > 0)
                picDataList[0].IsCoverPicture = true;

            #endregion

            #region 新增数据
            using (var uow = DbContext.FreeSql.CreateUnitOfWork())
            {
                try
                {
                    //文章详情
                    var artData = await uow.GetRepository<CarTechnicalArticleDetail>().InsertAsync(data);
                    //文章段落详情
                    if (parDataList.Count > 0)
                        await uow.GetRepository<CarTechnicalArticleParagraphDetail>().InsertAsync(parDataList);
                    //文章段落图片详情
                    if (picDataList.Count > 0)
                        await uow.GetRepository<CarTechnicalArticlePicture>().InsertAsync(picDataList);

                    state = artData != null;

                    if (state)
                        uow.Commit();
                    else
                        uow.Rollback();
                }
                catch (Exception ex)
                {
                    uow.Rollback();
                }
            }
            #endregion

            return new ResponseContext<bool>
            {
                Code = state ? CommonConstants.SuccessCode : CommonConstants.ErrorCode,
                Data = state,
                Msg = state ? "成功!" : "发布失败!"
            };
        }
        #endregion

        #region 获取平台技术文章列表
        /// <summary>
        /// 获取平台技术文章列表
        /// </summary>
        /// <param name="query"></param>
        /// <param name="user"></param>
        /// <returns></returns>
        public async Task<PageResponse<TechnicalArticleViewModel>> GetPlatformTechnicalArticleList(TechnicalArticleQuery query, UserTicket user)
        {
            var result = new PageResponse<TechnicalArticleViewModel>();
            if (query == null)
                return result;

            var select = DbContext.FreeSql.Select<CarTechnicalArticleDetail>().From<CarTechnologyTag, CarTechnicalArticleLivenessDetail, CarUser>(
                (a, b, c, d) => a.LeftJoin(x => x.TechnologyTagId == b.Id)
                .LeftJoin(x => x.Id == c.ArticleId)
                .LeftJoin(x => x.CreateBy == d.Id))
                .Where((a, b, c, d) => a.AuditStatus == AuditStatus.AuditSuccess
                    && a.HitShelfStatus == HitShelfStatus.HitShelf && a.IsDelete == CommonConstants.IsNotDelete);

            if (query.TagId.HasValue)
                select.Where((a, b, c, d) => a.TechnologyTagId == query.TagId.Value);
            if (!string.IsNullOrEmpty(query.TitleName))
                select.Where((a, b, c, d) => a.TitleName.Contains(query.TitleName));
            //if (query.IsOneself)
            //    select.Where((a, b, c, d) => a.CreateBy == user.Id);

            var totalCount = select.ToList().Count();

            var items = select.OrderByDescending((a, b, c, d) => a.UpdateTime).Page(query.PageIndex, query.PageSize)
                .ToList((a, b, c, d) => new TechnicalArticleViewModel
                {
                    Id = a.Id,
                    TitleName = a.TitleName,
                    TagId = a.TechnologyTagId,
                    TagName = b.TagName,
                    IsOriginal = a.IsOriginal,
                    PreviewNumber = c.PreviewNumber,
                    CollectNumber = c.CollectNumber,
                    LikeNumber = c.LikeNumber,
                    CreateTime = a.CreateTime,
                    CurrentTime = DateTime.Now,
                    CreateById = a.CreateBy,
                    CreateByName = d.NickName,
                    PictureLink = d.PictureLink,
                });

            //文章主键Ids
            List<long> articleIds = items.Select(x => x.Id).ToList();
            //查询文件段落列表
            List<CarTechnicalArticleParagraphDetail> parDataList = await DbContext.FreeSql.GetRepository<CarTechnicalArticleParagraphDetail>()
                .Where(x => articleIds.Contains(x.ArticleId) && x.IsDelete == CommonConstants.IsNotDelete).ToListAsync();
            //查询文章段落图片列表
            List<CarTechnicalArticlePicture> picDataList = await DbContext.FreeSql.GetRepository<CarTechnicalArticlePicture>()
                .Where(x => articleIds.Contains(x.ArticleId) && x.IsDelete == CommonConstants.IsNotDelete).ToListAsync();

            //获取封面图
            items.ForEach(x =>
            {
                if (x.CreateById == user.Id)
                    x.IsOneSelf = true;

                x.CoverPictureUrl = picDataList.Where(a => a.ArticleId == x.Id && a.IsCoverPicture == true).FirstOrDefault()?.PictureUrl;
            });

            items.ForEach(x =>
            {
                //文章段落
                List<TechnicalArticleParagraphViewModel> parList = parDataList.Where(a => a.ArticleId == x.Id).Select(a => new TechnicalArticleParagraphViewModel
                {
                    Id = a.Id,
                    Content = a.Content
                }).ToList();

                //段落图片
                parList.ForEach(a =>
                {
                    List<TechnicalArticlePictureViewModel> picList = picDataList.Where(q => q.ParagraphId == a.Id).Select(q => new TechnicalArticlePictureViewModel
                    {
                        PictureUrl = q.PictureUrl,
                    }).ToList();

                    a.PictureList.AddRange(picList);
                });

                x.ParagraphList.AddRange(parList);
            });

            result.Data = items;
            result.Code = CommonConstants.SuccessCode;
            result.PageSize = query.PageSize;
            result.PageIndex = query.PageIndex;
            result.RecordCount = totalCount;
            return result;
        }
        #endregion

        #region 获取某一技术文章详情
        /// <summary>
        /// 获取某一技术文章详情
        /// </summary>
        /// <param name="articleId"></param>
        /// <param name="user"></param>
        /// <returns></returns>
        public async Task<ResponseContext<TechnicalArticleDetailViewModel>> GetTechnicalArticleDetail(long articleId, UserTicket user)
        {
            ResponseContext<TechnicalArticleDetailViewModel> response = new ResponseContext<TechnicalArticleDetailViewModel>();

            var dataList = await DbContext.FreeSql.Select<CarTechnicalArticleDetail>().From<CarTechnologyTag, CarTechnicalArticleLivenessDetail,
                CarTechnicalArticleUserActiveDetail, CarUser>(
               (a, b, c, d, e) => a.LeftJoin(x => x.TechnologyTagId == b.Id)
               .LeftJoin(x => x.Id == c.ArticleId)
               .LeftJoin(x => x.Id == d.ArticleId && d.UserId == user.Id)
               .LeftJoin(x => x.CreateBy == e.Id))
               .Where((a, b, c, d, e) => a.Id == articleId && a.IsDelete == CommonConstants.IsNotDelete)
               .ToListAsync((a, b, c, d, e) => new TechnicalArticleDetailViewModel
               {
                   Id = a.Id,
                   TitleName = a.TitleName,
                   TagId = a.TechnologyTagId,
                   TagName = b.TagName,
                   CreateTime = a.CreateTime,
                   CreateById = a.CreateBy,
                   CreateByName = e.NickName,
                   PictureLink = e.PictureLink,
                   CurrentTime = DateTime.Now,
                   PreviewNumber = c.PreviewNumber,
                   CollectNumber = c.CollectNumber,
                   LikeNumber = c.LikeNumber,
                   IsPreview = d.IsPreview,
                   IsCollect = d.IsCollect,
                   IsLike = d.IsLike
               });

            if (dataList == null || dataList.Count == 0)
                return response;

            response.Data = dataList.FirstOrDefault();

            //查询文章段落列表
            List<TechnicalArticleParagraphViewModel> parList = await DbContext.FreeSql.GetRepository<CarTechnicalArticleParagraphDetail>()
                .Where(x => x.ArticleId == response.Data.Id && x.IsDelete == CommonConstants.IsNotDelete).ToListAsync(x =>
                 new TechnicalArticleParagraphViewModel
                 {
                     Id = x.Id,
                     Content = x.Content
                 });

            //查询文章段落图片列表
            List<CarTechnicalArticlePicture> picDataList = await DbContext.FreeSql.GetRepository<CarTechnicalArticlePicture>()
                .Where(x => x.ArticleId == response.Data.Id && x.IsDelete == CommonConstants.IsNotDelete).ToListAsync();

            //段落图片
            parList.ForEach(a =>
            {
                List<TechnicalArticlePictureViewModel> picList = picDataList.Where(q => q.ParagraphId == a.Id).Select(q => new TechnicalArticlePictureViewModel
                {
                    PictureUrl = q.PictureUrl,
                }).ToList();

                a.PictureList.AddRange(picList);
            });

            response.Data.ParagraphList.AddRange(parList);

            return response;
        }
        #endregion

        #region 删除自己发布的技术文章
        /// <summary>
        /// 删除自己发布的技术文章
        /// </summary>
        /// <param name="id"></param>
        /// <param name="user"></param>
        /// <returns></returns>
        public async Task<ResponseContext<bool>> DeleteOneselfTechnicalArticle(long id, UserTicket user)
        {
            CarTechnicalArticleDetail entity = await DbContext.FreeSql.GetRepository<CarTechnicalArticleDetail>().Where(x => x.Id == id).FirstAsync();

            if (entity == null)
                return new ResponseContext<bool> { Code = CommonConstants.ErrorCode, Data = false, Msg = "文章不存在!" };
            if (entity.CreateBy != user.Id)
                return new ResponseContext<bool> { Code = CommonConstants.ErrorCode, Data = false, Msg = "不是自己的文章,暂不支持删除!" };

            entity.IsDelete = 1;
            entity.UpdateTime = DateTime.Now;
            entity.UpdateBy = user.Id;

            bool state = DbContext.FreeSql.GetRepository<CarTechnicalArticleDetail>().UpdateDiy.SetSource(entity)
                .UpdateColumns(x => new
                {
                    x.IsDelete,
                    x.UpdateTime,
                    x.UpdateBy
                }).ExecuteAffrows() > 0;

            return new ResponseContext<bool> { Code = state ? CommonConstants.SuccessCode : CommonConstants.ErrorCode, Data = state, Msg = state ? "成功!" : "操作失败!" };
        }
        #endregion

        #region 用户操作技术文章活跃程度(查看、收藏、点赞)
        /// <summary>
        /// 用户操作技术文章活跃程度(查看、收藏、点赞)
        /// </summary>
        /// <param name="model"></param>
        /// <param name="user"></param>
        /// <returns></returns>
        public async Task<ResponseContext<bool>> UserOperationTechnicalArticleForActive(TechnicalArticleUserActiveRequestModel model, UserTicket user)
        {
            if (model == null || model.ArticleId == 0)
                return new ResponseContext<bool> { Code = CommonConstants.ErrorCode, Data = false, Msg = "参数不允许为空!" };

            //技术文章用户活跃详情
            CarTechnicalArticleUserActiveDetail data = await DbContext.FreeSql.GetRepository<CarTechnicalArticleUserActiveDetail>()
                .Where(x => x.ArticleId == model.ArticleId && x.UserId == user.Id && x.IsDelete == CommonConstants.IsNotDelete).FirstAsync();

            if (data == null && model.OperationType != UserActiveOperationType.Preview)
                return new ResponseContext<bool> { Code = CommonConstants.ErrorCode, Data = false, Msg = "操作类型有误!" };

            //技术文章活跃度详情
            CarTechnicalArticleLivenessDetail livenessData = await DbContext.FreeSql.GetRepository<CarTechnicalArticleLivenessDetail>()
                .Where(x => x.ArticleId == model.ArticleId && x.IsDelete == CommonConstants.IsNotDelete).FirstAsync();

            bool state = false;

            if (data == null)
            {
                data = new CarTechnicalArticleUserActiveDetail
                {
                    Id = IdWorker.NextId(),
                    ArticleId = model.ArticleId,
                    UserId = user.Id,
                    IsPreview = true,
                    CreateBy = user.Id,
                    CreateTime = DateTime.Now,
                    UpdateBy = user.Id,
                    UpdateTime = DateTime.Now,
                };

                if (livenessData == null)
                {
                    livenessData = new CarTechnicalArticleLivenessDetail
                    {
                        ArticleId = model.ArticleId,
                        PreviewNumber = 1,
                        CreateBy = user.Id,
                        CreateTime = DateTime.Now,
                        UpdateBy = user.Id,
                        UpdateTime = DateTime.Now,
                    };

                    //添加文章查看数
                    await DbContext.FreeSql.Insert<CarTechnicalArticleLivenessDetail>(livenessData).ExecuteAffrowsAsync();
                }
                else
                {
                    livenessData.PreviewNumber = livenessData.PreviewNumber + 1;

                    //更新文章查看数
                    await DbContext.FreeSql.GetRepository<CarTechnicalArticleLivenessDetail>().UpdateDiy.SetSource(livenessData).UpdateColumns(x => new
                    {
                        x.PreviewNumber,
                        x.UpdateBy,
                        x.UpdateTime
                    }).ExecuteAffrowsAsync();
                }

                state = await DbContext.FreeSql.Insert<CarTechnicalArticleUserActiveDetail>(data).ExecuteAffrowsAsync() > 0;
            }
            else
            {
                if (livenessData == null)
                    return new ResponseContext<bool> { Code = CommonConstants.ErrorCode, Data = false, Msg = "文章活跃度统计有误!" };

                livenessData.UpdateBy = user.Id;
                livenessData.UpdateTime = DateTime.Now;

                data.UpdateBy = user.Id;
                data.UpdateTime = DateTime.Now;

                bool tempBoole;

                //收藏
                if (model.OperationType == UserActiveOperationType.Collect)
                {
                    tempBoole = data.IsCollect;
                    data.IsCollect = tempBoole ? false : true;

                    if (data.IsCollect)
                        livenessData.CollectNumber = livenessData.CollectNumber + 1;
                    else
                        livenessData.CollectNumber = livenessData.CollectNumber - 1;
                }
                //点赞
                if (model.OperationType == UserActiveOperationType.Like)
                {
                    tempBoole = data.IsLike;
                    data.IsLike = tempBoole ? false : true;

                    if (data.IsLike)
                        livenessData.LikeNumber = livenessData.LikeNumber + 1;
                    else
                        livenessData.LikeNumber = livenessData.LikeNumber - 1;
                }

                #region 更新数据
                using (var uow = DbContext.FreeSql.CreateUnitOfWork())
                {
                    try
                    {
                        //更新文章活跃度
                        await uow.GetRepository<CarTechnicalArticleLivenessDetail>().UpdateDiy.SetSource(livenessData).UpdateColumns(x => new
                        {
                            x.CollectNumber,
                            x.LikeNumber,
                            x.UpdateBy,
                            x.UpdateTime
                        }).ExecuteAffrowsAsync();

                        //更新文章用户活跃度
                        state = await uow.GetRepository<CarTechnicalArticleUserActiveDetail>().UpdateDiy.SetSource(data).UpdateColumns(x => new
                        {
                            x.IsCollect,
                            x.IsLike,
                            x.UpdateBy,
                            x.UpdateTime
                        }).ExecuteAffrowsAsync() > 0;

                        if (state)
                            uow.Commit();
                        else
                            uow.Rollback();
                    }
                    catch (Exception ex)
                    {
                        uow.Rollback();
                    }
                }
                #endregion
            }

            return new ResponseContext<bool>
            {
                Code = state ? CommonConstants.SuccessCode : CommonConstants.ErrorCode,
                Data = state,
                Msg = state ? "成功!" : "操作失败!"
            };
        }
        #endregion

        #region 获取个人发布/收藏技术文章列表
        /// <summary>
        /// 获取个人发布/收藏技术文章列表
        /// </summary>
        /// <param name="query"></param>
        /// <param name="user"></param>
        /// <returns></returns>
        public async Task<PageResponse<TechnicalArticleViewModel>> GetOneSelfTechnicalArticleList(TechnicalArticleOneselfQuery query, UserTicket user)
        {
            var result = new PageResponse<TechnicalArticleViewModel>();
            if (query == null)
                return result;

            int totalCount = 0;
            List<TechnicalArticleViewModel> items = new List<TechnicalArticleViewModel>();

            //查询个人发布文章
            if (query.ResourceSource == ResourcePersonSource.DoOneOwn)
            {
                var select = DbContext.FreeSql.Select<CarTechnicalArticleDetail>().From<CarTechnologyTag, CarTechnicalArticleLivenessDetail, CarUser>(
                    (a, b, c, d) => a.LeftJoin(x => x.TechnologyTagId == b.Id)
                    .LeftJoin(x => x.Id == c.ArticleId)
                    .LeftJoin(x => x.CreateBy == d.Id))
                    .Where((a, b, c, d) => a.AuditStatus == AuditStatus.AuditSuccess && a.HitShelfStatus == HitShelfStatus.HitShelf &&
                       a.CreateBy == user.Id && a.IsDelete == CommonConstants.IsNotDelete);

                if (query.TagId.HasValue)
                    select.Where((a, b, c, d) => a.TechnologyTagId == query.TagId.Value);
                if (!string.IsNullOrEmpty(query.TitleName))
                    select.Where((a, b, c, d) => a.TitleName.Contains(query.TitleName));

                totalCount = select.ToList().Count();

                items = select.OrderByDescending((a, b, c, d) => a.UpdateTime).Page(query.PageIndex, query.PageSize)
                    .ToList((a, b, c, d) => new TechnicalArticleViewModel
                    {
                        Id = a.Id,
                        TitleName = a.TitleName,
                        TagId = a.TechnologyTagId,
                        TagName = b.TagName,
                        IsOriginal = a.IsOriginal,
                        PreviewNumber = c.PreviewNumber,
                        CollectNumber = c.CollectNumber,
                        LikeNumber = c.LikeNumber,
                        CreateTime = a.CreateTime,
                        CurrentTime = DateTime.Now,
                        CreateById = a.CreateBy,
                        CreateByName = d.NickName,
                        PictureLink = d.PictureLink,
                    });
            }

            //查询个人收藏文章
            if (query.ResourceSource == ResourcePersonSource.Collect)
            {
                var select = DbContext.FreeSql.Select<CarTechnicalArticleDetail>().From<CarTechnicalArticleUserActiveDetail, CarTechnologyTag,
                        CarTechnicalArticleLivenessDetail, CarUser>(
                        (a, b, c, d, e) => a.InnerJoin(x => x.Id == b.ArticleId)
                        .LeftJoin(x => x.TechnologyTagId == c.Id)
                        .LeftJoin(x => x.Id == d.ArticleId)
                        .LeftJoin(x => x.CreateBy == e.Id))
                        .Where((a, b, c, d, e) => a.AuditStatus == AuditStatus.AuditSuccess && a.HitShelfStatus == HitShelfStatus.HitShelf &&
                            b.UserId == user.Id && b.IsCollect == true && a.IsDelete == CommonConstants.IsNotDelete && b.IsDelete == CommonConstants.IsNotDelete);

                if (query.TagId.HasValue)
                    select.Where((a, b, c, d, e) => a.TechnologyTagId == query.TagId.Value);
                if (!string.IsNullOrEmpty(query.TitleName))
                    select.Where((a, b, c, d, e) => a.TitleName.Contains(query.TitleName));

                totalCount = select.ToList().Count();

                items = select.OrderByDescending((a, b, c, d, e) => a.UpdateTime).Page(query.PageIndex, query.PageSize)
                    .ToList((a, b, c, d, e) => new TechnicalArticleViewModel
                    {
                        Id = a.Id,
                        TitleName = a.TitleName,
                        TagId = a.TechnologyTagId,
                        TagName = c.TagName,
                        IsOriginal = a.IsOriginal,
                        PreviewNumber = d.PreviewNumber,
                        CollectNumber = d.CollectNumber,
                        LikeNumber = d.LikeNumber,
                        CreateTime = a.CreateTime,
                        CurrentTime = DateTime.Now,
                        CreateById = a.CreateBy,
                        CreateByName = e.NickName,
                        PictureLink = e.PictureLink,
                    });
            }

            //文章主键Ids
            List<long> articleIds = items.Select(x => x.Id).ToList();
            //查询文件段落列表
            List<CarTechnicalArticleParagraphDetail> parDataList = await DbContext.FreeSql.GetRepository<CarTechnicalArticleParagraphDetail>()
                .Where(x => articleIds.Contains(x.ArticleId) && x.IsDelete == CommonConstants.IsNotDelete).ToListAsync();
            //查询文章段落图片列表
            List<CarTechnicalArticlePicture> picDataList = await DbContext.FreeSql.GetRepository<CarTechnicalArticlePicture>()
                .Where(x => articleIds.Contains(x.ArticleId) && x.IsDelete == CommonConstants.IsNotDelete).ToListAsync();

            //获取封面图
            items.ForEach(x =>
            {
                if (x.CreateById == user.Id)
                    x.IsOneSelf = true;

                x.CoverPictureUrl = picDataList.Where(a => a.ArticleId == x.Id && a.IsCoverPicture == true).FirstOrDefault()?.PictureUrl;
            });

            items.ForEach(x =>
            {
                //文章段落
                List<TechnicalArticleParagraphViewModel> parList = parDataList.Where(a => a.ArticleId == x.Id).Select(a => new TechnicalArticleParagraphViewModel
                {
                    Id = a.Id,
                    Content = a.Content
                }).ToList();

                //段落图片
                parList.ForEach(a =>
                {
                    List<TechnicalArticlePictureViewModel> picList = picDataList.Where(q => q.ParagraphId == a.Id).Select(q => new TechnicalArticlePictureViewModel
                    {
                        PictureUrl = q.PictureUrl,
                    }).ToList();

                    a.PictureList.AddRange(picList);
                });

                x.ParagraphList.AddRange(parList);
            });

            result.Data = items;
            result.Code = CommonConstants.SuccessCode;
            result.PageSize = query.PageSize;
            result.PageIndex = query.PageIndex;
            result.RecordCount = totalCount;
            return result;
        }
        #endregion

        #region 用户评论技术文章
        /// <summary>
        /// 用户评论技术文章
        /// </summary>
        /// <param name="model"></param>
        /// <param name="user"></param>
        /// <returns></returns>
        public async Task<ResponseContext<bool>> CommentTechnicalArticle(TechnicalArticleCommentRequestModel model, UserTicket user)
        {
            if (model == null || model.Content == null)
                return new ResponseContext<bool> { Code = CommonConstants.ErrorCode, Data = false, Msg = "内容不允许为空!" };

            CarTechnicalArticleDetail entity = await DbContext.FreeSql.GetRepository<CarTechnicalArticleDetail>().Where(x => x.Id == model.ArticleId).FirstAsync();
            if (entity == null)
                return new ResponseContext<bool> { Code = CommonConstants.ErrorCode, Data = false, Msg = "文章不存在!" };

            //文章评论详情
            CarTechnicalArticleCommentDetail data = new CarTechnicalArticleCommentDetail
            {
                Id = IdWorker.NextId(),
                ArticleId = model.ArticleId,
                UserId = user.Id,
                Content = model.Content,
                CreateBy = user.Id,
                CreateTime = DateTime.Now,
                UpdateBy = user.Id,
                UpdateTime = DateTime.Now,
            };

            bool state = await DbContext.FreeSql.Insert<CarTechnicalArticleCommentDetail>(data).ExecuteAffrowsAsync() > 0;

            return new ResponseContext<bool>
            {
                Code = state ? CommonConstants.SuccessCode : CommonConstants.ErrorCode,
                Data = state,
                Msg = state ? "成功!" : "操作失败!"
            };
        }
        #endregion

        #region 获取文章评论列表
        /// <summary>
        /// 获取文章评论列表
        /// </summary>
        /// <param name="query"></param>
        /// <param name="user"></param>
        /// <returns></returns>
        public async Task<PageResponse<TechnicalArticleCommentViewModel>> GetTechnicalArticleCommentList(TechnicalArticleCommentQuery query, UserTicket user)
        {
            var result = new PageResponse<TechnicalArticleCommentViewModel>();
            if (query == null)
                return result;

            var select = DbContext.FreeSql.Select<CarTechnicalArticleCommentDetail>().From<CarUser>(
                (a, b) => a.LeftJoin(x => x.UserId == b.Id))
                .Where((a, b) => a.ArticleId == query.ArticleId && a.IsDelete == CommonConstants.IsNotDelete);

            var totalCount = select.ToList().Count();

            var items = await select.OrderByDescending((a, b) => a.UpdateTime).Page(query.PageIndex, query.PageSize)
                .ToListAsync((a, b) => new TechnicalArticleCommentViewModel
                {
                    Id = a.Id,
                    UserId = a.UserId,
                    Reviewer = b.NickName,
                    PictureLink = b.PictureLink,
                    Content = a.Content,
                    CreateTime = a.CreateTime,
                    CurrentTime = DateTime.Now,
                });

            items.ForEach(x =>
            {
                if (x.UserId == user.Id)
                    x.IsOneSelf = true;
            });

            result.Data = items;
            result.Code = CommonConstants.SuccessCode;
            result.PageSize = query.PageSize;
            result.PageIndex = query.PageIndex;
            result.RecordCount = totalCount;
            return result;
        }
        #endregion

        #region 删除自己发布的评论
        /// <summary>
        /// 删除自己发布的评论
        /// </summary>
        /// <param name="id"></param>
        /// <param name="user"></param>
        /// <returns></returns>
        public async Task<ResponseContext<bool>> DeleteOneselfComment(long id, UserTicket user)
        {
            CarTechnicalArticleCommentDetail entity = await DbContext.FreeSql.GetRepository<CarTechnicalArticleCommentDetail>().Where(x => x.Id == id).FirstAsync();

            if (entity == null)
                return new ResponseContext<bool> { Code = CommonConstants.ErrorCode, Data = false, Msg = "没有该数据源!" };
            if (entity.CreateBy != user.Id)
                return new ResponseContext<bool> { Code = CommonConstants.ErrorCode, Data = false, Msg = "不是自己的评论,暂不支持删除!" };

            entity.IsDelete = 1;
            entity.UpdateTime = DateTime.Now;
            entity.UpdateBy = user.Id;

            bool state = DbContext.FreeSql.GetRepository<CarTechnicalArticleCommentDetail>().UpdateDiy.SetSource(entity)
                .UpdateColumns(x => new
                {
                    x.IsDelete,
                    x.UpdateTime,
                    x.UpdateBy
                }).ExecuteAffrows() > 0;

            return new ResponseContext<bool> { Code = state ? CommonConstants.SuccessCode : CommonConstants.ErrorCode, Data = state, Msg = state ? "成功!" : "操作失败!" };
        }
        #endregion

        #region 发布二手或求购文章
        /// <summary>
        /// 发布二手或求购文章
        /// </summary>
        /// <param name="model"></param>
        /// <param name="user"></param>
        /// <returns></returns>
        public async Task<ResponseContext<bool>> PublishUsedOrAskBuyArticle(UsedOrAskBuyRequestModel model, UserTicket user)
        {
            if (model == null)
                return new ResponseContext<bool> { Code = CommonConstants.ErrorCode, Data = false, Msg = "内容不允许为空!" };

            bool state = false;

            #region 解析数据

            //查询二手、求购数量
            int countNumber = (int)await DbContext.FreeSql.Select<CarUsedOrAskBuyArticleDetail>().CountAsync();

            //二手或求购详情
            CarUsedOrAskBuyArticleDetail data = new CarUsedOrAskBuyArticleDetail
            {
                Id = IdWorker.NextId(),
                UsedOrAskBuyType = model.UsedOrAskBuyType,
                TitleName = model.TitleName,
                BrandId = model.BrandId,
                AccessoryTypeId = model.AccessoryTypeId,
                ProductTypeId = model.ProductTypeId,
                NewOldDegreeId = model.NewOldDegreeId,
                AbrasionDegreeId = model.AbrasionDegreeId,
                SaleReason = model.SaleReason,
                ProvinceCode = model.ProvinceCode,
                ProvinceName = model.ProvinceName,
                CityCode = model.CityCode,
                CityName = model.CityName,
                DistrictCode = model.DistrictCode,
                DistrictName = model.DistrictName,
                DetailedAddress = model.DetailedAddress,
                MobilePhone = model.MobilePhone,
                BargainType = model.BargainType,
                BrandAdditional = model.BrandAdditional,
                SerialNumber = string.Format("{0}{1}", DateTime.Now.ToString("yyyyMMdd"), countNumber + 1),
                AuditStatus = AuditStatus.AuditSuccess,//默认审核成功
                HitShelfStatus = HitShelfStatus.HitShelf,//默认已上架
                IsDealClose = false,
                CreateBy = user.Id,
                CreateTime = DateTime.Now,
                UpdateBy = user.Id,
                UpdateTime = DateTime.Now,
            };

            //图片数据集
            List<CarUsedOrAskBuyArticlePicture> picDataList = new List<CarUsedOrAskBuyArticlePicture>();

            model.PictureList.ForEach(a =>
            {
                CarUsedOrAskBuyArticlePicture picModel = new CarUsedOrAskBuyArticlePicture
                {
                    Id = IdWorker.NextId(),
                    UsedOrAskBuyId = data.Id,
                    PictureUrl = a.PictureUrl,
                    IsCoverPicture = false,
                    CreateBy = user.Id,
                    CreateTime = DateTime.Now,
                    UpdateBy = user.Id,
                    UpdateTime = DateTime.Now,
                };

                picDataList.Add(picModel);
            });

            //默认第一张为封面图
            if (picDataList.Count > 0)
                picDataList[0].IsCoverPicture = true;

            #endregion

            #region 新增数据
            using (var uow = DbContext.FreeSql.CreateUnitOfWork())
            {
                try
                {
                    //二手或求购详情
                    var resultData = await uow.GetRepository<CarUsedOrAskBuyArticleDetail>().InsertAsync(data);
                    //二手或求购图片详情
                    if (picDataList.Count > 0)
                        await uow.GetRepository<CarUsedOrAskBuyArticlePicture>().InsertAsync(picDataList);

                    state = resultData != null;

                    if (state)
                        uow.Commit();
                    else
                        uow.Rollback();
                }
                catch (Exception ex)
                {
                    uow.Rollback();
                }
            }
            #endregion

            return new ResponseContext<bool>
            {
                Code = state ? CommonConstants.SuccessCode : CommonConstants.ErrorCode,
                Data = state,
                Msg = state ? "成功!" : "发布失败!"
            };
        }
        #endregion

        #region 获取平台二手或求购文章列表
        /// <summary>
        /// 获取平台二手或求购文章列表
        /// </summary>
        /// <param name="query"></param>
        /// <param name="user"></param>
        /// <returns></returns>
        public async Task<PageResponse<UsedOrAskBuyArticleViewModel>> GetPlatformUsedOrAskBuyArticleList(UsedOrAskBuyArticleQuery query, UserTicket user)
        {
            var result = new PageResponse<UsedOrAskBuyArticleViewModel>();
            if (query == null)
                return result;

            var select = DbContext.FreeSql.Select<CarUsedOrAskBuyArticleDetail>().From<CarBrandType, CarAccessoryType, CarProductType, CarNewOldDegree,
                CarAbrasionDegree, CarUsedOrAskBuyArticleLivenessDetail, CarUser>(
                (a, b, c, d, e, f, g, h) => a.LeftJoin(x => x.BrandId == b.Id)
                .LeftJoin(x => x.AccessoryTypeId == c.Id)
                .LeftJoin(x => x.ProductTypeId == d.Id)
                .LeftJoin(x => x.NewOldDegreeId == e.Id)
                .LeftJoin(x => x.AbrasionDegreeId == f.Id)
                .LeftJoin(x => x.Id == g.UsedOrAskBuyId)
                .LeftJoin(x => x.CreateBy == h.Id))
                .Where((a, b, c, d, e, f, g, h) => a.AuditStatus == AuditStatus.AuditSuccess && a.HitShelfStatus == HitShelfStatus.HitShelf
                 && a.UsedOrAskBuyType == query.UsedOrAskBuyType && a.IsDelete == CommonConstants.IsNotDelete);

            #region 条件查询
            if (!string.IsNullOrEmpty(query.TitleName))
                select.Where((a, b, c, d, e, f, g, h) => a.TitleName.Contains(query.TitleName));
            if (query.BrandId.HasValue)
                select.Where((a, b, c, d, e, f, g, h) => a.BrandId == query.BrandId);
            if (query.AccessoryTypeId.HasValue)
                select.Where((a, b, c, d, e, f, g, h) => a.AccessoryTypeId == query.AccessoryTypeId);
            if (query.ProductTypeId.HasValue)
                select.Where((a, b, c, d, e, f, g, h) => a.ProductTypeId == query.ProductTypeId);
            if (query.NewOldDegreeId.HasValue)
                select.Where((a, b, c, d, e, f, g, h) => a.NewOldDegreeId == query.NewOldDegreeId);
            if (query.AbrasionDegreeId.HasValue)
                select.Where((a, b, c, d, e, f, g, h) => a.AbrasionDegreeId == query.AbrasionDegreeId);
            if (!string.IsNullOrEmpty(query.ProvinceCode))
                select.Where((a, b, c, d, e, f, g, h) => a.ProvinceCode.Contains(query.ProvinceCode));
            if (!string.IsNullOrEmpty(query.CityCode))
                select.Where((a, b, c, d, e, f, g, h) => a.CityCode.Contains(query.CityCode));
            if (!string.IsNullOrEmpty(query.DistrictCode))
                select.Where((a, b, c, d, e, f, g, h) => a.DistrictCode.Contains(query.DistrictCode));
            if (query.BargainType.HasValue)
                select.Where((a, b, c, d, e, f, g, h) => a.BargainType == query.BargainType);
            #endregion

            var totalCount = select.ToList().Count();

            var items = select.OrderByDescending((a, b, c, d, e, f, g, h) => a.UpdateTime).Page(query.PageIndex, query.PageSize)
                .ToList((a, b, c, d, e, f, g, h) => new UsedOrAskBuyArticleViewModel
                {
                    Id = a.Id,
                    UsedOrAskBuyType = a.UsedOrAskBuyType,
                    TitleName = a.TitleName,
                    BrandId = a.BrandId,
                    BrandName = b.BrandName,
                    AccessoryTypeId = a.AccessoryTypeId,
                    AccessoryName = c.AccessoryName,
                    ProductTypeId = a.ProductTypeId,
                    ProductName = d.ProductName,
                    NewOldDegreeId = a.NewOldDegreeId,
                    NewOldName = e.NewOldName,
                    AbrasionDegreeId = a.AbrasionDegreeId,
                    AbrasionName = f.AbrasionName,
                    SaleReason = a.SaleReason,
                    ProvinceCode = a.ProvinceCode,
                    ProvinceName = a.ProvinceName,
                    CityCode = a.CityCode,
                    CityName = a.CityName,
                    DistrictCode = a.DistrictCode,
                    DistrictName = a.DistrictName,
                    DetailedAddress = a.DetailedAddress,
                    MobilePhone = a.MobilePhone,
                    BargainType = a.BargainType,
                    SerialNumber = a.SerialNumber,
                    BrandAdditional = a.BrandAdditional,
                    PreviewNumber = g.PreviewNumber,
                    CollectNumber = g.CollectNumber,
                    CreateTime = a.CreateTime,
                    CurrentTime = DateTime.Now,
                    CreateById = a.CreateBy,
                    CreateByName = h.NickName,
                    PictureLink = h.PictureLink,
                });

            //文章主键Ids
            List<long> articleIds = items.Select(x => x.Id).ToList();

            //查询文章图片列表
            List<CarUsedOrAskBuyArticlePicture> picDataList = await DbContext.FreeSql.GetRepository<CarUsedOrAskBuyArticlePicture>()
                .Where(x => articleIds.Contains(x.UsedOrAskBuyId) && x.IsDelete == CommonConstants.IsNotDelete).ToListAsync();

            //获取封面图
            items.ForEach(x =>
            {
                if (x.CreateById == user.Id)
                    x.IsOneSelf = true;

                x.CoverPictureUrl = picDataList.Where(a => a.UsedOrAskBuyId == x.Id && a.IsCoverPicture == true).FirstOrDefault()?.PictureUrl;
            });

            items.ForEach(x =>
            {

                List<UsedOrAskBuyArticlePictureViewModel> picList = picDataList.Where(a => a.UsedOrAskBuyId == x.Id).Select(a => new UsedOrAskBuyArticlePictureViewModel
                {
                    PictureUrl = a.PictureUrl,
                }).ToList();

                x.PictureList.AddRange(picList);
            });

            result.Data = items;
            result.Code = CommonConstants.SuccessCode;
            result.PageSize = query.PageSize;
            result.PageIndex = query.PageIndex;
            result.RecordCount = totalCount;
            return result;
        }
        #endregion

        #region 获取某一二手或求购文章详情
        /// <summary>
        /// 获取某一二手或求购文章详情
        /// </summary>
        /// <param name="articleId"></param>
        /// <param name="user"></param>
        /// <returns></returns>
        public async Task<ResponseContext<UsedOrAskBuyArticleDetailViewModel>> GetUsedOrAskBuyArticleDetail(long articleId, UserTicket user)
        {
            ResponseContext<UsedOrAskBuyArticleDetailViewModel> response = new ResponseContext<UsedOrAskBuyArticleDetailViewModel>();

            var dataList = DbContext.FreeSql.Select<CarUsedOrAskBuyArticleDetail>().From<CarBrandType, CarAccessoryType, CarProductType, CarNewOldDegree,
                CarAbrasionDegree, CarUsedOrAskBuyArticleLivenessDetail, CarUsedOrAskBuyArticleUserActiveDetail, CarUser>(
                (a, b, c, d, e, f, g, h, i) => a.LeftJoin(x => x.BrandId == b.Id)
                .LeftJoin(x => x.AccessoryTypeId == c.Id)
                .LeftJoin(x => x.ProductTypeId == d.Id)
                .LeftJoin(x => x.NewOldDegreeId == e.Id)
                .LeftJoin(x => x.AbrasionDegreeId == f.Id)
                .LeftJoin(x => x.Id == g.UsedOrAskBuyId)
                .LeftJoin(x => x.Id == h.UsedOrAskBuyId && h.UserId == user.Id)
                .LeftJoin(x => x.CreateBy == i.Id))
                .Where((a, b, c, d, e, f, g, h, i) => a.Id == articleId && a.IsDelete == CommonConstants.IsNotDelete)
                .ToList((a, b, c, d, e, f, g, h, i) => new UsedOrAskBuyArticleDetailViewModel
                {
                    Id = a.Id,
                    TitleName = a.TitleName,
                    BrandId = a.BrandId,
                    BrandName = b.BrandName,
                    AccessoryTypeId = a.AccessoryTypeId,
                    AccessoryName = c.AccessoryName,
                    ProductTypeId = a.ProductTypeId,
                    ProductName = d.ProductName,
                    NewOldDegreeId = a.NewOldDegreeId,
                    NewOldName = e.NewOldName,
                    AbrasionDegreeId = a.AbrasionDegreeId,
                    AbrasionName = f.AbrasionName,
                    SaleReason = a.SaleReason,
                    ProvinceCode = a.ProvinceCode,
                    ProvinceName = a.ProvinceName,
                    CityCode = a.CityCode,
                    CityName = a.CityName,
                    DistrictCode = a.DistrictCode,
                    DistrictName = a.DistrictName,
                    DetailedAddress = a.DetailedAddress,
                    MobilePhone = a.MobilePhone,
                    BargainType = a.BargainType,
                    SerialNumber = a.SerialNumber,
                    BrandAdditional = a.BrandAdditional,
                    CreateTime = a.CreateTime,
                    CurrentTime = DateTime.Now,
                    CreateById = a.CreateBy,
                    CreateByName = i.NickName,
                    PictureLink = i.PictureLink,
                    PreviewNumber = g.PreviewNumber,
                    CollectNumber = g.CollectNumber,
                    IsPreview = h.IsPreview,
                    IsCollect = h.IsCollect,
                });

            if (dataList == null || dataList.Count == 0)
                return response;

            response.Data = dataList.FirstOrDefault();

            //查询文章图片列表
            List<UsedOrAskBuyArticlePictureViewModel> picDataList = await DbContext.FreeSql.GetRepository<CarUsedOrAskBuyArticlePicture>()
                .Where(x => x.UsedOrAskBuyId == response.Data.Id && x.IsDelete == CommonConstants.IsNotDelete).ToListAsync(x =>
                new UsedOrAskBuyArticlePictureViewModel
                {
                    PictureUrl = x.PictureUrl
                });

            response.Data.PictureList.AddRange(picDataList);

            return response;
        }
        #endregion

        #region 删除自己发布的二手或求购文章
        /// <summary>
        /// 删除自己发布的二手或求购文章
        /// </summary>
        /// <param name="id"></param>
        /// <param name="user"></param>
        /// <returns></returns>
        public async Task<ResponseContext<bool>> DeleteOneselfUsedOrAskBuyArticle(long id, UserTicket user)
        {
            CarUsedOrAskBuyArticleDetail entity = await DbContext.FreeSql.GetRepository<CarUsedOrAskBuyArticleDetail>().Where(x => x.Id == id).FirstAsync();

            if (entity == null)
                return new ResponseContext<bool> { Code = CommonConstants.ErrorCode, Data = false, Msg = "文章不存在!" };
            if (entity.CreateBy != user.Id)
                return new ResponseContext<bool> { Code = CommonConstants.ErrorCode, Data = false, Msg = "不是自己的文章,暂不支持删除!" };

            entity.IsDelete = 1;
            entity.UpdateTime = DateTime.Now;
            entity.UpdateBy = user.Id;

            bool state = DbContext.FreeSql.GetRepository<CarUsedOrAskBuyArticleDetail>().UpdateDiy.SetSource(entity)
                .UpdateColumns(x => new
                {
                    x.IsDelete,
                    x.UpdateTime,
                    x.UpdateBy
                }).ExecuteAffrows() > 0;

            return new ResponseContext<bool> { Code = state ? CommonConstants.SuccessCode : CommonConstants.ErrorCode, Data = state, Msg = state ? "成功!" : "操作失败!" };
        }
        #endregion

        #region 用户操作二手或求购文章活跃程度(查看、收藏)
        /// <summary>
        /// 用户操作二手或求购文章活跃程度(查看、收藏)
        /// </summary>
        /// <param name="model"></param>
        /// <param name="user"></param>
        /// <returns></returns>
        public async Task<ResponseContext<bool>> UserOperationUsedOrAskBuyArticleForActive(UsedOrAskBuyArticleUserActiveRequestModel model, UserTicket user)
        {
            if (model == null || model.UsedOrAskBuyId == 0)
                return new ResponseContext<bool> { Code = CommonConstants.ErrorCode, Data = false, Msg = "参数不允许为空!" };

            //二手或求购文章用户活跃详情
            CarUsedOrAskBuyArticleUserActiveDetail data = await DbContext.FreeSql.GetRepository<CarUsedOrAskBuyArticleUserActiveDetail>()
                .Where(x => x.UsedOrAskBuyId == model.UsedOrAskBuyId && x.UserId == user.Id && x.IsDelete == CommonConstants.IsNotDelete).FirstAsync();

            if (data == null && model.OperationType != UserActiveOperationType.Preview)
                return new ResponseContext<bool> { Code = CommonConstants.ErrorCode, Data = false, Msg = "操作类型有误!" };

            //二手或求购文章活跃度详情
            CarUsedOrAskBuyArticleLivenessDetail livenessData = await DbContext.FreeSql.GetRepository<CarUsedOrAskBuyArticleLivenessDetail>()
                .Where(x => x.UsedOrAskBuyId == model.UsedOrAskBuyId && x.IsDelete == CommonConstants.IsNotDelete).FirstAsync();

            bool state = false;

            if (data == null)
            {
                data = new CarUsedOrAskBuyArticleUserActiveDetail
                {
                    Id = IdWorker.NextId(),
                    UsedOrAskBuyId = model.UsedOrAskBuyId,
                    UserId = user.Id,
                    IsPreview = true,
                    CreateBy = user.Id,
                    CreateTime = DateTime.Now,
                    UpdateBy = user.Id,
                    UpdateTime = DateTime.Now,
                };

                if (livenessData == null)
                {
                    livenessData = new CarUsedOrAskBuyArticleLivenessDetail
                    {
                        UsedOrAskBuyId = model.UsedOrAskBuyId,
                        PreviewNumber = 1,
                        CreateBy = user.Id,
                        CreateTime = DateTime.Now,
                        UpdateBy = user.Id,
                        UpdateTime = DateTime.Now,
                    };

                    //添加文章查看数
                    await DbContext.FreeSql.Insert<CarUsedOrAskBuyArticleLivenessDetail>(livenessData).ExecuteAffrowsAsync();
                }
                else
                {
                    livenessData.PreviewNumber = livenessData.PreviewNumber + 1;

                    //更新文章查看数
                    await DbContext.FreeSql.GetRepository<CarUsedOrAskBuyArticleLivenessDetail>().UpdateDiy.SetSource(livenessData).UpdateColumns(x => new
                    {
                        x.PreviewNumber,
                        x.UpdateBy,
                        x.UpdateTime
                    }).ExecuteAffrowsAsync();
                }

                state = await DbContext.FreeSql.Insert<CarUsedOrAskBuyArticleUserActiveDetail>(data).ExecuteAffrowsAsync() > 0;
            }
            else
            {
                if (livenessData == null)
                    return new ResponseContext<bool> { Code = CommonConstants.ErrorCode, Data = false, Msg = "文章活跃度统计有误!" };

                livenessData.UpdateBy = user.Id;
                livenessData.UpdateTime = DateTime.Now;

                data.UpdateBy = user.Id;
                data.UpdateTime = DateTime.Now;

                bool tempBoole;

                //收藏
                if (model.OperationType == UserActiveOperationType.Collect)
                {
                    tempBoole = data.IsCollect;
                    data.IsCollect = tempBoole ? false : true;

                    if (data.IsCollect)
                        livenessData.CollectNumber = livenessData.CollectNumber + 1;
                    else
                        livenessData.CollectNumber = livenessData.CollectNumber - 1;
                }

                #region 更新数据
                using (var uow = DbContext.FreeSql.CreateUnitOfWork())
                {
                    try
                    {
                        //更新文章活跃度
                        await uow.GetRepository<CarUsedOrAskBuyArticleLivenessDetail>().UpdateDiy.SetSource(livenessData).UpdateColumns(x => new
                        {
                            x.CollectNumber,
                            x.UpdateBy,
                            x.UpdateTime
                        }).ExecuteAffrowsAsync();

                        //更新文章用户活跃度
                        state = await uow.GetRepository<CarUsedOrAskBuyArticleUserActiveDetail>().UpdateDiy.SetSource(data).UpdateColumns(x => new
                        {
                            x.IsCollect,
                            x.UpdateBy,
                            x.UpdateTime
                        }).ExecuteAffrowsAsync() > 0;

                        if (state)
                            uow.Commit();
                        else
                            uow.Rollback();
                    }
                    catch (Exception ex)
                    {
                        uow.Rollback();
                    }
                }
                #endregion
            }

            return new ResponseContext<bool>
            {
                Code = state ? CommonConstants.SuccessCode : CommonConstants.ErrorCode,
                Data = state,
                Msg = state ? "成功!" : "操作失败!"
            };
        }
        #endregion

        #region 获取个人发布/收藏二手或求购文章列表
        /// <summary>
        /// 获取个人发布/收藏二手或求购文章列表
        /// </summary>
        /// <param name="query"></param>
        /// <param name="user"></param>
        /// <returns></returns>
        public async Task<PageResponse<UsedOrAskBuyArticleViewModel>> GetOneSelfUsedOrAskBuyArticleList(UsedOrAskBuyArticleOneselfQuery query, UserTicket user)
        {
            var result = new PageResponse<UsedOrAskBuyArticleViewModel>();
            if (query == null)
                return result;

            int totalCount = 0;
            List<UsedOrAskBuyArticleViewModel> items = new List<UsedOrAskBuyArticleViewModel>();

            //查询个人发布文章
            if (query.ResourceSource == ResourcePersonSource.DoOneOwn)
            {
                var select = DbContext.FreeSql.Select<CarUsedOrAskBuyArticleDetail>().From<CarBrandType, CarAccessoryType, CarProductType, CarNewOldDegree,
                    CarAbrasionDegree, CarUsedOrAskBuyArticleLivenessDetail, CarUser>(
                    (a, b, c, d, e, f, g, h) => a.LeftJoin(x => x.BrandId == b.Id)
                    .LeftJoin(x => x.AccessoryTypeId == c.Id)
                    .LeftJoin(x => x.ProductTypeId == d.Id)
                    .LeftJoin(x => x.NewOldDegreeId == e.Id)
                    .LeftJoin(x => x.AbrasionDegreeId == f.Id)
                    .LeftJoin(x => x.Id == g.UsedOrAskBuyId)
                    .LeftJoin(x => x.CreateBy == h.Id))
                    .Where((a, b, c, d, e, f, g, h) => a.AuditStatus == AuditStatus.AuditSuccess && a.HitShelfStatus == HitShelfStatus.HitShelf
                     && a.UsedOrAskBuyType == query.UsedOrAskBuyType && a.IsDelete == CommonConstants.IsNotDelete);

                #region 条件查询
                if (!string.IsNullOrEmpty(query.TitleName))
                    select.Where((a, b, c, d, e, f, g, h) => a.TitleName.Contains(query.TitleName));
                if (query.BrandId.HasValue)
                    select.Where((a, b, c, d, e, f, g, h) => a.BrandId == query.BrandId);
                if (query.AccessoryTypeId.HasValue)
                    select.Where((a, b, c, d, e, f, g, h) => a.AccessoryTypeId == query.AccessoryTypeId);
                if (query.ProductTypeId.HasValue)
                    select.Where((a, b, c, d, e, f, g, h) => a.ProductTypeId == query.ProductTypeId);
                if (query.NewOldDegreeId.HasValue)
                    select.Where((a, b, c, d, e, f, g, h) => a.NewOldDegreeId == query.NewOldDegreeId);
                if (query.AbrasionDegreeId.HasValue)
                    select.Where((a, b, c, d, e, f, g, h) => a.AbrasionDegreeId == query.AbrasionDegreeId);
                if (!string.IsNullOrEmpty(query.ProvinceCode))
                    select.Where((a, b, c, d, e, f, g, h) => a.ProvinceCode.Contains(query.ProvinceCode));
                if (!string.IsNullOrEmpty(query.CityCode))
                    select.Where((a, b, c, d, e, f, g, h) => a.CityCode.Contains(query.CityCode));
                if (!string.IsNullOrEmpty(query.DistrictCode))
                    select.Where((a, b, c, d, e, f, g, h) => a.DistrictCode.Contains(query.DistrictCode));
                if (query.BargainType.HasValue)
                    select.Where((a, b, c, d, e, f, g, h) => a.BargainType == query.BargainType);
                #endregion

                totalCount = select.ToList().Count();

                items = select.OrderByDescending((a, b, c, d, e, f, g, h) => a.UpdateTime).Page(query.PageIndex, query.PageSize)
                    .ToList((a, b, c, d, e, f, g, h) => new UsedOrAskBuyArticleViewModel
                    {
                        Id = a.Id,
                        UsedOrAskBuyType = a.UsedOrAskBuyType,
                        TitleName = a.TitleName,
                        BrandId = a.BrandId,
                        BrandName = b.BrandName,
                        AccessoryTypeId = a.AccessoryTypeId,
                        AccessoryName = c.AccessoryName,
                        ProductTypeId = a.ProductTypeId,
                        ProductName = d.ProductName,
                        NewOldDegreeId = a.NewOldDegreeId,
                        NewOldName = e.NewOldName,
                        AbrasionDegreeId = a.AbrasionDegreeId,
                        AbrasionName = f.AbrasionName,
                        SaleReason = a.SaleReason,
                        ProvinceCode = a.ProvinceCode,
                        ProvinceName = a.ProvinceName,
                        CityCode = a.CityCode,
                        CityName = a.CityName,
                        DistrictCode = a.DistrictCode,
                        DistrictName = a.DistrictName,
                        DetailedAddress = a.DetailedAddress,
                        MobilePhone = a.MobilePhone,
                        BargainType = a.BargainType,
                        SerialNumber = a.SerialNumber,
                        BrandAdditional = a.BrandAdditional,
                        PreviewNumber = g.PreviewNumber,
                        CollectNumber = g.CollectNumber,
                        CreateTime = a.CreateTime,
                        CurrentTime = DateTime.Now,
                        CreateById = a.CreateBy,
                        CreateByName = h.NickName,
                        PictureLink = h.PictureLink,
                    });
            }

            //查询个人收藏文章
            if (query.ResourceSource == ResourcePersonSource.Collect)
            {
                var select = DbContext.FreeSql.Select<CarUsedOrAskBuyArticleDetail>().From<CarUsedOrAskBuyArticleUserActiveDetail, CarBrandType,
                    CarAccessoryType, CarProductType, CarNewOldDegree, CarAbrasionDegree, CarUsedOrAskBuyArticleLivenessDetail, CarUser>(
                    (a, b, c, d, e, f, g, h, i) => a.InnerJoin(x => x.Id == b.UsedOrAskBuyId)
                    .LeftJoin(x => x.BrandId == c.Id)
                    .LeftJoin(x => x.AccessoryTypeId == d.Id)
                    .LeftJoin(x => x.ProductTypeId == e.Id)
                    .LeftJoin(x => x.NewOldDegreeId == f.Id)
                    .LeftJoin(x => x.AbrasionDegreeId == g.Id)
                    .LeftJoin(x => x.Id == h.UsedOrAskBuyId)
                    .LeftJoin(x => x.CreateBy == i.Id))
                    .Where((a, b, c, d, e, f, g, h, i) => a.AuditStatus == AuditStatus.AuditSuccess && a.HitShelfStatus == HitShelfStatus.HitShelf &&
                     b.UserId == user.Id && b.IsCollect == true && a.UsedOrAskBuyType == query.UsedOrAskBuyType && a.IsDelete == CommonConstants.IsNotDelete
                      && b.IsDelete == CommonConstants.IsNotDelete);

                #region 条件查询
                if (!string.IsNullOrEmpty(query.TitleName))
                    select.Where((a, b, c, d, e, f, g, h, i) => a.TitleName.Contains(query.TitleName));
                if (query.BrandId.HasValue)
                    select.Where((a, b, c, d, e, f, g, h, i) => a.BrandId == query.BrandId);
                if (query.AccessoryTypeId.HasValue)
                    select.Where((a, b, c, d, e, f, g, h, i) => a.AccessoryTypeId == query.AccessoryTypeId);
                if (query.ProductTypeId.HasValue)
                    select.Where((a, b, c, d, e, f, g, h, i) => a.ProductTypeId == query.ProductTypeId);
                if (query.NewOldDegreeId.HasValue)
                    select.Where((a, b, c, d, e, f, g, h, i) => a.NewOldDegreeId == query.NewOldDegreeId);
                if (query.AbrasionDegreeId.HasValue)
                    select.Where((a, b, c, d, e, f, g, h, i) => a.AbrasionDegreeId == query.AbrasionDegreeId);
                if (!string.IsNullOrEmpty(query.ProvinceCode))
                    select.Where((a, b, c, d, e, f, g, h, i) => a.ProvinceCode.Contains(query.ProvinceCode));
                if (!string.IsNullOrEmpty(query.CityCode))
                    select.Where((a, b, c, d, e, f, g, h, i) => a.CityCode.Contains(query.CityCode));
                if (!string.IsNullOrEmpty(query.DistrictCode))
                    select.Where((a, b, c, d, e, f, g, h, i) => a.DistrictCode.Contains(query.DistrictCode));
                if (query.BargainType.HasValue)
                    select.Where((a, b, c, d, e, f, g, h, i) => a.BargainType == query.BargainType);
                #endregion

                totalCount = select.ToList().Count();

                items = select.OrderByDescending((a, b, c, d, e, f, g, h, i) => a.UpdateTime).Page(query.PageIndex, query.PageSize)
                    .ToList((a, b, c, d, e, f, g, h, i) => new UsedOrAskBuyArticleViewModel
                    {
                        Id = a.Id,
                        UsedOrAskBuyType = a.UsedOrAskBuyType,
                        TitleName = a.TitleName,
                        BrandId = a.BrandId,
                        BrandName = c.BrandName,
                        AccessoryTypeId = a.AccessoryTypeId,
                        AccessoryName = d.AccessoryName,
                        ProductTypeId = a.ProductTypeId,
                        ProductName = e.ProductName,
                        NewOldDegreeId = a.NewOldDegreeId,
                        NewOldName = f.NewOldName,
                        AbrasionDegreeId = a.AbrasionDegreeId,
                        AbrasionName = g.AbrasionName,
                        SaleReason = a.SaleReason,
                        ProvinceCode = a.ProvinceCode,
                        ProvinceName = a.ProvinceName,
                        CityCode = a.CityCode,
                        CityName = a.CityName,
                        DistrictCode = a.DistrictCode,
                        DistrictName = a.DistrictName,
                        DetailedAddress = a.DetailedAddress,
                        MobilePhone = a.MobilePhone,
                        BargainType = a.BargainType,
                        SerialNumber = a.SerialNumber,
                        BrandAdditional = a.BrandAdditional,
                        PreviewNumber = h.PreviewNumber,
                        CollectNumber = h.CollectNumber,
                        CreateTime = a.CreateTime,
                        CurrentTime = DateTime.Now,
                        CreateById = a.CreateBy,
                        CreateByName = i.NickName,
                        PictureLink = i.PictureLink,
                    });
            }

            //文章主键Ids
            List<long> articleIds = items.Select(x => x.Id).ToList();

            //查询文章图片列表
            List<CarUsedOrAskBuyArticlePicture> picDataList = await DbContext.FreeSql.GetRepository<CarUsedOrAskBuyArticlePicture>()
                .Where(x => articleIds.Contains(x.UsedOrAskBuyId) && x.IsDelete == CommonConstants.IsNotDelete).ToListAsync();

            //获取封面图
            items.ForEach(x =>
            {
                if (x.CreateById == user.Id)
                    x.IsOneSelf = true;

                x.CoverPictureUrl = picDataList.Where(a => a.UsedOrAskBuyId == x.Id && a.IsCoverPicture == true).FirstOrDefault()?.PictureUrl;
            });

            items.ForEach(x =>
            {

                List<UsedOrAskBuyArticlePictureViewModel> picList = picDataList.Where(a => a.UsedOrAskBuyId == x.Id).Select(a => new UsedOrAskBuyArticlePictureViewModel
                {
                    PictureUrl = a.PictureUrl,
                }).ToList();

                x.PictureList.AddRange(picList);
            });

            result.Data = items;
            result.Code = CommonConstants.SuccessCode;
            result.PageSize = query.PageSize;
            result.PageIndex = query.PageIndex;
            result.RecordCount = totalCount;
            return result;
        }
        #endregion

    }
}
