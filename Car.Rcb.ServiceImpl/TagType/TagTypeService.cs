﻿using Car.Framework;
using Car.Framework.Auth;
using Car.Framework.Authorizations;
using Car.Framework.Dal;
using Car.Framework.Entity;
using Car.Framework.Utils;
using Car.Rcb.Dto;
using Car.Rcb.IServices;
using Car.Rcb.Pocos;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Tas.Common.IdGenerate;

namespace Car.Rcb.ServiceImpl
{
    /// <summary>
    /// 标签类型相关服务实现
    /// </summary>
    public class TagTypeService : ITagTypeService
    {
        #region 获取技术类标签列表
        /// <summary>
        /// 获取技术类标签列表
        /// </summary>
        /// <returns></returns>
        public async Task<ResponseContext<List<TechnologyTagViewModel>>> GetTechnologyTagList()
        {
            ResponseContext<List<TechnologyTagViewModel>> response = new ResponseContext<List<TechnologyTagViewModel>>();

            response.Data = await DbContext.FreeSql.GetRepository<CarTechnologyTag>()
                .Where(x => x.DataUseStatus == DataUseStatus.Enabled && x.IsDelete == CommonConstants.IsNotDelete)
                .ToListAsync(x => new TechnologyTagViewModel
                {
                    Id = x.Id,
                    TagName = x.TagName,
                    IconLink = x.IconLink,
                    Sort = x.Sort,
                });

            return response;
        }
        #endregion

        #region 获取品牌类型列表
        /// <summary>
        /// 获取品牌类型列表
        /// </summary>
        /// <returns></returns>
        public async Task<ResponseContext<List<BrandTypeViewModel>>> GetBrandTypeList()
        {
            ResponseContext<List<BrandTypeViewModel>> response = new ResponseContext<List<BrandTypeViewModel>>();

            response.Data = await DbContext.FreeSql.GetRepository<CarBrandType>()
                .Where(x => x.DataUseStatus == DataUseStatus.Enabled && x.IsDelete == CommonConstants.IsNotDelete)
                .ToListAsync(x => new BrandTypeViewModel
                {
                    Id = x.Id,
                    BrandName = x.BrandName,
                    Sort = x.Sort
                });

            return response;
        }
        #endregion

        #region 获取配件分类列表
        /// <summary>
        /// 获取配件分类列表
        /// </summary>
        /// <returns></returns>
        public async Task<ResponseContext<List<AccessoryTypeViewModel>>> GetAccessoryTypeList()
        {
            ResponseContext<List<AccessoryTypeViewModel>> response = new ResponseContext<List<AccessoryTypeViewModel>>();

            response.Data = await DbContext.FreeSql.GetRepository<CarAccessoryType>()
                .Where(x => x.DataUseStatus == DataUseStatus.Enabled && x.IsDelete == CommonConstants.IsNotDelete)
                .ToListAsync(x => new AccessoryTypeViewModel
                {
                    Id = x.Id,
                    AccessoryName = x.AccessoryName,
                    Sort = x.Sort
                });

            return response;
        }
        #endregion

        #region 获取产品类型列表(根据配件分类)
        /// <summary>
        /// 获取产品类型列表(根据配件分类)
        /// </summary>
        /// <param name="accessoryId">配件分类Id</param>
        /// <returns></returns>
        public async Task<ResponseContext<List<ProductTypeViewModel>>> GetProductTypeList(long accessoryId)
        {
            ResponseContext<List<ProductTypeViewModel>> response = new ResponseContext<List<ProductTypeViewModel>>();

            if (accessoryId == 0)
                return response;

            response.Data = await DbContext.FreeSql.GetRepository<CarProductType>()
                .Where(x => x.AccessoryIds.Contains(accessoryId.ToString()) && x.DataUseStatus == DataUseStatus.Enabled && x.IsDelete == CommonConstants.IsNotDelete)
                .ToListAsync(x => new ProductTypeViewModel
                {
                    Id = x.Id,
                    ProductName = x.ProductName,
                    Sort = x.Sort
                });

            return response;
        }
        #endregion

        #region 获取新旧程度列表
        /// <summary>
        /// 获取新旧程度列表
        /// </summary>
        /// <returns></returns>
        public async Task<ResponseContext<List<NewOldDegreeViewModel>>> GetNewOldDegreeList()
        {
            ResponseContext<List<NewOldDegreeViewModel>> response = new ResponseContext<List<NewOldDegreeViewModel>>();

            response.Data = await DbContext.FreeSql.GetRepository<CarNewOldDegree>()
                .Where(x => x.DataUseStatus == DataUseStatus.Enabled && x.IsDelete == CommonConstants.IsNotDelete)
                .ToListAsync(x => new NewOldDegreeViewModel
                {
                    Id = x.Id,
                    NewOldName = x.NewOldName,
                    Sort = x.Sort
                });

            return response;
        }
        #endregion

        #region 获取磨损程度列表(根据新旧程度)
        /// <summary>
        /// 获取磨损程度列表(根据新旧程度)
        /// </summary>
        /// <param name="newOldId">新旧程度Id</param>
        /// <returns></returns>
        public async Task<ResponseContext<List<AbrasionDegreeViewModel>>> GetAbrasionDegreeList(long newOldId)
        {
            ResponseContext<List<AbrasionDegreeViewModel>> response = new ResponseContext<List<AbrasionDegreeViewModel>>();

            if (newOldId == 0)
                return response;

            response.Data = await DbContext.FreeSql.GetRepository<CarAbrasionDegree>()
                .Where(x => x.NewOldIds.Contains(newOldId.ToString()) && x.DataUseStatus == DataUseStatus.Enabled && x.IsDelete == CommonConstants.IsNotDelete)
                .ToListAsync(x => new AbrasionDegreeViewModel
                {
                    Id = x.Id,
                    AbrasionName = x.AbrasionName,
                    Sort = x.Sort
                });

            return response;
        }
        #endregion

    }
}
