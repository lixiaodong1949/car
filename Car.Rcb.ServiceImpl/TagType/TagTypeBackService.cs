﻿using Car.Framework;
using Car.Framework.Auth;
using Car.Framework.Authorizations;
using Car.Framework.Dal;
using Car.Framework.Entity;
using Car.Framework.Utils;
using Car.Rcb.Dto;
using Car.Rcb.IServices;
using Car.Rcb.Pocos;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Tas.Common.IdGenerate;

namespace Car.Rcb.ServiceImpl
{
    /// <summary>
    /// 标签类型管理相关服务实现
    /// </summary>
    public class TagTypeBackService : ITagTypeBackService
    {
        #region 技术标签 TechnologyTag

        #region 新增/编辑技术标签
        /// <summary>
        /// 新增/编辑技术标签
        /// </summary>
        /// <param name="model"></param>
        /// <param name="user"></param>
        /// <returns></returns>
        public async Task<ResponseContext<bool>> AddOrEditTechnologyTag(TechnologyTagRequestModel model, UserTicket user)
        {
            if (model == null || string.IsNullOrEmpty(model.TagName) || string.IsNullOrEmpty(model.IconLink))
                return new ResponseContext<bool> { Code = CommonConstants.ErrorCode, Msg = "参数不能为空!" };

            CarTechnologyTag entity = new CarTechnologyTag
            {
                Id = model.Id > 0 ? model.Id : IdWorker.NextId(),
                TagName = model.TagName,
                IconLink = model.IconLink,
                Sort = model.Sort,

                UpdateBy = user.Id,
                UpdateTime = DateTime.Now
            };

            bool state = false;

            if (model.Id == 0)
            {
                entity.DataUseStatus = DataUseStatus.Enabled;
                entity.CreateBy = user.Id;
                entity.CreateTime = DateTime.Now;

                CarTechnologyTag ttModel = await DbContext.FreeSql.GetRepository<CarTechnologyTag>().InsertAsync(entity);
                state = ttModel != null;
            }
            else
            {
                CarTechnologyTag dbData = await DbContext.FreeSql.GetRepository<CarTechnologyTag>().Where(x => x.Id != model.Id &&
                    x.TagName == model.TagName && x.IsDelete == CommonConstants.IsNotDelete).FirstAsync();
                if (dbData != null)
                    return new ResponseContext<bool> { Code = CommonConstants.ErrorCode, Msg = "标签名称已存在!" };

                state = await DbContext.FreeSql.GetRepository<CarTechnologyTag>().UpdateDiy.SetSource(entity).UpdateColumns(x => new
                {
                    x.TagName,
                    x.IconLink,
                    x.Sort,
                    x.UpdateBy,
                    x.UpdateTime
                }).ExecuteAffrowsAsync() > 0;
            }

            return new ResponseContext<bool> { Code = state ? CommonConstants.SuccessCode : CommonConstants.ErrorCode, Data = state, Msg = state ? "成功!" : "操作失败!" };
        }
        #endregion

        #region 启用或禁用技术标签
        /// <summary>
        /// 启用或禁用技术标签
        /// </summary>
        /// <param name="model"></param>
        /// <param name="user"></param>
        /// <returns></returns>
        public async Task<ResponseContext<bool>> EnabledOrDisabledTechnologyTag(EnabledOrDisabledDataRequestModel model, UserTicket user)
        {
            if (model == null)
                return new ResponseContext<bool> { Code = CommonConstants.ErrorCode, Msg = "参数不能为空!" };

            CarTechnologyTag entity = await DbContext.FreeSql.GetRepository<CarTechnologyTag>().Where(x => x.Id == model.Id).FirstAsync();
            if (entity == null)
                return new ResponseContext<bool> { Code = CommonConstants.ErrorCode, Msg = "数据不存在!" };

            if (entity.DataUseStatus == model.DataUseStatus)
            {
                if (entity.DataUseStatus == DataUseStatus.Enabled)
                    return new ResponseContext<bool> { Code = CommonConstants.ErrorCode, Msg = "已是启用状态!" };
                if (entity.DataUseStatus == DataUseStatus.Disabled)
                    return new ResponseContext<bool> { Code = CommonConstants.ErrorCode, Msg = "已是禁用状态!" };
            }

            entity.DataUseStatus = model.DataUseStatus;
            entity.UpdateTime = DateTime.Now;
            entity.UpdateBy = user.Id;

            bool state = await DbContext.FreeSql.GetRepository<CarTechnologyTag>().UpdateDiy.SetSource(entity).UpdateColumns(x => new
            {
                x.DataUseStatus,
                x.UpdateTime,
                x.UpdateBy
            }).ExecuteAffrowsAsync() > 0;

            return new ResponseContext<bool> { Code = state ? CommonConstants.SuccessCode : CommonConstants.ErrorCode, Data = state, Msg = state ? "成功!" : "操作失败!" };
        }
        #endregion

        #region 删除技术标签
        /// <summary>
        /// 删除技术标签
        /// </summary>
        /// <param name="model"></param>
        /// <param name="user"></param>
        /// <returns></returns>
        public async Task<ResponseContext<bool>> DeleteTechnologyTag(DeleteDataRequestModel model, UserTicket user)
        {
            var list = await DbContext.FreeSql.GetRepository<CarTechnologyTag>().Where(x => model.Ids.Contains(x.Id)).ToListAsync();

            if (list == null || list.Count == 0)
                return new ResponseContext<bool> { Code = CommonConstants.ErrorCode, Data = false, Msg = "没有该数据源!" };

            list.ForEach(x =>
            {
                x.IsDelete = CommonConstants.IsDelete;
                x.UpdateTime = DateTime.Now;
                x.UpdateBy = user.Id;
            });

            bool state = await DbContext.FreeSql.GetRepository<CarTechnologyTag>().UpdateDiy.SetSource(list).UpdateColumns(x => new
            {
                x.IsDelete,
                x.UpdateTime,
                x.UpdateBy
            }).ExecuteAffrowsAsync() > 0;

            return new ResponseContext<bool> { Code = state ? CommonConstants.SuccessCode : CommonConstants.ErrorCode, Data = state, Msg = state ? "成功!" : "操作失败!" };
        }
        #endregion

        #region 获取技术类标签列表(后台管理)
        /// <summary>
        /// 获取技术类标签列表(后台管理)
        /// </summary>
        /// <param name="query"></param>
        /// <returns></returns>
        public async Task<PageResponse<TechnologyTagBackViewModel>> GetTechnologyTagBackList(TechnologyTagQuery query)
        {
            var result = new PageResponse<TechnologyTagBackViewModel>();

            var select = DbContext.FreeSql.GetRepository<CarTechnologyTag>()
                .Where(a => a.IsDelete == CommonConstants.IsNotDelete);

            if (query != null && !string.IsNullOrEmpty(query.TagName))
                select.Where(a => a.TagName.Contains(query.TagName));
            if (query != null && query.DataUseStatus.HasValue)
                select.Where(a => a.DataUseStatus == query.DataUseStatus.Value);

            var totalCount = select.ToList().Count();

            var items = await select.OrderByDescending(x => x.Sort).Page(query.PageIndex, query.PageSize)
                .ToListAsync(x => new TechnologyTagBackViewModel
                {
                    Id = x.Id,
                    TagName = x.TagName,
                    IconLink = x.IconLink,
                    DataUseStatus = x.DataUseStatus,
                    Sort = x.Sort
                });

            result.Data = items;
            result.Code = CommonConstants.SuccessCode;
            result.PageSize = query.PageSize;
            result.PageIndex = query.PageIndex;
            result.RecordCount = totalCount;
            return result;
        }
        #endregion

        #endregion

        #region 品牌类型 BrandType

        #region 新增/编辑品牌类型
        /// <summary>
        /// 新增/编辑品牌类型
        /// </summary>
        /// <param name="model"></param>
        /// <param name="user"></param>
        /// <returns></returns>
        public async Task<ResponseContext<bool>> AddOrEditBrandType(BrandTypeRequestModel model, UserTicket user)
        {
            if (model == null || string.IsNullOrEmpty(model.BrandName))
                return new ResponseContext<bool> { Code = CommonConstants.ErrorCode, Msg = "参数不能为空!" };

            CarBrandType entity = new CarBrandType
            {
                Id = model.Id > 0 ? model.Id : IdWorker.NextId(),
                BrandName = model.BrandName,
                Sort = model.Sort,

                UpdateBy = user.Id,
                UpdateTime = DateTime.Now
            };

            bool state = false;

            if (model.Id == 0)
            {
                entity.DataUseStatus = DataUseStatus.Enabled;
                entity.CreateBy = user.Id;
                entity.CreateTime = DateTime.Now;

                CarBrandType btModel = await DbContext.FreeSql.GetRepository<CarBrandType>().InsertAsync(entity);
                state = btModel != null;
            }
            else
            {
                CarBrandType dbData = await DbContext.FreeSql.GetRepository<CarBrandType>().Where(x => x.Id != model.Id &&
                    x.BrandName == model.BrandName && x.IsDelete == CommonConstants.IsNotDelete).FirstAsync();
                if (dbData != null)
                    return new ResponseContext<bool> { Code = CommonConstants.ErrorCode, Msg = "品牌名称已存在!" };

                state = await DbContext.FreeSql.GetRepository<CarBrandType>().UpdateDiy.SetSource(entity).UpdateColumns(x => new
                {
                    x.BrandName,
                    x.Sort,
                    x.UpdateBy,
                    x.UpdateTime
                }).ExecuteAffrowsAsync() > 0;
            }

            return new ResponseContext<bool> { Code = state ? CommonConstants.SuccessCode : CommonConstants.ErrorCode, Data = state, Msg = state ? "成功!" : "操作失败!" };
        }
        #endregion

        #region 启用或禁用品牌类型
        /// <summary>
        /// 启用或禁用品牌类型
        /// </summary>
        /// <param name="model"></param>
        /// <param name="user"></param>
        /// <returns></returns>
        public async Task<ResponseContext<bool>> EnabledOrDisabledBrandType(EnabledOrDisabledDataRequestModel model, UserTicket user)
        {
            if (model == null)
                return new ResponseContext<bool> { Code = CommonConstants.ErrorCode, Msg = "参数不能为空!" };

            CarBrandType entity = await DbContext.FreeSql.GetRepository<CarBrandType>().Where(x => x.Id == model.Id).FirstAsync();
            if (entity == null)
                return new ResponseContext<bool> { Code = CommonConstants.ErrorCode, Msg = "数据不存在!" };

            if (entity.DataUseStatus == model.DataUseStatus)
            {
                if (entity.DataUseStatus == DataUseStatus.Enabled)
                    return new ResponseContext<bool> { Code = CommonConstants.ErrorCode, Msg = "已是启用状态!" };
                if (entity.DataUseStatus == DataUseStatus.Disabled)
                    return new ResponseContext<bool> { Code = CommonConstants.ErrorCode, Msg = "已是禁用状态!" };
            }

            entity.DataUseStatus = model.DataUseStatus;
            entity.UpdateTime = DateTime.Now;
            entity.UpdateBy = user.Id;

            bool state = await DbContext.FreeSql.GetRepository<CarBrandType>().UpdateDiy.SetSource(entity).UpdateColumns(x => new
            {
                x.DataUseStatus,
                x.UpdateTime,
                x.UpdateBy
            }).ExecuteAffrowsAsync() > 0;

            return new ResponseContext<bool> { Code = state ? CommonConstants.SuccessCode : CommonConstants.ErrorCode, Data = state, Msg = state ? "成功!" : "操作失败!" };
        }
        #endregion

        #region 删除品牌类型
        /// <summary>
        /// 删除品牌类型
        /// </summary>
        /// <param name="model"></param>
        /// <param name="user"></param>
        /// <returns></returns>
        public async Task<ResponseContext<bool>> DeleteBrandType(DeleteDataRequestModel model, UserTicket user)
        {
            var list = await DbContext.FreeSql.GetRepository<CarBrandType>().Where(x => model.Ids.Contains(x.Id)).ToListAsync();

            if (list == null || list.Count == 0)
                return new ResponseContext<bool> { Code = CommonConstants.ErrorCode, Data = false, Msg = "没有该数据源!" };

            list.ForEach(x =>
            {
                x.IsDelete = CommonConstants.IsDelete;
                x.UpdateTime = DateTime.Now;
                x.UpdateBy = user.Id;
            });

            bool state = await DbContext.FreeSql.GetRepository<CarBrandType>().UpdateDiy.SetSource(list).UpdateColumns(x => new
            {
                x.IsDelete,
                x.UpdateTime,
                x.UpdateBy
            }).ExecuteAffrowsAsync() > 0;

            return new ResponseContext<bool> { Code = state ? CommonConstants.SuccessCode : CommonConstants.ErrorCode, Data = state, Msg = state ? "成功!" : "操作失败!" };
        }
        #endregion

        #region 获取品牌类型列表(后台管理)
        /// <summary>
        /// 获取品牌类型列表(后台管理)
        /// </summary>
        /// <param name="query"></param>
        /// <returns></returns>
        public async Task<PageResponse<BrandTypeBackViewModel>> GetBrandTypeBackList(BrandTypeQuery query)
        {
            var result = new PageResponse<BrandTypeBackViewModel>();

            var select = DbContext.FreeSql.GetRepository<CarBrandType>()
                .Where(a => a.IsDelete == CommonConstants.IsNotDelete);

            if (query != null && !string.IsNullOrEmpty(query.BrandName))
                select.Where(a => a.BrandName.Contains(query.BrandName));
            if (query != null && query.DataUseStatus.HasValue)
                select.Where(a => a.DataUseStatus == query.DataUseStatus.Value);

            var totalCount = select.ToList().Count();

            var items = await select.OrderByDescending(x => x.Sort).Page(query.PageIndex, query.PageSize)
                .ToListAsync(x => new BrandTypeBackViewModel
                {
                    Id = x.Id,
                    BrandName = x.BrandName,
                    DataUseStatus = x.DataUseStatus,
                    Sort = x.Sort
                });

            result.Data = items;
            result.Code = CommonConstants.SuccessCode;
            result.PageSize = query.PageSize;
            result.PageIndex = query.PageIndex;
            result.RecordCount = totalCount;
            return result;
        }
        #endregion

        #endregion

        #region 配件分类 CarAccessoryType

        #region 新增/编辑配件分类
        /// <summary>
        /// 新增/编辑配件分类
        /// </summary>
        /// <param name="model"></param>
        /// <param name="user"></param>
        /// <returns></returns>
        public async Task<ResponseContext<bool>> AddOrEditAccessoryType(AccessoryTypeRequestModel model, UserTicket user)
        {
            if (model == null || string.IsNullOrEmpty(model.AccessoryName))
                return new ResponseContext<bool> { Code = CommonConstants.ErrorCode, Msg = "参数不能为空!" };

            CarAccessoryType entity = new CarAccessoryType
            {
                Id = model.Id > 0 ? model.Id : IdWorker.NextId(),
                AccessoryName = model.AccessoryName,
                Sort = model.Sort,

                UpdateBy = user.Id,
                UpdateTime = DateTime.Now
            };

            bool state = false;

            if (model.Id == 0)
            {
                entity.DataUseStatus = DataUseStatus.Enabled;
                entity.CreateBy = user.Id;
                entity.CreateTime = DateTime.Now;

                CarAccessoryType addModel = await DbContext.FreeSql.GetRepository<CarAccessoryType>().InsertAsync(entity);
                state = addModel != null;
            }
            else
            {
                CarAccessoryType dbData = await DbContext.FreeSql.GetRepository<CarAccessoryType>().Where(x => x.Id != model.Id &&
                    x.AccessoryName == model.AccessoryName && x.IsDelete == CommonConstants.IsNotDelete).FirstAsync();
                if (dbData != null)
                    return new ResponseContext<bool> { Code = CommonConstants.ErrorCode, Msg = "配件名称已存在!" };

                state = await DbContext.FreeSql.GetRepository<CarAccessoryType>().UpdateDiy.SetSource(entity).UpdateColumns(x => new
                {
                    x.AccessoryName,
                    x.Sort,
                    x.UpdateBy,
                    x.UpdateTime
                }).ExecuteAffrowsAsync() > 0;
            }

            return new ResponseContext<bool> { Code = state ? CommonConstants.SuccessCode : CommonConstants.ErrorCode, Data = state, Msg = state ? "成功!" : "操作失败!" };
        }
        #endregion

        #region 启用或禁用配件分类
        /// <summary>
        /// 启用或禁用配件分类
        /// </summary>
        /// <param name="model"></param>
        /// <param name="user"></param>
        /// <returns></returns>
        public async Task<ResponseContext<bool>> EnabledOrDisabledAccessoryType(EnabledOrDisabledDataRequestModel model, UserTicket user)
        {
            if (model == null)
                return new ResponseContext<bool> { Code = CommonConstants.ErrorCode, Msg = "参数不能为空!" };

            CarAccessoryType entity = await DbContext.FreeSql.GetRepository<CarAccessoryType>().Where(x => x.Id == model.Id).FirstAsync();
            if (entity == null)
                return new ResponseContext<bool> { Code = CommonConstants.ErrorCode, Msg = "数据不存在!" };

            if (entity.DataUseStatus == model.DataUseStatus)
            {
                if (entity.DataUseStatus == DataUseStatus.Enabled)
                    return new ResponseContext<bool> { Code = CommonConstants.ErrorCode, Msg = "已是启用状态!" };
                if (entity.DataUseStatus == DataUseStatus.Disabled)
                    return new ResponseContext<bool> { Code = CommonConstants.ErrorCode, Msg = "已是禁用状态!" };
            }

            entity.DataUseStatus = model.DataUseStatus;
            entity.UpdateTime = DateTime.Now;
            entity.UpdateBy = user.Id;

            bool state = await DbContext.FreeSql.GetRepository<CarAccessoryType>().UpdateDiy.SetSource(entity).UpdateColumns(x => new
            {
                x.DataUseStatus,
                x.UpdateTime,
                x.UpdateBy
            }).ExecuteAffrowsAsync() > 0;

            return new ResponseContext<bool> { Code = state ? CommonConstants.SuccessCode : CommonConstants.ErrorCode, Data = state, Msg = state ? "成功!" : "操作失败!" };
        }
        #endregion

        #region 删除配件分类
        /// <summary>
        /// 删除配件分类
        /// </summary>
        /// <param name="model"></param>
        /// <param name="user"></param>
        /// <returns></returns>
        public async Task<ResponseContext<bool>> DeleteAccessoryType(DeleteDataRequestModel model, UserTicket user)
        {
            var list = await DbContext.FreeSql.GetRepository<CarAccessoryType>().Where(x => model.Ids.Contains(x.Id)).ToListAsync();

            if (list == null || list.Count == 0)
                return new ResponseContext<bool> { Code = CommonConstants.ErrorCode, Data = false, Msg = "没有该数据源!" };

            list.ForEach(x =>
            {
                x.IsDelete = CommonConstants.IsDelete;
                x.UpdateTime = DateTime.Now;
                x.UpdateBy = user.Id;
            });

            bool state = await DbContext.FreeSql.GetRepository<CarAccessoryType>().UpdateDiy.SetSource(list).UpdateColumns(x => new
            {
                x.IsDelete,
                x.UpdateTime,
                x.UpdateBy
            }).ExecuteAffrowsAsync() > 0;

            return new ResponseContext<bool> { Code = state ? CommonConstants.SuccessCode : CommonConstants.ErrorCode, Data = state, Msg = state ? "成功!" : "操作失败!" };
        }
        #endregion

        #region 获取配件分类列表(后台管理)
        /// <summary>
        /// 获取品牌类型列表(后台管理)
        /// </summary>
        /// <param name="query"></param>
        /// <returns></returns>
        public async Task<PageResponse<AccessoryTypeBackViewModel>> GetAccessoryTypeBackList(AccessoryTypeQuery query)
        {
            var result = new PageResponse<AccessoryTypeBackViewModel>();

            var select = DbContext.FreeSql.GetRepository<CarAccessoryType>()
                .Where(a => a.IsDelete == CommonConstants.IsNotDelete);

            if (query != null && !string.IsNullOrEmpty(query.AccessoryName))
                select.Where(a => a.AccessoryName.Contains(query.AccessoryName));
            if (query != null && query.DataUseStatus.HasValue)
                select.Where(a => a.DataUseStatus == query.DataUseStatus.Value);

            var totalCount = select.ToList().Count();

            var items = await select.OrderByDescending(x => x.Sort).Page(query.PageIndex, query.PageSize)
                .ToListAsync(x => new AccessoryTypeBackViewModel
                {
                    Id = x.Id,
                    AccessoryName = x.AccessoryName,
                    DataUseStatus = x.DataUseStatus,
                    Sort = x.Sort
                });

            result.Data = items;
            result.Code = CommonConstants.SuccessCode;
            result.PageSize = query.PageSize;
            result.PageIndex = query.PageIndex;
            result.RecordCount = totalCount;
            return result;
        }
        #endregion

        #endregion

        #region 产品类型 ProductType

        #region 新增/编辑产品类型
        /// <summary>
        /// 新增/编辑产品类型
        /// </summary>
        /// <param name="model"></param>
        /// <param name="user"></param>
        /// <returns></returns>
        public async Task<ResponseContext<bool>> AddOrEditProductType(ProductTypeRequestModel model, UserTicket user)
        {
            if (model == null || string.IsNullOrEmpty(model.ProductName) || string.IsNullOrEmpty(model.AccessoryIds))
                return new ResponseContext<bool> { Code = CommonConstants.ErrorCode, Msg = "参数不能为空!" };

            CarProductType entity = new CarProductType
            {
                Id = model.Id > 0 ? model.Id : IdWorker.NextId(),
                ProductName = model.ProductName,
                AccessoryIds = model.AccessoryIds,
                Sort = model.Sort,

                UpdateBy = user.Id,
                UpdateTime = DateTime.Now
            };

            bool state = false;

            if (model.Id == 0)
            {
                entity.DataUseStatus = DataUseStatus.Enabled;
                entity.CreateBy = user.Id;
                entity.CreateTime = DateTime.Now;

                CarProductType addModel = await DbContext.FreeSql.GetRepository<CarProductType>().InsertAsync(entity);
                state = addModel != null;
            }
            else
            {
                CarProductType dbData = await DbContext.FreeSql.GetRepository<CarProductType>().Where(x => x.Id != model.Id &&
                    x.ProductName == model.ProductName && x.IsDelete == CommonConstants.IsNotDelete).FirstAsync();
                if (dbData != null)
                    return new ResponseContext<bool> { Code = CommonConstants.ErrorCode, Msg = "产品名称已存在!" };

                state = await DbContext.FreeSql.GetRepository<CarProductType>().UpdateDiy.SetSource(entity).UpdateColumns(x => new
                {
                    x.ProductName,
                    x.AccessoryIds,
                    x.Sort,
                    x.UpdateBy,
                    x.UpdateTime
                }).ExecuteAffrowsAsync() > 0;
            }

            return new ResponseContext<bool> { Code = state ? CommonConstants.SuccessCode : CommonConstants.ErrorCode, Data = state, Msg = state ? "成功!" : "操作失败!" };
        }
        #endregion

        #region 启用或禁用产品类型
        /// <summary>
        /// 启用或禁用产品类型
        /// </summary>
        /// <param name="model"></param>
        /// <param name="user"></param>
        /// <returns></returns>
        public async Task<ResponseContext<bool>> EnabledOrDisabledProductType(EnabledOrDisabledDataRequestModel model, UserTicket user)
        {
            if (model == null)
                return new ResponseContext<bool> { Code = CommonConstants.ErrorCode, Msg = "参数不能为空!" };

            CarProductType entity = await DbContext.FreeSql.GetRepository<CarProductType>().Where(x => x.Id == model.Id).FirstAsync();
            if (entity == null)
                return new ResponseContext<bool> { Code = CommonConstants.ErrorCode, Msg = "数据不存在!" };

            if (entity.DataUseStatus == model.DataUseStatus)
            {
                if (entity.DataUseStatus == DataUseStatus.Enabled)
                    return new ResponseContext<bool> { Code = CommonConstants.ErrorCode, Msg = "已是启用状态!" };
                if (entity.DataUseStatus == DataUseStatus.Disabled)
                    return new ResponseContext<bool> { Code = CommonConstants.ErrorCode, Msg = "已是禁用状态!" };
            }

            entity.DataUseStatus = model.DataUseStatus;
            entity.UpdateTime = DateTime.Now;
            entity.UpdateBy = user.Id;

            bool state = await DbContext.FreeSql.GetRepository<CarProductType>().UpdateDiy.SetSource(entity).UpdateColumns(x => new
            {
                x.DataUseStatus,
                x.UpdateTime,
                x.UpdateBy
            }).ExecuteAffrowsAsync() > 0;

            return new ResponseContext<bool> { Code = state ? CommonConstants.SuccessCode : CommonConstants.ErrorCode, Data = state, Msg = state ? "成功!" : "操作失败!" };
        }
        #endregion

        #region 删除产品类型
        /// <summary>
        /// 删除产品类型
        /// </summary>
        /// <param name="model"></param>
        /// <param name="user"></param>
        /// <returns></returns>
        public async Task<ResponseContext<bool>> DeleteProductType(DeleteDataRequestModel model, UserTicket user)
        {
            var list = await DbContext.FreeSql.GetRepository<CarProductType>().Where(x => model.Ids.Contains(x.Id)).ToListAsync();

            if (list == null || list.Count == 0)
                return new ResponseContext<bool> { Code = CommonConstants.ErrorCode, Data = false, Msg = "没有该数据源!" };

            list.ForEach(x =>
            {
                x.IsDelete = CommonConstants.IsDelete;
                x.UpdateTime = DateTime.Now;
                x.UpdateBy = user.Id;
            });

            bool state = await DbContext.FreeSql.GetRepository<CarProductType>().UpdateDiy.SetSource(list).UpdateColumns(x => new
            {
                x.IsDelete,
                x.UpdateTime,
                x.UpdateBy
            }).ExecuteAffrowsAsync() > 0;

            return new ResponseContext<bool> { Code = state ? CommonConstants.SuccessCode : CommonConstants.ErrorCode, Data = state, Msg = state ? "成功!" : "操作失败!" };
        }
        #endregion

        #region 获取产品类型列表(后台管理)
        /// <summary>
        /// 获取产品类型列表(后台管理)
        /// </summary>
        /// <param name="query"></param>
        /// <returns></returns>
        public async Task<PageResponse<ProductTypeBackViewModel>> GetProductTypeBackList(ProductTypeQuery query)
        {
            var result = new PageResponse<ProductTypeBackViewModel>();

            var select = DbContext.FreeSql.GetRepository<CarProductType>()
                .Where(a => a.AccessoryIds.Contains(query.AccessoryId.ToString()) && a.IsDelete == CommonConstants.IsNotDelete);

            if (query != null && !string.IsNullOrEmpty(query.ProductName))
                select.Where(a => a.ProductName.Contains(query.ProductName));
            if (query != null && query.DataUseStatus.HasValue)
                select.Where(a => a.DataUseStatus == query.DataUseStatus.Value);

            var totalCount = select.ToList().Count();

            var items = await select.OrderByDescending(x => x.Sort).Page(query.PageIndex, query.PageSize)
                .ToListAsync(x => new ProductTypeBackViewModel
                {
                    Id = x.Id,
                    ProductName = x.ProductName,
                    DataUseStatus = x.DataUseStatus,
                    Sort = x.Sort
                });

            result.Data = items;
            result.Code = CommonConstants.SuccessCode;
            result.PageSize = query.PageSize;
            result.PageIndex = query.PageIndex;
            result.RecordCount = totalCount;
            return result;
        }
        #endregion

        #endregion

        #region 新旧程度 NewOldDegree

        #region 新增/编辑新旧程度
        /// <summary>
        /// 新增/编辑新旧程度
        /// </summary>
        /// <param name="model"></param>
        /// <param name="user"></param>
        /// <returns></returns>
        public async Task<ResponseContext<bool>> AddOrEditNewOldDegree(NewOldDegreeRequestModel model, UserTicket user)
        {
            if (model == null || string.IsNullOrEmpty(model.NewOldName) || string.IsNullOrEmpty(model.NewOldName))
                return new ResponseContext<bool> { Code = CommonConstants.ErrorCode, Msg = "参数不能为空!" };

            CarNewOldDegree entity = new CarNewOldDegree
            {
                Id = model.Id > 0 ? model.Id : IdWorker.NextId(),
                NewOldName = model.NewOldName,
                Sort = model.Sort,

                UpdateBy = user.Id,
                UpdateTime = DateTime.Now
            };

            bool state = false;

            if (model.Id == 0)
            {
                entity.DataUseStatus = DataUseStatus.Enabled;
                entity.CreateBy = user.Id;
                entity.CreateTime = DateTime.Now;

                CarNewOldDegree addModel = await DbContext.FreeSql.GetRepository<CarNewOldDegree>().InsertAsync(entity);
                state = addModel != null;
            }
            else
            {
                CarNewOldDegree dbData = await DbContext.FreeSql.GetRepository<CarNewOldDegree>().Where(x => x.Id != model.Id &&
                    x.NewOldName == model.NewOldName && x.IsDelete == CommonConstants.IsNotDelete).FirstAsync();
                if (dbData != null)
                    return new ResponseContext<bool> { Code = CommonConstants.ErrorCode, Msg = "新旧名称已存在!" };

                state = await DbContext.FreeSql.GetRepository<CarNewOldDegree>().UpdateDiy.SetSource(entity).UpdateColumns(x => new
                {
                    x.NewOldName,
                    x.Sort,
                    x.UpdateBy,
                    x.UpdateTime
                }).ExecuteAffrowsAsync() > 0;
            }

            return new ResponseContext<bool> { Code = state ? CommonConstants.SuccessCode : CommonConstants.ErrorCode, Data = state, Msg = state ? "成功!" : "操作失败!" };
        }
        #endregion

        #region 启用或禁用新旧程度
        /// <summary>
        /// 启用或禁用新旧程度
        /// </summary>
        /// <param name="model"></param>
        /// <param name="user"></param>
        /// <returns></returns>
        public async Task<ResponseContext<bool>> EnabledOrDisabledNewOldDegree(EnabledOrDisabledDataRequestModel model, UserTicket user)
        {
            if (model == null)
                return new ResponseContext<bool> { Code = CommonConstants.ErrorCode, Msg = "参数不能为空!" };

            CarNewOldDegree entity = await DbContext.FreeSql.GetRepository<CarNewOldDegree>().Where(x => x.Id == model.Id).FirstAsync();
            if (entity == null)
                return new ResponseContext<bool> { Code = CommonConstants.ErrorCode, Msg = "数据不存在!" };

            if (entity.DataUseStatus == model.DataUseStatus)
            {
                if (entity.DataUseStatus == DataUseStatus.Enabled)
                    return new ResponseContext<bool> { Code = CommonConstants.ErrorCode, Msg = "已是启用状态!" };
                if (entity.DataUseStatus == DataUseStatus.Disabled)
                    return new ResponseContext<bool> { Code = CommonConstants.ErrorCode, Msg = "已是禁用状态!" };
            }

            entity.DataUseStatus = model.DataUseStatus;
            entity.UpdateTime = DateTime.Now;
            entity.UpdateBy = user.Id;

            bool state = await DbContext.FreeSql.GetRepository<CarNewOldDegree>().UpdateDiy.SetSource(entity).UpdateColumns(x => new
            {
                x.DataUseStatus,
                x.UpdateTime,
                x.UpdateBy
            }).ExecuteAffrowsAsync() > 0;

            return new ResponseContext<bool> { Code = state ? CommonConstants.SuccessCode : CommonConstants.ErrorCode, Data = state, Msg = state ? "成功!" : "操作失败!" };
        }
        #endregion

        #region 删除新旧程度
        /// <summary>
        /// 删除新旧程度
        /// </summary>
        /// <param name="model"></param>
        /// <param name="user"></param>
        /// <returns></returns>
        public async Task<ResponseContext<bool>> DeleteNewOldDegree(DeleteDataRequestModel model, UserTicket user)
        {
            var list = await DbContext.FreeSql.GetRepository<CarNewOldDegree>().Where(x => model.Ids.Contains(x.Id)).ToListAsync();

            if (list == null || list.Count == 0)
                return new ResponseContext<bool> { Code = CommonConstants.ErrorCode, Data = false, Msg = "没有该数据源!" };

            list.ForEach(x =>
            {
                x.IsDelete = CommonConstants.IsDelete;
                x.UpdateTime = DateTime.Now;
                x.UpdateBy = user.Id;
            });

            bool state = await DbContext.FreeSql.GetRepository<CarNewOldDegree>().UpdateDiy.SetSource(list).UpdateColumns(x => new
            {
                x.IsDelete,
                x.UpdateTime,
                x.UpdateBy
            }).ExecuteAffrowsAsync() > 0;

            return new ResponseContext<bool> { Code = state ? CommonConstants.SuccessCode : CommonConstants.ErrorCode, Data = state, Msg = state ? "成功!" : "操作失败!" };
        }
        #endregion

        #region 获取新旧程度列表(后台管理)
        /// <summary>
        /// 获取新旧程度列表(后台管理)
        /// </summary>
        /// <param name="query"></param>
        /// <returns></returns>
        public async Task<PageResponse<NewOldDegreeBackViewModel>> GetNewOldDegreeBackList(NewOldDegreeQuery query)
        {
            var result = new PageResponse<NewOldDegreeBackViewModel>();

            var select = DbContext.FreeSql.GetRepository<CarNewOldDegree>()
                .Where(a => a.IsDelete == CommonConstants.IsNotDelete);

            if (query != null && !string.IsNullOrEmpty(query.NewOldName))
                select.Where(a => a.NewOldName.Contains(query.NewOldName));
            if (query != null && query.DataUseStatus.HasValue)
                select.Where(a => a.DataUseStatus == query.DataUseStatus.Value);

            var totalCount = select.ToList().Count();

            var items = await select.OrderByDescending(x => x.Sort).Page(query.PageIndex, query.PageSize)
                .ToListAsync(x => new NewOldDegreeBackViewModel
                {
                    Id = x.Id,
                    NewOldName = x.NewOldName,
                    DataUseStatus = x.DataUseStatus,
                    Sort = x.Sort
                });

            result.Data = items;
            result.Code = CommonConstants.SuccessCode;
            result.PageSize = query.PageSize;
            result.PageIndex = query.PageIndex;
            result.RecordCount = totalCount;
            return result;
        }
        #endregion

        #endregion

        #region 磨损程度 AbrasionDegree

        #region 新增/编辑磨损程度
        /// <summary>
        /// 新增/编辑磨损程度
        /// </summary>
        /// <param name="model"></param>
        /// <param name="user"></param>
        /// <returns></returns>
        public async Task<ResponseContext<bool>> AddOrEditAbrasionDegree(AbrasionDegreeRequestModel model, UserTicket user)
        {
            if (model == null || string.IsNullOrEmpty(model.AbrasionName) || string.IsNullOrEmpty(model.NewOldIds))
                return new ResponseContext<bool> { Code = CommonConstants.ErrorCode, Msg = "参数不能为空!" };

            CarAbrasionDegree entity = new CarAbrasionDegree
            {
                Id = model.Id > 0 ? model.Id : IdWorker.NextId(),
                AbrasionName = model.AbrasionName,
                NewOldIds = model.NewOldIds,
                Sort = model.Sort,

                UpdateBy = user.Id,
                UpdateTime = DateTime.Now
            };

            bool state = false;

            if (model.Id == 0)
            {
                entity.DataUseStatus = DataUseStatus.Enabled;
                entity.CreateBy = user.Id;
                entity.CreateTime = DateTime.Now;

                CarAbrasionDegree addModel = await DbContext.FreeSql.GetRepository<CarAbrasionDegree>().InsertAsync(entity);
                state = addModel != null;
            }
            else
            {
                CarAbrasionDegree dbData = await DbContext.FreeSql.GetRepository<CarAbrasionDegree>().Where(x => x.Id != model.Id &&
                    x.AbrasionName == model.AbrasionName && x.IsDelete == CommonConstants.IsNotDelete).FirstAsync();
                if (dbData != null)
                    return new ResponseContext<bool> { Code = CommonConstants.ErrorCode, Msg = "磨损名称已存在!" };

                state = await DbContext.FreeSql.GetRepository<CarAbrasionDegree>().UpdateDiy.SetSource(entity).UpdateColumns(x => new
                {
                    x.AbrasionName,
                    x.NewOldIds,
                    x.Sort,
                    x.UpdateBy,
                    x.UpdateTime
                }).ExecuteAffrowsAsync() > 0;
            }

            return new ResponseContext<bool> { Code = state ? CommonConstants.SuccessCode : CommonConstants.ErrorCode, Data = state, Msg = state ? "成功!" : "操作失败!" };
        }
        #endregion

        #region 启用或禁用磨损程度
        /// <summary>
        /// 启用或禁用磨损程度
        /// </summary>
        /// <param name="model"></param>
        /// <param name="user"></param>
        /// <returns></returns>
        public async Task<ResponseContext<bool>> EnabledOrDisabledAbrasionDegree(EnabledOrDisabledDataRequestModel model, UserTicket user)
        {
            if (model == null)
                return new ResponseContext<bool> { Code = CommonConstants.ErrorCode, Msg = "参数不能为空!" };

            CarAbrasionDegree entity = await DbContext.FreeSql.GetRepository<CarAbrasionDegree>().Where(x => x.Id == model.Id).FirstAsync();
            if (entity == null)
                return new ResponseContext<bool> { Code = CommonConstants.ErrorCode, Msg = "数据不存在!" };

            if (entity.DataUseStatus == model.DataUseStatus)
            {
                if (entity.DataUseStatus == DataUseStatus.Enabled)
                    return new ResponseContext<bool> { Code = CommonConstants.ErrorCode, Msg = "已是启用状态!" };
                if (entity.DataUseStatus == DataUseStatus.Disabled)
                    return new ResponseContext<bool> { Code = CommonConstants.ErrorCode, Msg = "已是禁用状态!" };
            }

            entity.DataUseStatus = model.DataUseStatus;
            entity.UpdateTime = DateTime.Now;
            entity.UpdateBy = user.Id;

            bool state = await DbContext.FreeSql.GetRepository<CarAbrasionDegree>().UpdateDiy.SetSource(entity).UpdateColumns(x => new
            {
                x.DataUseStatus,
                x.UpdateTime,
                x.UpdateBy
            }).ExecuteAffrowsAsync() > 0;

            return new ResponseContext<bool> { Code = state ? CommonConstants.SuccessCode : CommonConstants.ErrorCode, Data = state, Msg = state ? "成功!" : "操作失败!" };
        }
        #endregion

        #region 删除磨损程度
        /// <summary>
        /// 删除磨损程度
        /// </summary>
        /// <param name="model"></param>
        /// <param name="user"></param>
        /// <returns></returns>
        public async Task<ResponseContext<bool>> DeleteAbrasionDegree(DeleteDataRequestModel model, UserTicket user)
        {
            var list = await DbContext.FreeSql.GetRepository<CarAbrasionDegree>().Where(x => model.Ids.Contains(x.Id)).ToListAsync();

            if (list == null || list.Count == 0)
                return new ResponseContext<bool> { Code = CommonConstants.ErrorCode, Data = false, Msg = "没有该数据源!" };

            list.ForEach(x =>
            {
                x.IsDelete = CommonConstants.IsDelete;
                x.UpdateTime = DateTime.Now;
                x.UpdateBy = user.Id;
            });

            bool state = await DbContext.FreeSql.GetRepository<CarAbrasionDegree>().UpdateDiy.SetSource(list).UpdateColumns(x => new
            {
                x.IsDelete,
                x.UpdateTime,
                x.UpdateBy
            }).ExecuteAffrowsAsync() > 0;

            return new ResponseContext<bool> { Code = state ? CommonConstants.SuccessCode : CommonConstants.ErrorCode, Data = state, Msg = state ? "成功!" : "操作失败!" };
        }
        #endregion

        #region 获取磨损程度列表(后台管理)
        /// <summary>
        /// 获取磨损程度列表(后台管理)
        /// </summary>
        /// <param name="query"></param>
        /// <returns></returns>
        public async Task<PageResponse<AbrasionDegreeBackViewModel>> GetAbrasionDegreeBackList(AbrasionDegreeQuery query)
        {
            var result = new PageResponse<AbrasionDegreeBackViewModel>();

            var select = DbContext.FreeSql.GetRepository<CarAbrasionDegree>()
                .Where(a => a.NewOldIds.Contains(query.NewOldId.ToString()) && a.IsDelete == CommonConstants.IsNotDelete);

            if (query != null && !string.IsNullOrEmpty(query.AbrasionName))
                select.Where(a => a.AbrasionName.Contains(query.AbrasionName));
            if (query != null && query.DataUseStatus.HasValue)
                select.Where(a => a.DataUseStatus == query.DataUseStatus.Value);

            var totalCount = select.ToList().Count();

            var items = await select.OrderByDescending(x => x.Sort).Page(query.PageIndex, query.PageSize)
                .ToListAsync(x => new AbrasionDegreeBackViewModel
                {
                    Id = x.Id,
                    AbrasionName = x.AbrasionName,
                    DataUseStatus = x.DataUseStatus,
                    Sort = x.Sort
                });

            result.Data = items;
            result.Code = CommonConstants.SuccessCode;
            result.PageSize = query.PageSize;
            result.PageIndex = query.PageIndex;
            result.RecordCount = totalCount;
            return result;
        }
        #endregion

        #endregion

    }
}
