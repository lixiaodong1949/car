﻿using Car.Framework;
using Car.Framework.Auth;
using Car.Framework.Authorizations;
using Car.Framework.Entity;
using Car.Rcb.Dto;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Car.Rcb.IServices
{
    /// <summary>
    /// 标签类型相关服务接口
    /// </summary>
    public interface ITagTypeService
    {
        /// <summary>
        /// 获取技术类标签列表
        /// </summary>
        /// <returns></returns>
        Task<ResponseContext<List<TechnologyTagViewModel>>> GetTechnologyTagList();

        /// <summary>
        /// 获取品牌类型列表
        /// </summary>
        /// <returns></returns>
        Task<ResponseContext<List<BrandTypeViewModel>>> GetBrandTypeList();

        /// <summary>
        /// 获取配件分类列表
        /// </summary>
        /// <returns></returns>
        Task<ResponseContext<List<AccessoryTypeViewModel>>> GetAccessoryTypeList();

        /// <summary>
        /// 获取产品类型列表(根据配件分类)
        /// </summary>
        /// <param name="accessoryId">配件分类Id</param>
        /// <returns></returns>
        Task<ResponseContext<List<ProductTypeViewModel>>> GetProductTypeList(long accessoryId);

        /// <summary>
        /// 获取新旧程度列表
        /// </summary>
        /// <returns></returns>
        Task<ResponseContext<List<NewOldDegreeViewModel>>> GetNewOldDegreeList();

        /// <summary>
        /// 获取磨损程度列表(根据新旧程度)
        /// </summary>
        /// <param name="newOldId"></param>
        /// <returns></returns>
        Task<ResponseContext<List<AbrasionDegreeViewModel>>> GetAbrasionDegreeList(long newOldId);

    }
}
