﻿using Car.Framework;
using Car.Framework.Auth;
using Car.Framework.Authorizations;
using Car.Framework.Entity;
using Car.Rcb.Dto;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Car.Rcb.IServices
{
    /// <summary>
    /// 标签类型管理相关服务接口
    /// </summary>
    public interface ITagTypeBackService
    {
        #region 技术标签

        /// <summary>
        /// 新增/编辑技术标签
        /// </summary>
        /// <param name="model"></param>
        /// <param name="user"></param>
        /// <returns></returns>
        Task<ResponseContext<bool>> AddOrEditTechnologyTag(TechnologyTagRequestModel model, UserTicket user);

        /// <summary>
        /// 启用或禁用技术标签
        /// </summary>
        /// <param name="model"></param>
        /// <param name="user"></param>
        /// <returns></returns>
        Task<ResponseContext<bool>> EnabledOrDisabledTechnologyTag(EnabledOrDisabledDataRequestModel model, UserTicket user);

        /// <summary>
        /// 删除技术标签
        /// </summary>
        /// <param name="model"></param>
        /// <param name="user"></param>
        /// <returns></returns>
        Task<ResponseContext<bool>> DeleteTechnologyTag(DeleteDataRequestModel model, UserTicket user);

        /// <summary>
        /// 获取技术类标签列表(后台管理)
        /// </summary>
        /// <param name="query"></param>
        /// <returns></returns>
        Task<PageResponse<TechnologyTagBackViewModel>> GetTechnologyTagBackList(TechnologyTagQuery query);

        #endregion

        #region 品牌类型

        /// <summary>
        /// 新增/编辑品牌类型
        /// </summary>
        /// <param name="model"></param>
        /// <param name="user"></param>
        /// <returns></returns>
        Task<ResponseContext<bool>> AddOrEditBrandType(BrandTypeRequestModel model, UserTicket user);

        /// <summary>
        /// 启用或禁用品牌类型
        /// </summary>
        /// <param name="model"></param>
        /// <param name="user"></param>
        /// <returns></returns>
        Task<ResponseContext<bool>> EnabledOrDisabledBrandType(EnabledOrDisabledDataRequestModel model, UserTicket user);

        /// <summary>
        /// 删除品牌类型
        /// </summary>
        /// <param name="model"></param>
        /// <param name="user"></param>
        /// <returns></returns>
        Task<ResponseContext<bool>> DeleteBrandType(DeleteDataRequestModel model, UserTicket user);

        /// <summary>
        /// 获取品牌类型列表(后台管理)
        /// </summary>
        /// <param name="query"></param>
        /// <returns></returns>
        Task<PageResponse<BrandTypeBackViewModel>> GetBrandTypeBackList(BrandTypeQuery query);

        #endregion

        #region 配件分类

        /// <summary>
        /// 新增/编辑配件分类
        /// </summary>
        /// <param name="model"></param>
        /// <param name="user"></param>
        /// <returns></returns>
        Task<ResponseContext<bool>> AddOrEditAccessoryType(AccessoryTypeRequestModel model, UserTicket user);

        /// <summary>
        /// 启用或禁用配件分类
        /// </summary>
        /// <param name="model"></param>
        /// <param name="user"></param>
        /// <returns></returns>
        Task<ResponseContext<bool>> EnabledOrDisabledAccessoryType(EnabledOrDisabledDataRequestModel model, UserTicket user);

        /// <summary>
        /// 删除配件分类
        /// </summary>
        /// <param name="model"></param>
        /// <param name="user"></param>
        /// <returns></returns>
        Task<ResponseContext<bool>> DeleteAccessoryType(DeleteDataRequestModel model, UserTicket user);

        /// <summary>
        /// 获取品牌类型列表(后台管理)
        /// </summary>
        /// <param name="query"></param>
        /// <returns></returns>
        Task<PageResponse<AccessoryTypeBackViewModel>> GetAccessoryTypeBackList(AccessoryTypeQuery query);

        #endregion

        #region 产品类型

        /// <summary>
        /// 新增/编辑产品类型
        /// </summary>
        /// <param name="model"></param>
        /// <param name="user"></param>
        /// <returns></returns>
        Task<ResponseContext<bool>> AddOrEditProductType(ProductTypeRequestModel model, UserTicket user);

        /// <summary>
        /// 启用或禁用产品类型
        /// </summary>
        /// <param name="model"></param>
        /// <param name="user"></param>
        /// <returns></returns>
        Task<ResponseContext<bool>> EnabledOrDisabledProductType(EnabledOrDisabledDataRequestModel model, UserTicket user);

        /// <summary>
        /// 删除产品类型
        /// </summary>
        /// <param name="model"></param>
        /// <param name="user"></param>
        /// <returns></returns>
        Task<ResponseContext<bool>> DeleteProductType(DeleteDataRequestModel model, UserTicket user);

        /// <summary>
        /// 获取产品类型列表(后台管理)
        /// </summary>
        /// <param name="query"></param>
        /// <returns></returns>
        Task<PageResponse<ProductTypeBackViewModel>> GetProductTypeBackList(ProductTypeQuery query);

        #endregion

        #region 新旧程度

        /// <summary>
        /// 新增/编辑新旧程度
        /// </summary>
        /// <param name="model"></param>
        /// <param name="user"></param>
        /// <returns></returns>
        Task<ResponseContext<bool>> AddOrEditNewOldDegree(NewOldDegreeRequestModel model, UserTicket user);

        /// <summary>
        /// 启用或禁用新旧程度
        /// </summary>
        /// <param name="model"></param>
        /// <param name="user"></param>
        /// <returns></returns>
        Task<ResponseContext<bool>> EnabledOrDisabledNewOldDegree(EnabledOrDisabledDataRequestModel model, UserTicket user);

        /// <summary>
        /// 删除新旧程度
        /// </summary>
        /// <param name="model"></param>
        /// <param name="user"></param>
        /// <returns></returns>
        Task<ResponseContext<bool>> DeleteNewOldDegree(DeleteDataRequestModel model, UserTicket user);

        /// <summary>
        /// 获取新旧程度列表(后台管理)
        /// </summary>
        /// <param name="query"></param>
        /// <returns></returns>
        Task<PageResponse<NewOldDegreeBackViewModel>> GetNewOldDegreeBackList(NewOldDegreeQuery query);

        #endregion

        #region 磨损程度

        /// <summary>
        /// 新增/编辑磨损程度
        /// </summary>
        /// <param name="model"></param>
        /// <param name="user"></param>
        /// <returns></returns>
        Task<ResponseContext<bool>> AddOrEditAbrasionDegree(AbrasionDegreeRequestModel model, UserTicket user);

        /// <summary>
        /// 启用或禁用磨损程度
        /// </summary>
        /// <param name="model"></param>
        /// <param name="user"></param>
        /// <returns></returns>
        Task<ResponseContext<bool>> EnabledOrDisabledAbrasionDegree(EnabledOrDisabledDataRequestModel model, UserTicket user);

        /// <summary>
        /// 删除磨损程度
        /// </summary>
        /// <param name="model"></param>
        /// <param name="user"></param>
        /// <returns></returns>
        Task<ResponseContext<bool>> DeleteAbrasionDegree(DeleteDataRequestModel model, UserTicket user);

        /// <summary>
        /// 获取磨损程度列表(后台管理)
        /// </summary>
        /// <param name="query"></param>
        /// <returns></returns>
        Task<PageResponse<AbrasionDegreeBackViewModel>> GetAbrasionDegreeBackList(AbrasionDegreeQuery query);

        #endregion

    }
}
