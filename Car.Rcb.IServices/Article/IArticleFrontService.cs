﻿using Car.Framework;
using Car.Framework.Auth;
using Car.Framework.Authorizations;
using Car.Framework.Entity;
using Car.Rcb.Dto;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Car.Rcb.IServices
{
    /// <summary>
    /// 文章前台相关服务接口
    /// </summary>
    public interface IArticleFrontService
    {
        /// <summary>
        /// 发布技术文章
        /// </summary>
        /// <param name="model"></param>
        /// <param name="user"></param>
        /// <returns></returns>
        Task<ResponseContext<bool>> PublishTechnicalArticle(TechnicalArticleRequestModel model, UserTicket user);

        /// <summary>
        /// 获取平台技术文章列表
        /// </summary>
        /// <param name="query"></param>
        /// <param name="user"></param>
        /// <returns></returns>
        Task<PageResponse<TechnicalArticleViewModel>> GetPlatformTechnicalArticleList(TechnicalArticleQuery query, UserTicket user);

        /// <summary>
        /// 获取某一技术文章详情
        /// </summary>
        /// <param name="articleId"></param>
        /// <param name="user"></param>
        /// <returns></returns>
        Task<ResponseContext<TechnicalArticleDetailViewModel>> GetTechnicalArticleDetail(long articleId, UserTicket user);

        /// <summary>
        /// 删除自己发布的技术文章
        /// </summary>
        /// <param name="id"></param>
        /// <param name="user"></param>
        /// <returns></returns>
        Task<ResponseContext<bool>> DeleteOneselfTechnicalArticle(long id, UserTicket user);

        /// <summary>
        /// 用户操作技术文章活跃程度(查看、收藏、点赞)
        /// </summary>
        /// <param name="model"></param>
        /// <param name="user"></param>
        /// <returns></returns>
        Task<ResponseContext<bool>> UserOperationTechnicalArticleForActive(TechnicalArticleUserActiveRequestModel model, UserTicket user);

        /// <summary>
        /// 获取个人发布/收藏技术文章列表
        /// </summary>
        /// <param name="query"></param>
        /// <param name="user"></param>
        /// <returns></returns>
        Task<PageResponse<TechnicalArticleViewModel>> GetOneSelfTechnicalArticleList(TechnicalArticleOneselfQuery query, UserTicket user);

        /// <summary>
        /// 用户评论技术文章
        /// </summary>
        /// <param name="model"></param>
        /// <param name="user"></param>
        /// <returns></returns>
        Task<ResponseContext<bool>> CommentTechnicalArticle(TechnicalArticleCommentRequestModel model, UserTicket user);

        /// <summary>
        /// 获取文章评论列表
        /// </summary>
        /// <param name="query"></param>
        /// <param name="user"></param>
        /// <returns></returns>
        Task<PageResponse<TechnicalArticleCommentViewModel>> GetTechnicalArticleCommentList(TechnicalArticleCommentQuery query, UserTicket user);

        /// <summary>
        /// 删除自己发布的评论
        /// </summary>
        /// <param name="id"></param>
        /// <param name="user"></param>
        /// <returns></returns>
        Task<ResponseContext<bool>> DeleteOneselfComment(long id, UserTicket user);

        /// <summary>
        /// 发布二手或求购文章
        /// </summary>
        /// <param name="model"></param>
        /// <param name="user"></param>
        /// <returns></returns>
        Task<ResponseContext<bool>> PublishUsedOrAskBuyArticle(UsedOrAskBuyRequestModel model, UserTicket user);

        /// <summary>
        /// 获取平台二手或求购文章列表
        /// </summary>
        /// <param name="query"></param>
        /// <param name="user"></param>
        /// <returns></returns>
        Task<PageResponse<UsedOrAskBuyArticleViewModel>> GetPlatformUsedOrAskBuyArticleList(UsedOrAskBuyArticleQuery query, UserTicket user);

        /// <summary>
        /// 获取某一二手或求购文章详情
        /// </summary>
        /// <param name="articleId"></param>
        /// <param name="user"></param>
        /// <returns></returns>
        Task<ResponseContext<UsedOrAskBuyArticleDetailViewModel>> GetUsedOrAskBuyArticleDetail(long articleId, UserTicket user);

        /// <summary>
        /// 删除自己发布的二手或求购文章
        /// </summary>
        /// <param name="id"></param>
        /// <param name="user"></param>
        /// <returns></returns>
        Task<ResponseContext<bool>> DeleteOneselfUsedOrAskBuyArticle(long id, UserTicket user);

        /// <summary>
        /// 用户操作二手或求购文章活跃程度(查看、收藏)
        /// </summary>
        /// <param name="model"></param>
        /// <param name="user"></param>
        /// <returns></returns>
        Task<ResponseContext<bool>> UserOperationUsedOrAskBuyArticleForActive(UsedOrAskBuyArticleUserActiveRequestModel model, UserTicket user);

        /// <summary>
        /// 获取个人发布/收藏二手或求购文章列表
        /// </summary>
        /// <param name="query"></param>
        /// <param name="user"></param>
        /// <returns></returns>
        Task<PageResponse<UsedOrAskBuyArticleViewModel>> GetOneSelfUsedOrAskBuyArticleList(UsedOrAskBuyArticleOneselfQuery query, UserTicket user);

    }
}
