﻿using Car.Framework;
using Car.Framework.Auth;
using Car.Framework.Authorizations;
using Car.Framework.Entity;
using Car.Rcb.Dto;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Car.Rcb.IServices
{
    /// <summary>
    /// 文章后台相关服务接口
    /// </summary>
    public interface IArticleBackService
    {
        /// <summary>
        /// 获取技术文章后台列表
        /// </summary>
        /// <param name="query"></param>
        /// <returns></returns>
        Task<PageResponse<TechnicalArticleForBackViewModel>> GetTechnicalArticleListForBack(TechnicalArticleForBackQuery query);

        /// <summary>
        /// 审核技术文章
        /// </summary>
        /// <param name="model"></param>
        /// <param name="user"></param>
        /// <returns></returns>
        Task<ResponseContext<bool>> AuditTechnicalArticle(TechnicalArticleAuditRequestModel model, UserTicket user);

        /// <summary>
        /// 获取二手或求购文章后台列表
        /// </summary>
        /// <param name="query"></param>
        /// <returns></returns>
        Task<PageResponse<UsedOrAskBuyArticleForBackViewModel>> GetUsedOrAskBuyArticleListForBack(UsedOrAskBuyArticleForBackQuery query);

        /// <summary>
        /// 审核二手或求购文章
        /// </summary>
        /// <param name="model"></param>
        /// <param name="user"></param>
        /// <returns></returns>
        Task<ResponseContext<bool>> AuditUsedOrAskBuyArticle(UsedOrAskBuyArticleAuditRequestModel model, UserTicket user);

    }
}
