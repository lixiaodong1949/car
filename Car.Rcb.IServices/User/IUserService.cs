﻿using Car.Framework;
using Car.Framework.Auth;
using Car.Framework.Authorizations;
using Car.Rcb.Dto;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Car.Rcb.IServices
{
    /// <summary>
    /// 用户相关服务接口
    /// </summary>
    public interface IUserService
    {
        /// <summary>
        /// 用户注册
        /// </summary>
        /// <param name="model"></param>
        /// <param name="loginIp"></param>
        /// <param name="_requirement"></param>
        /// <returns></returns>
        Task<ResponseContext<UserTicket>> UseRegister(UserRegisterRequestModel model, string loginIp, PermissionRequirement _requirement);

        /// <summary>
        /// 用户手机号、密码登录
        /// </summary>
        /// <param name="model"></param>
        /// <param name="loginIp"></param>
        /// <param name="_requirement"></param>
        /// <returns></returns>
        Task<ResponseContext<UserTicket>> UserPasswordLogin(UserLoginRequestModel model, string loginIp, PermissionRequirement _requirement);

        /// <summary>
        /// 获取用户信息
        /// </summary>
        /// <param name="user"></param>
        /// <returns></returns>
        Task<ResponseContext<UserInfoViewModel>> GetUserInfo(UserTicket user);

        /// <summary>
        /// 更新用户信息
        /// </summary>
        /// <param name="model"></param>
        /// <param name="user"></param>
        /// <returns></returns>
        Task<ResponseContext<bool>> UpdateUserInfo(UserUpdateRequestModel model, UserTicket user);

    }
}
