using Car.Framework;
using Car.Framework.Auth;
using Car.Framework.AutoMapper;
using Car.Framework.Dal;
using Car.Framework.Logger;
using Car.Framework.Web.Filter;
using Car.Rcb.Api.Auth;
using Car.Rcb.IServices;
using Car.Rcb.ServiceImpl;
using Car.Repository.Extensions;
using Car.Swagger;
using MediatR;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using Microsoft.IdentityModel.Tokens;
using NLog.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Tas.Common.Configurations;

namespace Car.Rcb.Api
{
    public class Startup
    {
        private CustsomSwaggerOptions CURRENT_SWAGGER_OPTIONS = new CustsomSwaggerOptions()
        {
            ProjectName = "人车邦",
            //ProjectName = "XXX",
            ApiVersions = new string[] { "v1" },//要显示的版本
            UseCustomIndex = false,
            RoutePrefix = "swagger",

            AddSwaggerGenAction = c =>
            {
                var xmlPaths = Directory.GetFiles(System.AppContext.BaseDirectory, "Car.*.xml").ToList();
                foreach (var path in xmlPaths)
                {
                    c.IncludeXmlComments(path, true);
                }
            },
            UseSwaggerAction = c =>
            {

            },
            UseSwaggerUIAction = c =>
            {
            }
        };

        /// <summary>
        /// 
        /// </summary>
        public IConfiguration Configuration { get; }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="configuration"></param>
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="services"></param>
        private void AddTokenValidation(IServiceCollection services)
        {
            var securityKey = new SymmetricSecurityKey(Encoding.ASCII.GetBytes(CommonConstants.IdentServerSecret));
            var signingCredentials = new SigningCredentials(securityKey, SecurityAlgorithms.HmacSha256);

            var permissionRequirement = new PermissionRequirement(signingCredentials);
            services.AddSingleton(permissionRequirement);
        }

        // This method gets called by the runtime. Use this method to add services to the container.
        // For more information on how to configure your application, visit https://go.microsoft.com/fwlink/?LinkID=398940
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddScoped<CustomExceptionFilter>();
            services.AddMvc(options =>
            {
                options.Filters.Add<ActionModelStateFilter>();
                options.Filters.Add<AuthorizationFilter>();
                options.EnableEndpointRouting = false;
                //options.Filters.Add<Filters.HttpGlobalExceptionFilter>();


            }).SetCompatibilityVersion(CompatibilityVersion.Version_3_0).AddNewtonsoftJson(options =>
            {
                //忽略循环引用
                //options.SerializerSettings.ReferenceLoopHandling = ReferenceLoopHandling.Ignore;
                //options.SerializerSettings.NullValueHandling = NullValueHandling.Ignore;
                //设置时间格式

                options.SerializerSettings.DateFormatString = "yyyy-MM-dd HH:mm:ss";
                options.SerializerSettings.ContractResolver = new Newtonsoft.Json.Serialization.DefaultContractResolver();//json字符串大小写原样输出


            }).ConfigureApiBehaviorOptions(options =>
            {
                options.SuppressModelStateInvalidFilter = true;
            });

            //版本控制
            // services.AddMvcCore().AddVersionedApiExplorer(o => o.GroupNameFormat = "'v'VVV");

            services.AddApiVersioning(option =>
            {
                // allow a client to call you without specifying an api version
                // since we haven't configured it otherwise, the assumed api version will be 1.0
                option.AssumeDefaultVersionWhenUnspecified = true;
                option.ReportApiVersions = false;
            })
            .AddCustomSwagger(CURRENT_SWAGGER_OPTIONS)
            .AddCors(options =>
            {
                options.AddPolicy(CommonConstants.AnyOrigin, builder =>
                {
                    //builder.AllowAnyOrigin() //允许任何来源的主机访问                             
                    //.AllowAnyMethod()
                    //.AllowAnyHeader()
                    //.AllowCredentials();//指定处理cookie
                    builder.WithOrigins("http://*.*.*.*")//.SetIsOriginAllowedToAllowWildcardSubdomains()//设置允许访问的域
                   .AllowAnyMethod()
                   .AllowAnyHeader()
                   .AllowCredentials();
                });
            });

            services.AddMediatR(typeof(Program).Assembly);

            services.AddHttpClient();

            //services.AddMQProducerServices(Configuration);//消息队列，生产者

            //services.AddAutoMapper();
            AddTokenValidation(services);
            InitService(services);
        }

        #region 依赖注入业务服务
        /// <summary>
        /// 依赖注入业务服务
        /// </summary>
        /// <param name="services"></param>
        private void InitService(IServiceCollection services)
        {
            services.AddTransient<IUserService, UserService>();
            services.AddTransient<IArticleFrontService, ArticleFrontService>();
            services.AddTransient<ITagTypeService, TagTypeService>();
            services.AddTransient<ITagTypeBackService, TagTypeBackService>();
        }
        #endregion

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, Microsoft.AspNetCore.Hosting.IHostingEnvironment env, ILoggerFactory loggerFactory,
            Microsoft.AspNetCore.Hosting.IApplicationLifetime lifetime)
        {
            app.UseExceptionHandler(builder =>
            {
                builder.Run(async context =>
                {
                    context.Response.StatusCode = StatusCodes.Status500InternalServerError;
                    context.Response.ContentType = "application/json";
                    var ex = context.Features.Get<Microsoft.AspNetCore.Diagnostics.IExceptionHandlerFeature>();
                    if (ex?.Error != null)
                    {
                        CommonLogger.Error("UseExceptionHandler", ex?.Error);
                    }
                    await context.Response.WriteAsync(ex?.Error?.Message ?? "an error occure");
                });
            }).UseHsts();

            #region Consul
            //app.UseConsul(Configuration);//consul  服务注册
            #endregion

            app.UseMvc().UseCustomSwagger(CURRENT_SWAGGER_OPTIONS);

            app.UseCors(CommonConstants.AnyOrigin);
            app.UseStaticFiles();


            ProjectConfiguration.GetInstance().
                RegisterConfiguration(Configuration).
                UseCommonLogger(loggerFactory.AddNLog()).
                UseFreeSqlRepository(Configuration["DbConnection"]).
                UseAutoMapper(Framework.Utils.RuntimeHelper.GetAllAssemblies());

            DataMergeHelper.Start();
        }
    }
}