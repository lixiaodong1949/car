﻿using Car.Framework;
using Car.Framework.Authorizations;
using IdentityModel;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Controllers;
using Microsoft.AspNetCore.Mvc.Filters;
using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;

namespace Car.Rcb.Api.Auth
{
    /// <summary>
    /// 
    /// </summary>
    public class AuthorizationFilter : IAuthorizationFilter
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="httpContext"></param> 
        /// <param name="claims"></param> 
        private static UserTicket SetUserTicket(HttpContext httpContext, IEnumerable<Claim> claims)
        {
            var dicClaim = claims.ToDictionary(m => m.Type, m => m.Value);
            dicClaim.TryGetValue(JwtClaimTypes.Id, out var id);
            dicClaim.TryGetValue(CommonConstants.ClaimsNickName, out var nickName);            
            dicClaim.TryGetValue(CommonConstants.ClaimsRealName, out var realName);
            var userTicket = new UserTicket
            {
                Id = long.Parse(id),
                NickName = nickName,                
                RealName = realName,
            };
            httpContext.Items[CommonConstants.ContextUserPropertyKey] = userTicket;
            return userTicket;
        }

        public void OnAuthorization(AuthorizationFilterContext context)
        {
            var anonymous = ((ControllerActionDescriptor)context.ActionDescriptor).MethodInfo.GetCustomAttributes(typeof(AllowAnonymousAttribute), true);
            if (anonymous.Any())
            {
                return;
            }

            if (!context.HttpContext.Request.Headers.TryGetValue("token", out var token) || string.IsNullOrEmpty(token))
            {

                if (!context.HttpContext.Request.Headers.TryGetValue("Authorization", out token) || string.IsNullOrEmpty(token))
                {
                    context.Result = new ObjectResult(new { Code = CommonConstants.Unauthorized, Sub_msg = "请求缺少token", Msg = "请求缺少token" });

                    return;
                }
                token = token.ToString().Replace("Bearer ", "");
            }

            var jwt = new JwtSecurityTokenHandler().ReadJwtToken(token);
            var ticket = SetUserTicket(context.HttpContext, jwt.Claims);
            var userId = ticket.Id.ToString();

            //var cacheToken = RedisHelper.HGet(CacheKeys.UserLoginTokenHashKey, $"{userId}-{ticket.LoginType}");
            //if (string.IsNullOrEmpty(cacheToken))  //测试环境先注释
            //{
            //    //var log = UserService.GetLastLoginLog(ticket.Id);
            //    //if (log.Status == Status.Disable || !Equals(log.AccessToken, token))
            //    //{
            //    //    context.Result = new ObjectResult(new { code = CommonConstants.Unauthorized, sub_msg = "无效的token请求", msg = "无效的token请求" });
            //    //    return;
            //    //}
            //    RedisHelper.HSet(CacheKeys.UserLoginTokenHashKey, $"{userId}-{ticket.LoginType}", token);
            //}
            //else if (!Equals(token, cacheToken))
            //{
            //    RedisHelper.HSet(CacheKeys.UserLoginTokenHashKey, "loginoutinfo", $"filed:{userId}-{ticket.LoginType} token:{token} cachetoken：{cacheToken}");
            //    context.Result = new ObjectResult(new
            //    {
            //        Code = CommonConstants.Unauthorized,
            //        Sub_msg = $"此账号在另一处登录，您已被迫下线!",
            //        Msg = "此账号在另一处登录，您已被迫下线!"
            //    });
            //    return;
            //}

        }
    }
}
