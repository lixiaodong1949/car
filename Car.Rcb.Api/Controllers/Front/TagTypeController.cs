﻿using Car.Framework;
using Car.Framework.Auth;
using Car.Framework.Authorizations;
using Car.Rcb.Api;
using Car.Rcb.Api.Auth;
using Car.Rcb.Dto;
using Car.Rcb.IServices;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Car.Framework.AliyunOss;

namespace Yssx.S.Api.Controllers
{
    /// <summary>
    /// 标签类型相关Api
    /// </summary>
    public class TagTypeController : BaseController
    {
        ITagTypeService _service;

        /// <summary>
        /// 构造函数
        /// </summary>
        /// <param name="service"></param>
        public TagTypeController(ITagTypeService service)
        {
            _service = service;
        }

        #region 获取技术类标签列表
        /// <summary>
        /// 获取技术类标签列表
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [AllowAnonymous]
        public async Task<ResponseContext<List<TechnologyTagViewModel>>> GetTechnologyTagList()
        {
            return await _service.GetTechnologyTagList();
        }
        #endregion

        #region 获取品牌类型列表
        /// <summary>
        /// 获取品牌类型列表
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [AllowAnonymous]
        public async Task<ResponseContext<List<BrandTypeViewModel>>> GetBrandTypeList()
        {
            return await _service.GetBrandTypeList();
        }
        #endregion

        #region 获取配件分类列表
        /// <summary>
        /// 获取配件分类列表
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [AllowAnonymous]
        public async Task<ResponseContext<List<AccessoryTypeViewModel>>> GetAccessoryTypeList()
        {
            return await _service.GetAccessoryTypeList();
        }
        #endregion

        #region 获取产品类型列表(根据配件分类)
        /// <summary>
        /// 获取产品类型列表(根据配件分类)
        /// </summary>
        /// <param name="accessoryId">配件分类Id</param>
        /// <returns></returns>
        [HttpGet]
        [AllowAnonymous]
        public async Task<ResponseContext<List<ProductTypeViewModel>>> GetProductTypeList(long accessoryId)
        {
            return await _service.GetProductTypeList(accessoryId);
        }
        #endregion

        #region 获取新旧程度列表
        /// <summary>
        /// 获取新旧程度列表
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [AllowAnonymous]
        public async Task<ResponseContext<List<NewOldDegreeViewModel>>> GetNewOldDegreeList()
        {
            return await _service.GetNewOldDegreeList();
        }
        #endregion

        #region 获取磨损程度列表(根据新旧程度)
        /// <summary>
        /// 获取磨损程度列表(根据新旧程度)
        /// </summary>
        /// <param name="newOldId">新旧程度Id</param>
        /// <returns></returns>
        [HttpGet]
        [AllowAnonymous]
        public async Task<ResponseContext<List<AbrasionDegreeViewModel>>> GetAbrasionDegreeList(long newOldId)
        {
            return await _service.GetAbrasionDegreeList(newOldId);
        }
        #endregion

    }
}