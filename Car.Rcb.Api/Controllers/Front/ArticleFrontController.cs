﻿using Car.Framework;
using Car.Framework.Auth;
using Car.Framework.Authorizations;
using Car.Framework.Entity;
using Car.Rcb.Api;
using Car.Rcb.Api.Auth;
using Car.Rcb.Dto;
using Car.Rcb.IServices;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Yssx.S.Api.Controllers
{
    /// <summary>
    /// 文章前台相关Api
    /// </summary>
    public class ArticleFrontController : BaseController
    {
        IArticleFrontService _service;

        /// <summary>
        /// 构造函数
        /// </summary>
        /// <param name="service"></param>
        public ArticleFrontController(IArticleFrontService service)
        {
            _service = service;
        }

        #region 发布技术文章
        /// <summary>
        /// 发布技术文章
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost]
        public async Task<ResponseContext<bool>> PublishTechnicalArticle(TechnicalArticleRequestModel model)
        {
            return await _service.PublishTechnicalArticle(model, this.CurrentUser);
        }
        #endregion

        #region 获取平台技术文章列表
        /// <summary>
        /// 获取平台技术文章列表
        /// </summary>
        /// <param name="query"></param>
        /// <returns></returns>
        [HttpGet]
        public async Task<PageResponse<TechnicalArticleViewModel>> GetPlatformTechnicalArticleList([FromQuery] TechnicalArticleQuery query)
        {
            return await _service.GetPlatformTechnicalArticleList(query, this.CurrentUser);
        }
        #endregion

        #region 获取某一技术文章详情
        /// <summary>
        /// 获取某一技术文章详情
        /// </summary>
        /// <param name="articleId"></param>
        /// <returns></returns>
        [HttpGet]
        public async Task<ResponseContext<TechnicalArticleDetailViewModel>> GetTechnicalArticleDetail(long articleId)
        {
            return await _service.GetTechnicalArticleDetail(articleId, this.CurrentUser);
        }
        #endregion

        #region 删除自己发布的技术文章
        /// <summary>
        /// 删除自己发布的技术文章
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpGet]
        public async Task<ResponseContext<bool>> DeleteOneselfTechnicalArticle(long id)
        {
            return await _service.DeleteOneselfTechnicalArticle(id, this.CurrentUser);
        }
        #endregion

        #region 用户操作技术文章活跃程度(查看、收藏、点赞)
        /// <summary>
        /// 用户操作技术文章活跃程度(查看、收藏、点赞)
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost]
        public async Task<ResponseContext<bool>> UserOperationTechnicalArticleForActive(TechnicalArticleUserActiveRequestModel model)
        {
            return await _service.UserOperationTechnicalArticleForActive(model, this.CurrentUser);
        }
        #endregion

        #region 获取个人发布/收藏技术文章列表
        /// <summary>
        /// 获取个人发布/收藏技术文章列表
        /// </summary>
        /// <param name="query"></param>
        /// <returns></returns>
        [HttpGet]
        public async Task<PageResponse<TechnicalArticleViewModel>> GetOneSelfTechnicalArticleList([FromQuery] TechnicalArticleOneselfQuery query)
        {
            return await _service.GetOneSelfTechnicalArticleList(query, this.CurrentUser);
        }
        #endregion

        #region 用户评论技术文章
        /// <summary>
        /// 用户评论技术文章
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost]
        public async Task<ResponseContext<bool>> CommentTechnicalArticle(TechnicalArticleCommentRequestModel model)
        {
            return await _service.CommentTechnicalArticle(model, this.CurrentUser);
        }
        #endregion

        #region 获取文章评论列表
        /// <summary>
        /// 获取文章评论列表
        /// </summary>
        /// <param name="query"></param>
        /// <returns></returns>
        [HttpGet]
        public async Task<PageResponse<TechnicalArticleCommentViewModel>> GetTechnicalArticleCommentList([FromQuery] TechnicalArticleCommentQuery query)
        {
            return await _service.GetTechnicalArticleCommentList(query, this.CurrentUser);
        }
        #endregion

        #region 删除自己发布的评论
        /// <summary>
        /// 删除自己发布的评论
        /// </summary>
        /// <param name="id"></param>
        /// <param name="user"></param>
        /// <returns></returns>
        [HttpGet]
        public async Task<ResponseContext<bool>> DeleteOneselfComment(long id, UserTicket user)
        {
            return await _service.DeleteOneselfComment(id, this.CurrentUser);
        }
        #endregion

        #region 发布二手或求购文章
        /// <summary>
        /// 发布二手或求购文章
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost]
        public async Task<ResponseContext<bool>> PublishUsedOrAskBuyArticle(UsedOrAskBuyRequestModel model)
        {
            return await _service.PublishUsedOrAskBuyArticle(model, this.CurrentUser);
        }
        #endregion

        #region 获取平台二手或求购文章列表
        /// <summary>
        /// 获取平台二手或求购文章列表
        /// </summary>
        /// <param name="query"></param>
        /// <returns></returns>
        [HttpGet]
        public async Task<PageResponse<UsedOrAskBuyArticleViewModel>> GetPlatformUsedOrAskBuyArticleList([FromQuery] UsedOrAskBuyArticleQuery query)
        {
            return await _service.GetPlatformUsedOrAskBuyArticleList(query, this.CurrentUser);
        }
        #endregion

        #region 获取某一二手或求购文章详情
        /// <summary>
        /// 获取某一二手或求购文章详情
        /// </summary>
        /// <param name="articleId"></param>
        /// <returns></returns>
        [HttpGet]
        public async Task<ResponseContext<UsedOrAskBuyArticleDetailViewModel>> GetUsedOrAskBuyArticleDetail(long articleId)
        {
            return await _service.GetUsedOrAskBuyArticleDetail(articleId, this.CurrentUser);
        }
        #endregion

        #region 删除自己发布的二手或求购文章
        /// <summary>
        /// 删除自己发布的二手或求购文章
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpGet]
        public async Task<ResponseContext<bool>> DeleteOneselfUsedOrAskBuyArticle(long id)
        {
            return await _service.DeleteOneselfUsedOrAskBuyArticle(id, this.CurrentUser);
        }
        #endregion

        #region 用户操作二手或求购文章活跃程度(查看、收藏)
        /// <summary>
        /// 用户操作二手或求购文章活跃程度(查看、收藏)
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost]
        public async Task<ResponseContext<bool>> UserOperationUsedOrAskBuyArticleForActive(UsedOrAskBuyArticleUserActiveRequestModel model)
        {
            return await _service.UserOperationUsedOrAskBuyArticleForActive(model, this.CurrentUser);
        }
        #endregion

        #region 获取个人发布/收藏二手或求购文章列表
        /// <summary>
        /// 获取个人发布/收藏二手或求购文章列表
        /// </summary>
        /// <param name="query"></param>
        /// <returns></returns>
        [HttpGet]
        public async Task<PageResponse<UsedOrAskBuyArticleViewModel>> GetOneSelfUsedOrAskBuyArticleList([FromQuery] UsedOrAskBuyArticleOneselfQuery query)
        {
            return await _service.GetOneSelfUsedOrAskBuyArticleList(query, this.CurrentUser);
        }
        #endregion

    }
}