﻿using Car.Framework;
using Car.Framework.Auth;
using Car.Framework.Authorizations;
using Car.Rcb.Api;
using Car.Rcb.Api.Auth;
using Car.Rcb.Dto;
using Car.Rcb.IServices;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;
using System;
using System.Threading.Tasks;

namespace Yssx.S.Api.Controllers
{
    /// <summary>
    /// 用户相关Api
    /// </summary>
    public class UserController : BaseController
    {
        /// <summary>
        /// 自定义策略参数
        /// </summary>
        PermissionRequirement _requirement;

        IUserService _service;

        /// <summary>
        /// 构造函数
        /// </summary>
        /// <param name="requirement"></param>
        /// <param name="service"></param>
        public UserController(PermissionRequirement requirement, IUserService service)
        {
            _requirement = requirement;
            _service = service;
        }

        #region 用户注册
        /// <summary>
        /// 用户注册
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost]
        [AllowAnonymous]
        public async Task<ResponseContext<UserTicket>> UseRegister(UserRegisterRequestModel model)
        {
            return await _service.UseRegister(model, HttpContext.GetClientUserIp(), _requirement);
        }
        #endregion

        #region 用户手机号、密码登录
        /// <summary>
        /// 用户手机号、密码登录
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>      
        [HttpPost]
        [AllowAnonymous]
        public async Task<ResponseContext<UserTicket>> UserPasswordLogin(UserLoginRequestModel model)
        {
            return await _service.UserPasswordLogin(model, HttpContext.GetClientUserIp(), _requirement);
        }
        #endregion

        #region 获取用户信息
        /// <summary>
        /// 获取用户信息
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public async Task<ResponseContext<UserInfoViewModel>> GetUserInfo()
        {
            return await _service.GetUserInfo(this.CurrentUser);
        }
        #endregion

        #region 更新用户信息
        /// <summary>
        /// 更新用户信息
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost]
        public async Task<ResponseContext<bool>> UpdateUserInfo(UserUpdateRequestModel model)
        {
            return await _service.UpdateUserInfo(model, this.CurrentUser);
        }
        #endregion

    }
}