﻿using Car.Framework;
using Car.Framework.Auth;
using Car.Framework.Authorizations;
using Car.Rcb.Api;
using Car.Rcb.Api.Auth;
using Car.Rcb.Dto;
using Car.Rcb.IServices;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Car.Framework.AliyunOss;
using System.IO;

namespace Yssx.S.Api.Controllers
{
    /// <summary>
    /// 上传文件相关Api
    /// </summary>
    public class UploadFilesController : BaseController
    {
        #region 文件上传
        /*
         * 
        /// <summary>
        /// 文件上传
        /// </summary>
        /// <param name="form"></param>
        /// <param name="requestBucketName"></param>
        /// <param name="watermark">是否添加水印(默认不添加)</param>
        /// <param name="iscompressed">是否压缩图片(默认压缩)</param>
        /// <param name="istestSpace"></param> 
        /// <returns></returns>
        [HttpPost]
        [AllowAnonymous]
        public ResponseContext<string> PostFiles(IFormCollection form, string requestBucketName = "rcb", [FromForm] bool watermark = false,
            [FromForm] bool iscompressed = true, [FromForm] bool istestSpace = false)
        {
            var file = form.Files.FirstOrDefault();
            var file1 = Request.Form.Files.FirstOrDefault();
            if (file == null)
            {
                if (file1 == null)
                    return new ResponseContext<string> { Code = CommonConstants.ErrorCode, Msg = "没有接收到文件，上传失败" };
                else
                    file = file1;
            }

            Tuple<string, string> tuple = null;
            string msg = "";
            try
            {
                tuple = UploadFormFile(file, requestBucketName, watermark, iscompressed, istestSpace);
            }
            catch (Exception ex)
            {
                msg = ex.Message;
            }

            if (tuple == null)
                return new ResponseContext<string> { Code = CommonConstants.ErrorCode, Msg = $"tuple为空,上传失败：{msg}" };

            return new ResponseContext<string> { Code = CommonConstants.SuccessCode, Data = tuple.Item2 };
        }

        */
        #endregion

        #region 文件上传
        /// <summary>
        /// 文件上传
        /// </summary>
        /// <param name="form"></param>
        /// <returns></returns>
        [HttpPost]
        [AllowAnonymous]
        public ResponseContext<List<string>> UploadIFormFile(IFormCollection form)
        {
            List<string> urlList = new List<string>();
            if (form.Files.Count > 0)
            {
                foreach (var file in form.Files)
                {
                    urlList.Add(UploadLocalFile(file).Data);
                }
                return new ResponseContext<List<string>> { Code = CommonConstants.SuccessCode, Data = urlList, Msg = "" };
            }
            else
            {
                return new ResponseContext<List<string>> { Code = CommonConstants.ErrorCode, Msg = "没有接收到文件，上传失败" };
            }
        }
        #endregion

        #region 上传本地文件
        /// <summary>
        /// 上传本地文件
        /// </summary>
        /// <param name="file"></param>
        /// <returns></returns>
        private ResponseContext<string> UploadLocalFile(IFormFile file)
        {
            //文件名
            string fileName = DateTime.Now.ToString("yyyyMMddHHmmssfff") + file.FileName; //System.IO.Path.GetExtension(file.FileName)
            //文件大小，单位字节
            int fileContentLength = (int)file.Length;
            //上传路径
            string filePath = Path.Combine(Directory.GetCurrentDirectory() + "/file/" + DateTime.Now.ToString("yyyy-MM-dd") + "/");
            //二进制数组             
            byte[] fileBytes = new byte[fileContentLength];
            //创建Stream对象，并指向上传文件
            Stream fileStream = file.OpenReadStream();
            //从当前流中读取字节，读入字节数组中
            fileStream.Read(fileBytes, 0, fileContentLength);
            //全路径（路劲+文件名）
            string fullPath = filePath + fileName;
            ////保存到磁盘
            //SaveToDisk(fileBytes, fullPath);

            var folder = Path.GetDirectoryName(fullPath);
            //如果没有此文件夹，则新建
            if (!Directory.Exists(folder))
            {
                Directory.CreateDirectory(folder);
            }
            //创建文件，返回一个 FileStream，它提供对 path 中指定的文件的读/写访问。
            using (FileStream stream = System.IO.File.Create(fullPath))
            {
                //将字节数组写入流
                stream.Write(fileBytes, 0, fileBytes.Length);
                stream.Close();
            }

            string serviceFilePath = "http://39.108.89.162:8082/" + DateTime.Now.ToString("yyyy-MM-dd") + "/" + fileName;

            #region 上传到阿里云OSS

            //string requestBucketName = "freessoul";

            //var file1 = Request.Form.Files.FirstOrDefault();
            //if (file == null)
            //{
            //    if (file1 == null)
            //        return new ResponseContext<string> { Code = CommonConstants.ErrorCode, Msg = "没有接收到文件，上传失败" };
            //    else
            //        file = file1;
            //}
            //Tuple<string, string> tuple = null;
            //string msg = "";
            //try
            //{
            //    tuple = UploadFormFile(file, requestBucketName, watermark, iscompressed, istestSpace);
            //}
            //catch (Exception ex)
            //{
            //    msg = ex.Message;
            //}

            //if (tuple == null)
            //    return new ResponseContext<string> { Code = CommonConstants.ErrorCode, Msg = $"tuple为空,上传失败：{msg}" };

            //return new ResponseContext<string> { Code = CommonConstants.SuccessCode, Data = tuple.Item2 };

            #endregion

            return new ResponseContext<string> { Code = CommonConstants.SuccessCode, Data = serviceFilePath };
        }
        #endregion

        #region 上传流文件
        /// <summary>
        /// 上传流文件
        /// </summary>
        /// <param name="file"></param>
        /// <param name="requestBucketName"></param>
        /// <param name="watermark"></param>
        /// <param name="iscompressed"></param>
        /// <param name="istestSpace"></param>
        /// <returns></returns>
        private Tuple<string, string> UploadFormFile(IFormFile file, string requestBucketName = "freessoul", bool watermark = false, bool iscompressed = true, bool istestSpace = false)
        {
            using (var ms = file.OpenReadStream())
            {
                return AliyunOssHelper.OssUpload(file.FileName, ms, requestBucketName, watermark, iscompressed, istestSpace);
            }
        }
        #endregion

        #region 上传本地文件(接口测试)
        /// <summary>
        /// 上传本地文件(接口测试)
        /// </summary>
        /// <param name="file"></param>
        /// <returns></returns>
        [HttpPost]
        [AllowAnonymous]
        public ResponseContext<string> UploadLocalFileTest(IFormFile file)
        {
            //文件名
            string fileName = DateTime.Now.ToString("yyyyMMddHHmmssfff") + file.FileName; //System.IO.Path.GetExtension(file.FileName)
            //文件大小，单位字节
            int fileContentLength = (int)file.Length;
            //上传路径
            string filePath = Path.Combine(Directory.GetCurrentDirectory() + "/file/" + DateTime.Now.ToString("yyyy-MM-dd") + "/");
            //二进制数组             
            byte[] fileBytes = new byte[fileContentLength];
            //创建Stream对象，并指向上传文件
            Stream fileStream = file.OpenReadStream();
            //从当前流中读取字节，读入字节数组中
            fileStream.Read(fileBytes, 0, fileContentLength);
            //全路径（路劲+文件名）
            string fullPath = filePath + fileName;
            ////保存到磁盘
            //SaveToDisk(fileBytes, fullPath);

            var folder = Path.GetDirectoryName(fullPath);
            //如果没有此文件夹，则新建
            if (!Directory.Exists(folder))
            {
                Directory.CreateDirectory(folder);
            }
            //创建文件，返回一个 FileStream，它提供对 path 中指定的文件的读/写访问。
            using (FileStream stream = System.IO.File.Create(fullPath))
            {
                //将字节数组写入流
                stream.Write(fileBytes, 0, fileBytes.Length);
                stream.Close();
            }

            string serviceFilePath = "http://39.108.89.162:8082/" + DateTime.Now.ToString("yyyy-MM-dd") + "/" + fileName;

            #region 上传到阿里云OSS

            //string requestBucketName = "freessoul";

            //var file1 = Request.Form.Files.FirstOrDefault();
            //if (file == null)
            //{
            //    if (file1 == null)
            //        return new ResponseContext<string> { Code = CommonConstants.ErrorCode, Msg = "没有接收到文件，上传失败" };
            //    else
            //        file = file1;
            //}
            //Tuple<string, string> tuple = null;
            //string msg = "";
            //try
            //{
            //    tuple = UploadFormFile(file, requestBucketName, watermark, iscompressed, istestSpace);
            //}
            //catch (Exception ex)
            //{
            //    msg = ex.Message;
            //}

            //if (tuple == null)
            //    return new ResponseContext<string> { Code = CommonConstants.ErrorCode, Msg = $"tuple为空,上传失败：{msg}" };

            //return new ResponseContext<string> { Code = CommonConstants.SuccessCode, Data = tuple.Item2 };

            #endregion

            return new ResponseContext<string> { Code = CommonConstants.SuccessCode, Data = serviceFilePath };
        }
        #endregion

    }
}