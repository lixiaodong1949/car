﻿using Car.Framework;
using Car.Framework.Authorizations;
using Car.Rcb.Api.Auth;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;


namespace Yssx.S.Api.Controllers
{
    [Produces("application/json")]
    [ApiVersion("1.0", Deprecated = false)]
    //[Route("api/v{api-version:apiVersion}/[controller]/[action]")]
    [Route("car/[controller]/[action]")]
    [ApiController]
    [EnableCors(CommonConstants.AnyOrigin)]//跨域
    [ServiceFilter(typeof(CustomExceptionFilter))]
    public class BaseController : Controller
    {
        /// <summary>
        /// 
        /// </summary>
        protected UserTicket CurrentUser { get; private set; }

        /// <summary>
        ///  Called before the action method is invoked.
        /// </summary>
        /// <param name="context"></param>
        [NonAction]
        public override void OnActionExecuting(ActionExecutingContext context)
        {
            HttpContext.Items.TryGetValue(CommonConstants.ContextUserPropertyKey, out var requestUser);
            CurrentUser = requestUser as UserTicket;
        }
    }
}