﻿using Car.Framework;
using Car.Framework.Auth;
using Car.Framework.Authorizations;
using Car.Framework.Entity;
using Car.Rcb.Api;
using Car.Rcb.Api.Auth;
using Car.Rcb.Dto;
using Car.Rcb.IServices;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Yssx.S.Api.Controllers
{
    /// <summary>
    /// 文章后台相关Api
    /// </summary>
    public class ArticleBackController : BaseController
    {
        IArticleBackService _service;

        /// <summary>
        /// 构造函数
        /// </summary>
        /// <param name="service"></param>
        public ArticleBackController(IArticleBackService service)
        {
            _service = service;
        }

        #region 获取技术文章后台列表
        /// <summary>
        /// 获取技术文章后台列表
        /// </summary>
        /// <param name="query"></param>
        /// <returns></returns>        
        [HttpGet]
        public async Task<PageResponse<TechnicalArticleForBackViewModel>> GetTechnicalArticleListForBack([FromQuery] TechnicalArticleForBackQuery query)
        {
            return await _service.GetTechnicalArticleListForBack(query);
        }
        #endregion

        #region 审核技术文章
        /// <summary>
        /// 审核技术文章
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost]
        public async Task<ResponseContext<bool>> AuditTechnicalArticle(TechnicalArticleAuditRequestModel model)
        {
            return await _service.AuditTechnicalArticle(model, this.CurrentUser);
        }
        #endregion

        #region 获取二手或求购文章后台列表
        /// <summary>
        /// 获取二手或求购文章后台列表
        /// </summary>
        /// <param name="query"></param>
        /// <returns></returns>
        [HttpGet]
        public async Task<PageResponse<UsedOrAskBuyArticleForBackViewModel>> GetUsedOrAskBuyArticleListForBack([FromQuery] UsedOrAskBuyArticleForBackQuery query)
        {
            return await _service.GetUsedOrAskBuyArticleListForBack(query);
        }
        #endregion

        #region 审核二手或求购文章
        /// <summary>
        /// 审核二手或求购文章
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost]
        public async Task<ResponseContext<bool>> AuditUsedOrAskBuyArticle(UsedOrAskBuyArticleAuditRequestModel model)
        {
            return await _service.AuditUsedOrAskBuyArticle(model, this.CurrentUser);
        }
        #endregion

    }
}