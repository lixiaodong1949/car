﻿using Car.Framework;
using Car.Framework.Auth;
using Car.Framework.Authorizations;
using Car.Framework.Entity;
using Car.Rcb.Api;
using Car.Rcb.Api.Auth;
using Car.Rcb.Dto;
using Car.Rcb.IServices;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Yssx.S.Api.Controllers
{
    /// <summary>
    /// 标签类型后台管理相关Api
    /// </summary>
    public class TagTypeBackController : BaseController
    {
        ITagTypeBackService _service;

        /// <summary>
        /// 构造函数
        /// </summary>
        /// <param name="service"></param>
        public TagTypeBackController(ITagTypeBackService service)
        {
            _service = service;
        }

        #region 技术标签

        #region 新增/编辑技术标签
        /// <summary>
        /// 新增/编辑技术标签
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost]
        public async Task<ResponseContext<bool>> AddOrEditTechnologyTag(TechnologyTagRequestModel model)
        {
            return await _service.AddOrEditTechnologyTag(model, this.CurrentUser);
        }
        #endregion

        #region 启用或禁用技术标签
        /// <summary>
        /// 启用或禁用技术标签
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost]
        public async Task<ResponseContext<bool>> EnabledOrDisabledTechnologyTag(EnabledOrDisabledDataRequestModel model)
        {
            return await _service.EnabledOrDisabledTechnologyTag(model, this.CurrentUser);
        }
        #endregion

        #region 删除技术标签
        /// <summary>
        /// 删除技术标签
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost]
        public async Task<ResponseContext<bool>> DeleteTechnologyTag(DeleteDataRequestModel model)
        {
            return await _service.DeleteTechnologyTag(model, this.CurrentUser);
        }
        #endregion

        #region 获取技术类标签列表(后台管理)
        /// <summary>
        /// 获取技术类标签列表(后台管理)
        /// </summary>
        /// <param name="query"></param>
        /// <returns></returns>
        [HttpGet]
        public async Task<PageResponse<TechnologyTagBackViewModel>> GetTechnologyTagBackList([FromQuery] TechnologyTagQuery query)
        {
            return await _service.GetTechnologyTagBackList(query);
        }
        #endregion

        #endregion

        #region 品牌类型

        #region 新增/编辑品牌类型
        /// <summary>
        /// 新增/编辑品牌类型
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost]
        public async Task<ResponseContext<bool>> AddOrEditBrandType(BrandTypeRequestModel model)
        {
            return await _service.AddOrEditBrandType(model, this.CurrentUser);
        }
        #endregion

        #region 启用或禁用品牌类型
        /// <summary>
        /// 启用或禁用品牌类型
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost]
        public async Task<ResponseContext<bool>> EnabledOrDisabledBrandType(EnabledOrDisabledDataRequestModel model)
        {
            return await _service.EnabledOrDisabledBrandType(model, this.CurrentUser);
        }
        #endregion

        #region 删除品牌类型
        /// <summary>
        /// 删除品牌类型
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost]
        public async Task<ResponseContext<bool>> DeleteBrandType(DeleteDataRequestModel model)
        {
            return await _service.DeleteBrandType(model, this.CurrentUser);
        }
        #endregion

        #region 获取品牌类型列表(后台管理)
        /// <summary>
        /// 获取品牌类型列表(后台管理)
        /// </summary>
        /// <param name="query"></param>
        /// <returns></returns>
        [HttpGet]
        public async Task<PageResponse<BrandTypeBackViewModel>> GetBrandTypeBackList([FromQuery] BrandTypeQuery query)
        {
            return await _service.GetBrandTypeBackList(query);
        }
        #endregion

        #endregion

        #region 配件分类

        #region 新增/编辑配件分类
        /// <summary>
        /// 新增/编辑配件分类
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost]
        public async Task<ResponseContext<bool>> AddOrEditAccessoryType(AccessoryTypeRequestModel model)
        {
            return await _service.AddOrEditAccessoryType(model, this.CurrentUser);
        }
        #endregion

        #region 启用或禁用配件分类
        /// <summary>
        /// 启用或禁用配件分类
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost]
        public async Task<ResponseContext<bool>> EnabledOrDisabledAccessoryType(EnabledOrDisabledDataRequestModel model)
        {
            return await _service.EnabledOrDisabledAccessoryType(model, this.CurrentUser);
        }
        #endregion

        #region 删除配件分类
        /// <summary>
        /// 删除配件分类
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost]
        public async Task<ResponseContext<bool>> DeleteAccessoryType(DeleteDataRequestModel model)
        {
            return await _service.DeleteAccessoryType(model, this.CurrentUser);
        }
        #endregion

        #region 获取品牌类型列表(后台管理)
        /// <summary>
        /// 获取品牌类型列表(后台管理)
        /// </summary>
        /// <param name="query"></param>
        /// <returns></returns>
        [HttpGet]
        public async Task<PageResponse<AccessoryTypeBackViewModel>> GetAccessoryTypeBackList([FromQuery] AccessoryTypeQuery query)
        {
            return await _service.GetAccessoryTypeBackList(query);
        }
        #endregion

        #endregion

        #region 产品类型

        #region 新增/编辑产品类型
        /// <summary>
        /// 新增/编辑产品类型
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost]
        public async Task<ResponseContext<bool>> AddOrEditProductType(ProductTypeRequestModel model)
        {
            return await _service.AddOrEditProductType(model, this.CurrentUser);
        }
        #endregion

        #region 启用或禁用产品类型 
        /// <summary>
        /// 启用或禁用产品类型
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost]
        public async Task<ResponseContext<bool>> EnabledOrDisabledProductType(EnabledOrDisabledDataRequestModel model)
        {
            return await _service.EnabledOrDisabledProductType(model, this.CurrentUser);
        }
        #endregion

        #region 删除产品类型
        /// <summary>
        /// 删除产品类型
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost]
        public async Task<ResponseContext<bool>> DeleteProductType(DeleteDataRequestModel model)
        {
            return await _service.DeleteProductType(model, this.CurrentUser);
        }
        #endregion

        #region 获取产品类型列表(后台管理)
        /// <summary>
        /// 获取产品类型列表(后台管理)
        /// </summary>
        /// <param name="query"></param>
        /// <returns></returns>
        [HttpGet]
        public async Task<PageResponse<ProductTypeBackViewModel>> GetProductTypeBackList([FromQuery] ProductTypeQuery query)
        {
            return await _service.GetProductTypeBackList(query);
        }
        #endregion

        #endregion

        #region 新旧程度

        #region 新增/编辑新旧程度
        /// <summary>
        /// 新增/编辑新旧程度
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost]
        public async Task<ResponseContext<bool>> AddOrEditNewOldDegree(NewOldDegreeRequestModel model)
        {
            return await _service.AddOrEditNewOldDegree(model, this.CurrentUser);
        }
        #endregion

        #region 启用或禁用新旧程度
        /// <summary>
        /// 启用或禁用新旧程度
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost]
        public async Task<ResponseContext<bool>> EnabledOrDisabledNewOldDegree(EnabledOrDisabledDataRequestModel model)
        {
            return await _service.EnabledOrDisabledNewOldDegree(model, this.CurrentUser);
        }
        #endregion

        #region 删除新旧程度
        /// <summary>
        /// 删除新旧程度
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost]
        public async Task<ResponseContext<bool>> DeleteNewOldDegree(DeleteDataRequestModel model)
        {
            return await _service.DeleteNewOldDegree(model, this.CurrentUser);
        }
        #endregion

        #region 获取新旧程度列表(后台管理)
        /// <summary>
        /// 获取新旧程度列表(后台管理)
        /// </summary>
        /// <param name="query"></param>
        /// <returns></returns>
        [HttpGet]
        public async Task<PageResponse<NewOldDegreeBackViewModel>> GetNewOldDegreeBackList([FromQuery] NewOldDegreeQuery query)
        {
            return await _service.GetNewOldDegreeBackList(query);
        }
        #endregion

        #endregion

        #region 磨损程度

        #region 新增/编辑磨损程度
        /// <summary>
        /// 新增/编辑磨损程度
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost]
        public async Task<ResponseContext<bool>> AddOrEditAbrasionDegree(AbrasionDegreeRequestModel model)
        {
            return await _service.AddOrEditAbrasionDegree(model, this.CurrentUser);
        }
        #endregion

        #region 启用或禁用磨损程度
        /// <summary>
        /// 启用或禁用磨损程度
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost]
        public async Task<ResponseContext<bool>> EnabledOrDisabledAbrasionDegree(EnabledOrDisabledDataRequestModel model)
        {
            return await _service.EnabledOrDisabledAbrasionDegree(model, this.CurrentUser);
        }
        #endregion

        #region 删除磨损程度
        /// <summary>
        /// 删除磨损程度
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost]
        public async Task<ResponseContext<bool>> DeleteAbrasionDegree(DeleteDataRequestModel model)
        {
            return await _service.DeleteAbrasionDegree(model, this.CurrentUser);
        }
        #endregion

        #region 获取磨损程度列表(后台管理)
        /// <summary>
        /// 获取磨损程度列表(后台管理)
        /// </summary>
        /// <param name="query"></param>
        /// <returns></returns>
        [HttpGet]
        public async Task<PageResponse<AbrasionDegreeBackViewModel>> GetAbrasionDegreeBackList([FromQuery] AbrasionDegreeQuery query)
        {
            return await _service.GetAbrasionDegreeBackList(query);
        }
        #endregion

        #endregion
    }
}